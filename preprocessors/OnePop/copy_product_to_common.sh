#!/bin/bash
# This script copy xml files from 2 locations to the shared space for 'en' and 'fr'

#user who is authorized to run
U=autonomy
echo "Start $0 $(date)"

#checking the user
if [ $USER != $U ]
then
	echo "This script should be for $U  or $U_PRD - not  for $USER"
	exit -1
fi

#Startup catalogs
DIR_S=/Autonomy/Preprocessors/OnePop_zone2
DIR_1_en=dest_tmp/US/en_US
DIR_2_en=dest_tmp/WW/en_GB
DIR_1_fr=dest_tmp/FR/fr_FR
DIR_2_fr=dest_tmp/WW/fr_FR

#Destination catalogs
DIR_OUT_en=dest_tmp/WW/com_en
DIR_OUT_fr=dest_tmp/WW/com_fr

arg=$#
if [ $arg -gt 1 ]
then
        echo "Invalid number of invocation arguments"
        echo" More $0 --help"
        exit -2
fi
if [ "x$1" = "x--help" ]
then
        echo "This script copy (hard link) files xml from:"
        echo "for EN"
	echo " $DIR_S/$DIR_1_en"
        echo " $DIR_S/$DIR_2_en"
        echo "destination directory $DIR_S/$DIR_OUT_en"
	echo "for FR"
        echo " $DIR_S/$DIR_1_fr"
        echo " $DIR_S/$DIR_2_fr"
        echo "destination directory $DIR_S/$DIR_OUT_fr"
        exit 0
fi

#Checking the availability of directories
if [ ! -d  $DIR_S ]; then
	echo " $DIR_S not exists"
	exit -1 
fi
cd $DIR_S
if [ ! -d  $DIR_1_en ]; then
	echo " $DIR_1_en not exists"
	exit -1 
fi
if [ ! -d  $DIR_2_en ]; then
	echo " $DIR_2_en not exists"
	exit -1 
fi
if [ ! -d  $DIR_1_fr ]; then
	echo " $DIR_1_fr not exists"
	exit -1 
fi
if [ ! -d  $DIR_2_fr ]; then
	echo " $DIR_2_fr not exists"
	exit -1 
fi
if [ ! -d  $DIR_OUT_en ]; then
	mkdir $DIR_OUT_en
fi
if [ ! -d  $DIR_OUT_fr ]; then
	mkdir $DIR_OUT_fr
fi

echo "Start  update $DIR_1_en: `date`" 
cd $DIR_S/$DIR_1_en 
for ii in $(find . -name "*.xml")
do
	ln -f $ii $DIR_S/$DIR_OUT_en/$ii
done

echo "Start  update $DIR_2_en: `date`" 
cd $DIR_S/$DIR_2_en 
for ii in $(find . -name "*.xml")
do
	ln -f $ii $DIR_S/$DIR_OUT_en/$ii
done

echo "Start  update $DIR_1_fr: `date`" 
cd $DIR_S/$DIR_1_fr 
for ii in $(find . -name "*.xml")
do
	ln -f $ii $DIR_S/$DIR_OUT_fr/$ii
done

echo "Start  update $DIR_2_fr: `date`" 
cd $DIR_S/$DIR_2_fr 
for ii in $(find . -name "*.xml")
do
	ln -f $ii $DIR_S/$DIR_OUT_fr/$ii
done

echo "End   $0 $(date)" 
exit 0
