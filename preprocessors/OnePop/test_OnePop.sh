#!/bin/bash
#The script compares the number of products in the OLD_DIR and NEW_DIR catalogs.
#If the number of xml files for each country is greater than MIN_PROCENT_OF_PODUCT, it returns 0, otherwise it returns -1

MIN_PROCENT_OF_PODUCT=90

# OLD_DIR=/hpe/idol/preprocessors/OnePop/dest
# NEW_DIR=/hpe/idol/preprocessors/OnePop/dest_new

OLD_DIR=$(pwd)/dest
NEW_DIR=$(pwd)/dest_tmp

if [ ! -d $OLD_DIR ]
then
	echo "Directory $OLD_DIR not exists"
	exit -1
fi

if [ ! -d $NEW_DIR  ]
then
	echo "Directory $NEW_DIR not exists"
	echo -1
fi


echo "Start testing $(date)"
for i_old in $(ls -1d $OLD_DIR/??/*"_"??)
do 
  	i_new=$(echo $i_old|sed "s;$OLD_DIR;$NEW_DIR;")		

	SUM_OLD=$(find $i_old -type f -name "*xml"|wc -l)
	SUM_NEW=$(find $i_new -type f -name "*xml"|wc -l)
	MIN_NO=$(($SUM_OLD*$MIN_PROCENT_OF_PODUCT/100))
	if [ $SUM_NEW -lt $MIN_NO ]
	then
		echo "ERROR: Number of files in $i_new is too small"
		echo $i_old $SUM_OLD  $i_new $SUM_NEW "MIN_NO="$MIN_NO
		exit -1
	else
		echo "For $i_old is ok ( $SUM_OLD > $MIN_NO )"
	fi
done
echo "Stop  testing $(date)"
exit 0

