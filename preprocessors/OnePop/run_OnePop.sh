#!/bin/bash
export JAVA_HOME=/hpe/idol/jvm/jdk-11.0.2
export PATH=$JAVA_HOME/bin:/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/sbin
DEST_TMP=$(pwd)/dest_tmp
DEST=$(pwd)/dest
DEST_OLD=$(pwd)/dest_old
BSL=$(pwd)/BSL-data

echo "Start test $DEST_TMP $(date)"
if [ -d $DEST_TMP ]
then
	echo "Directory  $DEST_TMP exist - exiting..."
	exit -1
else
	mkdir $DEST_TMP
fi
echo "End   test $DEST_TMP $(date)"

echo "Start downloading $(date)"
if [ ! -d $BSL ]
then
	mkdir $BSL
fi

URL=http:\/\/bsl.schneider-electric.com\/bsl-solr-service\/getProducts?start=0\&rows=999999999
echo $URL
if [ $(find $BSL -type f -name products.xml -mtime -1|wc -l) -eq 0 ]
then
	mv -f ${BSL}/products.xml ${BSL}/products.xml_old
 	wget $URL -O ${BSL}/products.xml
	stat=$?
	echo "Status wget=$stat"
	if [ $stat -eq 0 ]
	then
		chmod 644 $BSL/products.xml
	else
		echo "wget products.xml finising with ERROR = $stat" 	
		rmdir $DEST_TMP 
		rm $BSL/products.xml
		mv ${BSL}/products.xml_old ${BSL}/products.xml
		exit -2
	fi
else
	echo "$BSL/products.xml is fresh"
fi


URL=http:\/\/bsl.schneider-electric.com\/bsl-solr-service\/getRanges?start=0\&rows=99999
echo $URL
if [ $(find $BSL -type f -name ranges.xml -mtime -1|wc -l) -eq 0 ]
then
	mv -f ${BSL}/ranges.xml ${BSL}/ranges.xml_old
 	wget $URL -O BSL-data/ranges.xml
	stat=$?
        echo "Status wget=$stat"
        if [ $stat -eq 0 ]
        then
		chmod 644 $BSL/ranges.xml
        else
                echo "wget ranges.xml finising with ERROR = $stat"
                rmdir $DEST_TMP 
                rm $BSL/ranges.xml
                mv ${BSL}/ranges.xml_old ${BSL}/ranges.xml
                exit -4
        fi

else
	echo "$BSL/ranges.xml is fresh"
fi
echo "End   downloading $(date)"

echo "Start java $(date)"
java -version


java    -Xmx24G -DtotalEntitySizeLimit=2147480000 -Djdk.xml.totalEntitySizeLimit=2147480000 -jar jar/preprocessor2-0.0.20-onepop2019.jar \
	--getProducts=$BSL/products.xml \
	--getRanges=$BSL/ranges.xml \
	--rangeSitemap=cfg/RangeSitemapURLs.props \
	--write-method=zip2 \
	--productsTypesForCountriesFilesDir=product_type \
	--hitsSiteToCountryAndLangFile=cfg/dict_country_code.txt \
	--productsHitsFile=cfg/ProductDatasheet.csv \
	--maxHits=9999999 \
	--labelsFile=cfg/onepopLabels.txt \
	--outputDir=$DEST_TMP \
	--extendedBusinessLogging=true \
	--country-locale=cfg/OnePop.props

stat=$?
echo "Status=$stat"
if [ $stat -ne 0 ]
then
	echo "java finising with ERROR = $stat"
	rm -rf $DEST_TMP
	exit -8
fi
echo "End   java $(date)"

echo "Start unzip $(date)"

unzip -q  $DEST_TMP/files.zip -d $DEST_TMP
stat=$?
echo "Status=$stat"
if [ $stat -ne 0 ]
then
        echo "unzip finising with ERROR = $stat"
        rm -rf $DEST_TMP
        exit -16
fi
echo "End   unzip $(date)"



echo "Start create com directories for WW $(date)"
# Only for WW
#
# US/en and WW/en add to WW/com_en
# FR/fr and WW/fr add to WW/com_fr

./copy_product_to_common.sh
stat=$?
if [ $stat -ne 0 ]
then
        echo "copy_product_to_common_in_tmp.sh finising with ERROR = $stat"
        rm -rf $DEST_TMP
        exit -32
fi


echo "End   create com directories for WW $(date)"

./test_OnePop.sh
stat=$?
if [ $stat -ne 0 ]
then
        echo "test_OnePop.sh finising with ERROR = $stat"
        rm -rf $DEST_TMP
        exit -64
fi

echo "Start rm $DEST_OLD $(date)"
if [ -d $DEST_OLD ]
then
	mv $DEST_OLD ${DEST_OLD}_rm
	rm -rf ${DEST_OLD}_rm
	echo "Status rm=$?"
fi
echo "End   rm ${DEST_OLD} $(date)"

echo "Start mv $(date)"
if [ -d $DEST ]
then
	mv -f $DEST $DEST_OLD
	echo "Status mv $DEST $DEST_OLD =$?"
fi
if [ -d $DEST_TMP ]
then
	mv -f $DEST_TMP $DEST
	echo "Status mv $DEST_TMP $DEST =$?"
fi
echo "End   mv $(date)"

echo "End  all"
exit 0

