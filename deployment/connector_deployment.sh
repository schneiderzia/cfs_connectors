##################################################################################################################
# Input validation
##################################################################################################################

environment=`echo $1 | egrep '^prod_emea$|^prod_apac$|^prod_game$|^preprod_old$|^preprod_new$|^dev$'`
zone=`echo $2 | grep '^[0123456789]$'`
conn_name=`echo $3 | egrep '^all$|^common$|^bsldoc$|^ecat$|^inquira$|^pes$|^sdl$|^blog$|^oos$|^prodsub$|^video$|^crossref$|^thomasnet$|^traceparts$|^ecobulding$|^digest$|^myse$|^clipsal$|^faq$|^marketo$'`
options=`echo $4 | egrep '^deployment$|^configuration$|^connector$|^cfs$|^lua$|^stop$|^start$|^cleanup$|^index$|^sitemaps$'`

echo "Validated Input:" 
echo "  environment: $environment"
echo "  zone: $zone"
echo "  connector : $conn_name"
echo "  options: $options"

if [ -z $environment ]; then
  echo "Environemnt empty or not valid, should be one from the list: prod_emea, prod_apac, prod_game, preprod_old, preprod_new,dev"
  exit
fi

if [ -z $zone ]; then
  echo "Zone empty or not valid, should be a digit"
  exit
fi

if [ -z $conn_name ]; then
  echo "Connector empty or not valid, should be one from the list: common, bsldoc, ecat, inquira, pes, sdl, blog, oos, prodsub, video, crossref, thomasnet, traceparts, digest, myse, clipsal, faq, marketo or all"
  exit
fi

if [ -z $options ]; then
  echo "Options empty or not valid, should be one from the list: deployment, configuration, connector, cfs, lua, stop, start, cleanup, index, sitemaps"
  exit
fi

if [ "$conn_name" == "common" ] && [ "$options" != "lua" ]; then 
 echo "For common as the connector the only valid option is lua"
 exit
fi

if [ "$conn_name" != "common" ] && [ "$options" == "lua" ]; then
 echo "For lua option the only valid connector name is common"
 exit
fi


##################################################################################################################
### Environments configuration
##################################################################################################################

### Default values ###
indexer_port="18${zone}10"
error_server_port="12${zone}10"
port_prefix="4"
onepop_preprocessor_path="/hpe/idol/src"
prodsub_preprocessor_path="/hpe/idol/src"
ddc_preprocessor_path="/hpe/idol/src"
video_preprocessor_path="/hpe/idol/src"
traceparts_preprocessor_path="/hpe/idol/src"
digest_preprocessor_path="/hpe/idol/src"
faq_preprocessor_path="/hpe/idol/src"
marketo_preprocessor_path="/hpe/idol/src"
bsl_import_path="/Autonomy/Data/BSLData"
hit_statistics_path="/hpe/idol/hit_statistics"

### Production EMEA ###
if [ "$environment" == "prod_emea" ]; then

 conn_server_ip="10.238.17.31"
 conn_server_name="schnapxp1752.ebusiness.schneider-electric.com"
 license_server_ip="10.238.17.29"
 port_prefix="5"
 lp_sitemaps_path="/hpe/idol/zone${zone}/cfs_connectors/sdl/sitemaps"

 if [ "$zone" == "2" ]; then
  jobs="at_de bg_bg ch_de ch_fr cz_cs de_de fi_fi hr_hr it_it lt_lt lv_lv pl_pl pt_pt si_sl sk_sk"
  conn_path="zone${zone}/cfs_connectors"
 fi

 if [ "$zone" == "4" ]; then
  jobs="dz_fr eg_ar eg_en es_es fr_fr ie_en il_he ru_ru tr_tr xf_fr za_en"
  conn_path="zone${zone}/cfs_connectors"
 fi

 if [ "$zone" == "5" ]; then
  jobs="be_fr be_nl dk_da ee_et gr_el hu_hu ng_en nl_nl no_no ro_ro rs_sr se_sv ua_ru ua_uk uk_en"
  conn_path="zone${zone}/cfs_connectors"
 fi

fi

### Production APAC ###
if [ "$environment" == "prod_apac" ]; then

 conn_server_ip="10.238.17.31"
 conn_server_name="schnapxp1752.ebusiness.schneider-electric.com"
 license_server_ip="10.238.17.29"
 indexer_port="18${zone}20"
 error_server_port="12010"

 if [ "$zone" == "0" ]; then
  jobs="au_en hk_en id_en id_id in_en jp_ja kr_ko my_en sg_en th_en th_th tw_zh vn_en vn_vi"
  conn_path="wms/apac1/cfs_connectors"
  lp_sitemaps_path="/hpe/idol/${conn_path}/sdl/sitemaps"
  clipsal_sitemaps_path="/hpe/idol/${conn_path}/clipsal_sitemaps"
 fi

 if [ "$zone" == "2" ]; then
  jobs="ae_en cn_zh kz_ru nz_en sa_ar sa_en ph_en"
  conn_path="wms/apac2/cfs_connectors"
  lp_sitemaps_path="/hpe/idol/${conn_path}/sdl/sitemaps"
 fi

fi

### Production GAME ###
if [ "$environment" == "prod_game" ]; then

 conn_server_ip="10.238.32.31"
 conn_server_name="LIDOLUS05PRD.schneider-electric.com"
 license_server_ip="10.238.32.7"
 indexer_port="18${zone}10"
 error_server_port="12010"
 bsl_import_path="/hpe/idol/Data/BSLData"
 lp_sitemaps_path="/hpe/idol/wms/game1/cfs_connectors/sdl/sitemaps"

 if [ "$environment" == "prod_game" ] && [ "$zone" == "3" ]; then
  jobs="ar_es br_pt ca_en ca_fr cl_es co_es cr_es mx_es pe_es us_en ww_en ww_fr"
  conn_path="wms/game1/cfs_connectors"
  indexer_port="18030"
 fi

if [ "$environment" == "prod_game" ] && [ "$zone" == "6" ]; then
  jobs="at_de at_en be_en be_fr be_nl ch_de ch_en ch_fr ch_it de_de de_en dk_da dk_en el_en el_no fi_en fi_fi fi_sv hr_en hr_hr hu_en hu_hu nl_en nl_nl no_en no_no ro_en ro_ro se_en se_sv si_en si_sl sk_en sk_sk za_en"
  conn_path="myse/zone6/cfs_connectors"
  myse_connection="READ_ONLY/ResdOnlY125#@MYSE5"
fi

if [ "$environment" == "prod_game" ] && [ "$zone" == "7" ]; then
  jobs="bg_bg bg_en cz_cs cz_de cz_en cz_ru dz_en dz_fr eg_en el_en el_no es_ar es_en es_es fr_en fr_fr gb_en gr_el gr_en ie_en il_en il_he int_en int_es int_fr ma_en ma_fr ng_en pl_en pl_pl pt_en pt_pt rs_en rs_sr ru_en ru_ru tr_en tr_ru tr_tr ua_en ua_ru ua_uk"
  conn_path="myse/zone7/cfs_connectors"
  myse_connection="READ_ONLY/ResdOnlY125#@MYSE5"
fi

if [ "$environment" == "prod_game" ] && [ "$zone" == "8" ]; then
  jobs="ae_en au_en cn_en cn_zh hk_en hk_zz id_en id_id in_en jp_en jp_ja jp2_en jp2_ja kr_en kr_ko kz_en kz_ru my_en nz_en ph_en sa_en sg_en th_en th_th tw_en tw_zh vn_en vn_vi"
  conn_path=" myse/zone8/cfs_connectors "
  myse_connection="READ_ONLY/ResdOnlY125#@MYSE5"
fi

fi

### Pre-production old ###
if [ "$environment" == "preprod_old" ]; then

 conn_server_ip="10.238.13.27"
 conn_server_name="schmapxu1717.ebusiness.schneider-electric.com"
 license_server_ip="10.238.13.25"
 port_prefix="5"
# onepop_preprocessor_path="/Autonomy/PreProcessors/OnePopStep2XmlPreprocessor"
# prodsub_preprocessor_path="/Autonomy/Preprocessors/Productsubstitution_zone2"

 if [ "$zone" == "0" ]; then
  jobs="ar_es br_pt ca_en ca_fr cl_es co_es cr_es mx_es pe_es us_en ww_en ww_fr"
  conn_path="zone${zone}/cfs_connectors"
 fi

 if [ "$zone" == "1" ]; then
  jobs="au_en be_fr be_nl ca_en ca_fr cn_zh de_de es_es no_no ru_ru se_sv uk_en us_en ww_fr ww_en"
  conn_path="zone${zone}/cfs_connectors"
  onepop_preprocessor_path="/Autonomy/PreProcessors/OnePopStep2XmlPreprocessor_preprod"
  prodsub_preprocessor_path="/Autonomy/Preprocessors/Productsubstitution_preprod"
 fi

 if [ "$zone" == "2" ]; then
  jobs="au_en be_fr be_nl ca_en ca_fr cn_zh de_de es_es fr_fr il_he no_no ru_ru se_sv uk_en us_en ww_fr ww_en"
  conn_path="zone${zone}/cfs_connectors"
 fi

fi

### Pre-production new ###
if [ "$environment" == "preprod_new" ]; then

 conn_server_ip="10.238.86.116"
 conn_server_name="LIDOLDIT7926.schneider-electric.com"
 license_server_ip="10.238.81.235"
 bsl_import_path="/hpe/idol/Data/BSLData"
 lp_sitemaps_path="/hpe/idol/preprocessors/data/lp_sitemaps"
 hit_statistics_path="/hpe/idol/preprocessors/data/hit_statistics"
 error_server_port="12010"

 if [ "$zone" == "0" ]; then
  jobs="ar_es br_pt cl_es co_es cr_es pe_es ww_en ww_fr"
  conn_path="wms/game1/cfs_connectors"
  lp_sitemaps_path="/hpe/idol/${conn_path}/sdl/sitemaps"
 fi

 if [ "$zone" == "2" ]; then
  jobs="ca_en ca_fr mx_es us_en"
  conn_path="wms/game2/cfs_connectors"
  lp_sitemaps_path="/hpe/idol/${conn_path}/sdl/sitemaps"
 fi

 if [ "$zone" == "3" ]; then
  jobs="au_en hk_en in_en jp_ja kr_ko my_en nz_en sg_en tw_zh vn_en vn_vi"
  conn_path="wms/apac1/cfs_connectors"
  lp_sitemaps_path="/hpe/idol/${conn_path}/sdl/sitemaps"
  clipsal_sitemaps_path="/hpe/idol/wms/apac1/preprocessors/data/clipsal_sitemaps"
 fi

 if [ "$zone" == "4" ]; then
  jobs="ae_en cn_zh id_en id_id kz_ru ph_en sa_ar sa_en th_en th_th"
  conn_path="wms/apac2/cfs_connectors"
  lp_sitemaps_path="/hpe/idol/${conn_path}/sdl/sitemaps"
 fi

 if [ "$zone" == "6" ] && [ "$conn_name" == "myse" ]; then
  jobs="ae_en au_en cn_en cn_zh hk_en hk_zz id_en id_id in_en jp_en jp_ja jp2_en jp2_ja kr_en kr_ko kz_en kz_ru my_en nz_en ph_en sa_en sg_en th_en th_th tw_en tw_zh vn_en vn_vi"
  conn_path="myse/zone${zone}/cfs_connectors"
  myse_connection="MYSEDV1/MYSEDV1@MYSE1"
 fi

 if [ "$zone" == "7" ] && [ "$conn_name" == "myse" ]; then
  jobs="at_de at_en bg_bg bg_en ch_de ch_en ch_fr ch_it cz_cs cz_de cz_en cz_ru de_de de_en fi_fi fi_en fi_sv hr_hr hr_en pl_en pl_pl pt_en pt_pt si_sl si_en sk_sk sk_en"
  conn_path="myse/zone${zone}/cfs_connectors"
  myse_connection="MYSEDV1/MYSEDV1@MYSE1"
 fi

 if [ "$zone" == "8" ] && [ "$conn_name" == "myse" ]; then
  jobs="ar_en ar_es br_en br_pt ca_en ca_fr cl_en cl_es co_en co_es cr_en cr_es mx_en mx_es pe_en pe_es us_en us2_en"
  conn_path="myse/zone${zone}/cfs_connectors"
  myse_connection="MYSEDV1/MYSEDV1@MYSE1"
 fi

 if [ "$zone" == "9" ] && [ "$conn_name" == "myse" ]; then
  jobs="be_en be_fr be_nl dk_da dk_en el_no el_en es_ar es_en es_es fr_en fr_fr gb_en gr_el gr_en hu_hu hu_en ma_en ma_fr ng_en nl_en nl_nl no_no no_en ro_ro ro_en rs_en rs_sr se_sv se_en ua_en ua_ru ua_uk za_en"
  conn_path="myse/zone${zone}/cfs_connectors"
  myse_connection="MYSEDV1/MYSEDV1@MYSE1"
 fi

 if [ "$zone" == "5" ] && [ "$conn_name" != "myse" ]; then
  jobs="be_fr be_nl cz_cs dz_fr ie_en ng_en pl_pl pt_pt se_sv uk_en"
  conn_path="wms/emea1/cfs_connectors"
  lp_sitemaps_path="/hpe/idol/${conn_path}/sdl/sitemaps"
 fi

 if [ "$zone" == "6" ] && [ "$conn_name" != "myse" ]; then
  jobs="ch_de ch_fr dk_da fr_fr hr_hr no_no rs_sr si_sl sk_sk"
  conn_path="wms/emea2/cfs_connectors"
  lp_sitemaps_path="/hpe/idol/${conn_path}/sdl/sitemaps"
 fi

 if [ "$zone" == "7" ] && [ "$conn_name" != "myse" ]; then
  jobs="at_de de_de eg_ar eg_en es_es hu_hu it_it nl_nl xf_fr za_en"
  conn_path="wms/emea3/cfs_connectors"
  lp_sitemaps_path="/hpe/idol/${conn_path}/sdl/sitemaps"
 fi

 if [ "$zone" == "8" ] && [ "$conn_name" != "myse" ]; then
  jobs="bg_bg ee_et fi_fi gr_el il_en il_he lt_lt lv_lv ro_ro ru_ru tr_tr ua_ru ua_uk"
  conn_path="wms/emea4/cfs_connectors"
  lp_sitemaps_path="/hpe/idol/${conn_path}/sdl/sitemaps"
 fi

fi

### Dev & Int ###
if [ "$environment" == "dev" ]; then

 conn_server_ip="10.238.81.165"
 conn_server_name="LDCXDIT4427.schneider-electric.com"
 license_server_ip="10.238.82.20"
 bsl_import_path="/hpe/idol/Data/BSLData"
 lp_sitemaps_path="/hpe/idol/preprocessors/data/lp_sitemaps"
 clipsal_sitemaps_path="/hpe/idol/preprocessors/data/lp_sitemaps"
 hit_statistics_path="/hpe/idol/preprocessors/data/hit_statistics"
 bsl_import_path="/hpe/idol/Data/BSLData"
 port_prefix="4"

 if [ "$zone" == "0" ]; then
  jobs="au_en be_fr be_nl ca_en ca_fr cn_zh de_de es_es fr_fr il_he no_no ru_ru se_sv uk_en us_en ww_fr ww_en"
  conn_path="zone${zone}/cfs_connectors"
 fi

 if [ "$zone" == "1" ]; then
  jobs="au_en be_fr be_nl ca_en ca_fr cn_zh de_de es_es no_no ru_ru se_sv uk_en us_en ww_fr ww_en"
  conn_path="zone${zone}/cfs_connectors"
  ddc_preprocessor_path="/hpe/idol/preprocessors/bsldoc_preprod/ddc_out/dest"
  onepop_preprocessor_path="/hpe/idol/preprocessors/OnePop_preprod/dest"
  prodsub_preprocessor_path="/hpe/idol/preprocessors/Productsubstitution_preprod"
 fi

 if [ "$zone" == "2" ]; then
  jobs="au_en be_fr be_nl ca_en ca_fr cn_zh de_de es_es fr_fr il_he no_no ru_ru se_sv uk_en us_en ww_fr ww_en"
  conn_path="zone${zone}/cfs_connectors"
 fi

 if [ "$zone" == "3" ]; then
  jobs="us_en"
  conn_path="zone0/rf/cfs_connectors"
 fi

fi


### Zone validation
if [ "$conn_name" != "common" ]; then
 if [ "$conn_path" == "" ] || [ "$jobs" == "" ]; then
  echo "Zone $zone is not correct for $environment environment"
  exit
 fi
fi


##################################################################################################################
### Connectors & CFS configuration
##################################################################################################################

### Default values
lua_path="/hpe/idol/${conn_path}/common/lua"
number_of_jobs=$(echo $jobs |wc -w)
conn_subnames="$conn_name"
indexer_batch_size=1000
indexer_time_interval=300
number_of_threads=$number_of_jobs
xml="false"

cfs_task="//Pre0=IdxWriter:./output_pre0.idx
Pre0=lua:${lua_path}/wms_doc_pre_processing.lua
Post0=lua:${lua_path}/wms_doc_post_processing.lua
Post1=lua:${lua_path}/gs_add_fields.lua
Post2=lua:${lua_path}/get_cat_values.lua
Post3=ImportErrorFilter:ImportErrorFilterSettings
//Post4=IdxWriter:./output_post0.idx"

###  Documents
if [ "$conn_name" == "bsldoc" ]; then

 conn_number="03"
 conn_type="fsc"
 indexer_batch_size=5000
 indexer_time_interval=150
 number_of_threads=$((number_of_threads*2))

fi

### Products
if [ "$conn_name" == "ecat" ]; then

 conn_number="09"
 conn_type="fsc"
 indexer_batch_size=5000
 indexer_time_interval=150

cfs_task="//Pre0=IdxWriter:./output_pre0.idx
Pre0=lua:${lua_path}/wms_doc_pre_processing.lua
Post0=lua:${lua_path}/wms_doc_post_processing.lua
Post1=lua:${lua_path}/gs_add_fields.lua
Post2=lua:${lua_path}/get_cat_values.lua
Post3=lua:${lua_path}/ngrams.lua
Post4=ImportErrorFilter:ImportErrorFilterSettings
//Post5=IdxWriter:./output_post0.idx"

fi

### FAQ
if [ "$conn_name" == "inquira" ]; then

 conn_number="04"
 conn_type="httpc"

cfs_task="//Pre0=IdxWriter:./output_pre0.idx
Pre0=lua:${lua_path}/nofilter.lua
Pre1=TextToDocs:inquira_answer
Pre2=lua:${lua_path}/wms_doc_pre_processing.lua
Post0=ExtractMetadata:ExtractMetadataSettings
Post1=lua:${lua_path}/metatags.lua
Post2=lua:${lua_path}/wms_doc_post_processing.lua
Post3=lua:${lua_path}/gs_add_fields.lua
Post4=lua:${lua_path}/get_cat_values.lua
Post5=ImportErrorFilter:ImportErrorFilterSettings
Post6=IdxWriter:./output_post0.idx"

cfs_text="[inquira_answer]
MainRangeRegex0=(.*)

MainFieldName0=ANSWER
MainFieldRegex0=<div class=\"content\">(.*?)<div class=\"ATTACHMENTS\">

MainFieldName1=VIDEO
MainFieldRegex1=www.youtube.com\/embed\/(.*?)[\?|\"|&]

[ExtractMetadataSettings]
//FieldnamePrefix=meta_"

fi

if [ "$conn_name" == "faq" ]; then

 conn_number="18"
 conn_type="fsc"
 indexer_batch_size=5000
 indexer_time_interval=150
 number_of_threads=1
 xml="true"

xml_parsing="[XMLParsing]
ReferencePaths=none"

fi

### PES
if [ "$conn_name" == "pes" ]; then

 conn_number="01"
 conn_type="httpc"
 conn_subnames="navigation family offer"

cfs_task="//Pre0=IdxWriter:./output_pre0.idx
Pre0=lua:${lua_path}/nofilter.lua
Pre1=TextToDocs:pes_web_navigation_ttd
Pre2=TextToDocs:pes_web_family_ttd
Pre3=TextToDocs:pes_web_offer_ttd
Pre4=lua:${lua_path}/wms_doc_pre_processing.lua
Post0=ExtractMetadata:ExtractMetadataSettings
Post1=lua:${lua_path}/metatags.lua
Post2=lua:${lua_path}/wms_doc_post_processing.lua
Post3=lua:${lua_path}/gs_add_fields.lua
Post4=lua:${lua_path}/get_cat_values.lua
Post5=ImportErrorFilter:ImportErrorFilterSettings
//Post6=IdxWriter:./output_post0.idx"

cfs_text="[pes_web_navigation_ttd]
FieldMatchesName0=CONTENT_SOURCE
FieldMatchesRegex0=NAV_PES

MainRangeRegex0=(.*)

MainFieldName0=DRETITLE
MainFieldRegex0=<meta property=\"og:title\" content=\"(.*?)\" />

MainFieldName1=DRETITLE
MainFieldRegex1=<!--L1-NAME_BEGIN-->(.*?)<!--L1-NAME_END-->

MainFieldName2=Content1
MainFieldRegex2=<!--L2_BEGIN-->(.*?)<!--L2_END-->

MainFieldName3=Content2
MainFieldRegex3=<!--GEN_DESC_BEGIN-->(.*?)<!--GEN_DESC_END-->

MainFieldName4=PRODUCT_RANGE
MainFieldRegex4=<!--RANGE-NAME_BEGIN-->(.*?)<!--RANGE-NAME_END-->

MainFieldName5=Content3
MainFieldRegex5=<!--RANGE-SHORTDESC_BEGIN-->(.*?)<!--RANGE-SHORTDESC_END-->

MainFieldName6=Content4
MainFieldRegex6=<!--RANGE-LONGDESC_BEGIN-->(.*?)<!--RANGE-LONGDESC_END-->

MainFieldName7=IMAGE
MainFieldRegex7=<meta property=\"og:image\" content=\"(.*?)\"

[pes_web_family_ttd]
FieldMatchesName0=CONTENT_SOURCE
FieldMatchesRegex0=FAMILY_PES

MainRangeRegex0=(.*)

MainFieldName0=DRETITLE
MainFieldRegex0=<meta property=\"og:title\" content=\"(.*?)\" />

MainFieldName1=DRETITLE
MainFieldRegex1=<!--L2-NAME_BEGIN-->(.*?)<!--L2-NAME_END-->

MainFieldName2=Content1
MainFieldRegex2=<!--GEN_DESC_BEGIN-->(.*?)<!--GEN_DESC_END-->

MainFieldName3=PRODUCT_RANGE
MainFieldRegex3=<!--RANGE-NAME_BEGIN-->(.*?)<!--RANGE-NAME_END-->

MainFieldName4=Content2
MainFieldRegex4=<!--RANGE-SHORTDESC_BEGIN-->(.*?)<!--RANGE-SHORTDESC_END-->

MainFieldName5=IMAGE
MainFieldRegex5=<meta property=\"og:image\" content=\"(.*?)\"


[pes_web_offer_ttd]
FieldMatchesName0=CONTENT_SOURCE
FieldMatchesRegex0=OFFER_PES

MainRangeRegex0=<main class=\"sdl-main-global(.*)</main>

MainFieldName0=Content1
MainFieldRegex0=<!--BENEFITS_BEGIN-->\s*(.*?)\s*<!--BENEFITS_END-->

MainFieldName1=Content2
MainFieldRegex1=<!--APPLICATIONS_BEGIN-->\s*(.*?)\s*<!--APPLICATIONS_END-->

MainFieldName2=Content3
MainFieldRegex2=<!--PRESENTATION_BEGIN-->\s*(.*?)\s*<!--PRESENTATION_END-->

MainFieldName3=LONG_DESC
MainFieldRegex3=<!--LONGDESC_BEGIN-->\s*(.*?)\s*<!--LONGDESC_END-->

MainRangeRegex1=<!DOCTYPE HTML>(.*?)<!-- SDL9 -->

MainFieldName4=IMAGE
MainFieldRegex4=<meta property=\"og:image\" content=\"(.*?)\"

MainFieldName5=DRETITLE
MainFieldRegex5=<meta name=\"range-name\" content=\"(.*?)\"

[ExtractMetadataSettings]
//FieldnamePrefix=meta_"

fi

### SDL
if [ "$conn_name" == "sdl" ]; then

 conn_number="02"
 conn_type="httpc"
 conn_subnames="web lp"

cfs_task="//Pre0=IdxWriter:./output_pre0.idx
Pre0=lua:${lua_path}/nofilter.lua
Pre1=TextToDocs:sdl_web_ttd
Pre2=lua:${lua_path}/wms_doc_pre_processing.lua
Post0=ExtractMetadata:ExtractMetadataSettings
Post1=lua:${lua_path}/metatags.lua
Post2=lua:${lua_path}/wms_doc_post_processing.lua
Post3=lua:${lua_path}/gs_add_fields.lua
Post4=lua:${lua_path}/get_cat_values.lua
Post5=ImportErrorFilter:ImportErrorFilterSettings
Post6=IdxWriter:./output_post0.idx"

cfs_text="[sdl_web_ttd]
MainRangeRegex0=(.*)

MainFieldName0=ISERROR
MainFieldRegex0=<!-- ERROR_(.*?)-->

MainFieldName1=VIDEO
MainFieldRegex1=www.youtube.com\/embed\/(.*?)[\?|\"|&|']

MainFieldName2=Content1
MainFieldRegex2=<!-- Content - Article Slide Carousel -->(.*?)<!-- \/\/ Content - Article Slide Carousel \/\/ -->

MainFieldName3=Content2
MainFieldRegex3=<!-- Content - Article -->(.*?)<!-- \/\/ Content - Article \/\/ -->

MainFieldName4=Content3
MainFieldRegex4=<!-- Content - Flexible Blocks no Link -->(.*?)<!-- \/\/ Content - Flexible Blocks no Link \/\/ -->

MainFieldName5=Title
MainFieldRegex5=<title>(.*?)<\/title>

MainFieldName6=Title
MainFieldRegex6=<meta name=\"og:title\" property=\"og:title\" content=\"(.*?)\" />

MainFieldName7=DESCRIPTION
MainFieldRegex7=<meta name=\"og:description\" property=\"og:description\" content=\"(.*?)\" *\/>

MainFieldName8=DESCRIPTION
MainFieldRegex8=<meta property=\"og:description\" content=\"(.*?)\" *\/>

[ExtractMetadataSettings]
//FieldnamePrefix=meta_"

fi

### Product substitution
if [ "$conn_name" == "prodsub" ]; then

 conn_number="06"
 conn_type="fsc"

fi

### Products out-of-scope
if [ "$conn_name" == "oos" ]; then

 conn_number="07"
 conn_type="fsc"

fi

### Blogs
if [ "$conn_name" == "blog" ]; then

 conn_number="08"
 conn_type="httpc"

cfs_task="//Pre0=IdxWriter:./output_pre0.idx
Pre0=TextToDocs:blog_web
Pre1=lua:${lua_path}/wms_doc_pre_processing.lua
Post0=lua:${lua_path}/wms_doc_post_processing.lua
Post1=lua:${lua_path}/gs_add_fields.lua
Post2=lua:${lua_path}/get_cat_values.lua
Post3=ImportErrorFilter:ImportErrorFilterSettings
//Post4=IdxWriter:./output_post0.idx"

cfs_text="[blog_web]
MainRangeRegex0=(.*)

MainFieldName0=DRETITLE
MainFieldRegex0=<h1 class=\"entry-title single-title\" itemprop=\"headline\">(.*?)<\/h1>

MainFieldName1=DATE
MainFieldRegex1=<span class=\"entry-date\"><abbr class=\"published\" title=\"(.*?)T

MainFieldName2=CONTENT
MainFieldRegex2=<section class=\"entry-content clearfix\" itemprop=\"articleBody\">(.*?)<\/section>

MainFieldName3=KEYWORDS
MainFieldRegex3=<meta property=\"article:tag\" content=\"(.*?)\" \/>

MainFieldName4=IMAGE
MainFieldRegex4=<meta property=\"og:image\" content=\"(.*?)\" \/>

MainFieldName5=DESCRIPTION
MainFieldRegex5=<meta property=\"og:description\" content=\"(.*?)\" />"

fi

### Embedded video
if [ "$conn_name" == "video" ]; then

 conn_number="05"
 conn_type="fsc"
 conn_subnames="sdl faq"

fi

### Digest (for US only)
if [ "$conn_name" == "digest" ]; then

 conn_number="11"
 conn_type="fsc"
 conn_subnames="plus supplemental"

fi

### Thomasnet CAD files (for US only)
if [ "$conn_name" == "thomasnet" ]; then

 conn_number="13"
 conn_type="httpc"

cfs_task="//Pre0=IdxWriter:./output_pre0.idx
Pre0=TextToDocs:thomasnet_web
Pre1=lua:${lua_path}/wms_doc_pre_processing.lua
Post0=lua:${lua_path}/wms_doc_post_processing.lua
Post1=lua:${lua_path}/gs_add_fields.lua
Post2=lua:${lua_path}/get_cat_values.lua
Post3=ImportErrorFilter:ImportErrorFilterSettings
Post4=IdxWriter:./output_post0.idx"

cfs_text="[thomasnet_web]
MainRangeRegex0=(.*)

MainFieldName0=TITLE
MainFieldRegex0=<h1>(.*?)<\/h1>

MainFieldName1=SPEC
MainFieldRegex1=<span data-measure='general' itemprop=\"value\" >(.*?)<\/span>

MainFieldName2=IMAGE
MainFieldRegex2=<ul class=\"ad-thumb-list\">.*src=\"(\/ImgSmall.*?)\""

fi

### Traceparts CAD files (for US only)
if [ "$conn_name" == "traceparts" ]; then

 conn_number="14"
 conn_type="fsc"

fi

### CrossRef product substitution (for US & Canada only)
if [ "$conn_name" == "crossref" ]; then

 conn_number="10"
 conn_type="dbc"
 indexer_batch_size=5000
 indexer_time_interval=150

fi

### Clipsal (for Australia only)
if [ "$conn_name" == "clipsal" ]; then

 conn_number="15"
 conn_type="httpc"

cfs_task="//Pre0=IdxWriter:./output_pre0.idx
Pre0=TextToDocs:clipsal_web
Pre1=lua:${lua_path}/wms_doc_pre_processing.lua
Post0=lua:${lua_path}/wms_doc_post_processing.lua
Post1=lua:${lua_path}/gs_add_fields.lua
Post2=lua:${lua_path}/get_cat_values.lua
Post3=ImportErrorFilter:ImportErrorFilterSettings
Post4=IdxWriter:./output_post0.idx"

cfs_text="[clipsal_web]
MainRangeRegex0=(.*)

MainFieldName0=TITLE
MainFieldRegex0=<h5>Item Number: <span class=\"js-pp-catalog-number catalog-number-text\">(.*?)<\/span><\/h5>

MainFieldName1=DESCRIPTION
MainFieldRegex1=<div class=\"product-title-desc\">(.*?)<\/div>

MainFieldName2=IMAGE
MainFieldRegex2=<div class=\"image-holder\">.*?<img src=\"(.*?)\"

MainFieldName3=KEYWORDS
MainFieldRegex3=data-sheet-url=\"https:\/\/eref.se.com\/au\/en\/myse\/product-pdf\/(.*?)\""

fi

### Marketo
if [ "$conn_name" == "marketo" ]; then

 conn_number="22"
 conn_type="fsc"

cfs_task="//Pre0=IdxWriter:./output_pre0.idx
Pre0=TextToDocs:marketo_web
Pre1=lua:${lua_path}/wms_doc_pre_processing.lua
Post0=lua:${lua_path}/wms_doc_post_processing.lua
Post1=lua:${lua_path}/gs_add_fields.lua
Post2=lua:${lua_path}/get_cat_values.lua
Post3=ImportErrorFilter:ImportErrorFilterSettings
Post4=IdxWriter:./output_post0.idx"

cfs_text="[marketo_web]
MainRangeRegex0=(.*)

MainFieldName0=TITLE
MainFieldRegex0=<meta property=\"og:title\" content=\"(.*?)\" \/>

MainFieldName1=DESCRIPTION
MainFieldRegex1=<meta property=\"og:description\" content=\"(.*?)\"

MainFieldName2=IMAGE
MainFieldRegex2=<meta property=\"og:image\" content=\"(.*?)\""

fi

### MySE
if [ "$conn_name" == "myse" ]; then

 conn_number="20"
 conn_type="dbc"
 indexer_batch_size=5000
 indexer_time_interval=150

cfs_task="//Pre0=IdxWriter:./output_pre0.idx
Pre0=lua:${lua_path}/wms_doc_pre_processing.lua
Post0=lua:${lua_path}/wms_doc_post_processing.lua
Post1=ImportErrorFilter:ImportErrorFilterSettings
//Post2=IdxWriter:./output_post0.idx"

fi


### All WMS connectors operation
if [ "$conn_name" == "all" ];then

 connectors="bsldoc ecat inquira pes sdl blog oos prodsub video crossref thomasnet traceparts digest"
 for connector in $connectors
 do
  ./connector_deployment.sh $environment $zone $connector $options
 done
 exit

fi

### Connector validation
if [ "$conn_name" != "common" ]; then
 if [ "$conn_number" == "" ] || [ "$conn_type" == "" ]; then
  echo "$conn_name connector is not configured correctly"
  exit
 fi
fi

### Calculated values
conn_port="${port_prefix}${zone}${conn_number}"


##################################################################################################################
### Countriess configuration
##################################################################################################################

connector_jobs () {

### Default values
sdl_keys="b2b_standard,b2c_standard,partners_standard,aboutus_standard,b2b_brands"


if [ "$job_name" == "ae_en" ]; then
country_database=WMS_AE
bsl_country_code=GB 
country_code=AE
country_name="Gulf countries and Pakistan"
language_code=en
tgt_lang=English
domain_prefix=https://www.se.com/ae/en
sdl_region=apac1
region=apac1
myse_store_id=146
fi

if [ "$job_name" == "ar_en" ]; then
country_code=AR
language_code=en
tgt_lang=English
myse_store_id=120
fi

if [ "$job_name" == "ar_es" ]; then
country_database=WMS_AR
bsl_country_code=AR 
country_code=AR
country_name="Argentina,Uruguay,Paraguay"
language_code=es
tgt_lang=Spanish
domain_prefix=https://www.se.com/ar/es
sdl_region=amer1
region=game1
myse_store_id=120
fi

if [ "$job_name" == "at_de" ]; then
country_database=WMS_AT
bsl_country_code=DE 
country_code=AT
country_name="Austria"
language_code=de
tgt_lang=German
domain_prefix=https://www.se.com/at/de
sdl_region=euro2
region=emea1
myse_store_id=114
fi

if [ "$job_name" == "at_en" ]; then
country_code=AT
language_code=en
tgt_lang=English
myse_store_id=114
fi

if [ "$job_name" == "au_en" ]; then
country_database=WMS_AU
bsl_country_code=GB 
country_code=AU
country_name="Australia"
language_code=en
tgt_lang=English
domain_prefix=https://www.se.com/au/en
sdl_region=apac1
region=apac1
myse_store_id=127
fi

if [ "$job_name" == "be_en" ]; then
country_code=BE
language_code=en
tgt_lang=English
myse_store_id=129
fi

if [ "$job_name" == "be_fr" ]; then
country_database=WMS_BE
bsl_country_code=BE 
country_code=BE
country_name="Belgium"
language_code=fr
tgt_lang=French
domain_prefix=https://www.se.com/be/fr
sdl_region=euro2
region=emea1
myse_store_id=129
fi

if [ "$job_name" == "be_nl" ]; then
country_database=WMS_BE
bsl_country_code=BE 
country_code=BE
country_name="Belgium"
language_code=nl
tgt_lang=Dutch
domain_prefix=https://www.se.com/be/nl
sdl_region=euro2
region=emea1
myse_store_id=129
fi

if [ "$job_name" == "bg_bg" ]; then
country_database=WMS_BG
bsl_country_code=BG 
country_code=BG
country_name="Bulgaria"
language_code=bg
tgt_lang=Bulgarian
domain_prefix=https://www.se.com/bg/bg
sdl_region=euro3
region=emea2
myse_store_id=157
fi

if [ "$job_name" == "bg_en" ]; then
country_code=BG
language_code=en
tgt_lang=English
myse_store_id=157
fi

if [ "$job_name" == "br_en" ]; then
country_code=BR
language_code=en
tgt_lang=English
myse_store_id=150
fi

if [ "$job_name" == "br_pt" ]; then
country_database=WMS_BR
bsl_country_code=BR 
country_code=BR
country_name="Brasil"
language_code=pt
tgt_lang=Portuguese
domain_prefix=https://www.se.com/br/pt
sdl_region=amer1
region=game1
myse_store_id=150
fi

if [ "$job_name" == "ca_en" ]; then
country_database=WMS_CA
bsl_country_code=CA
country_code=CA
country_name="Canada"
crossref_country_name="CAN"
language_code=en
tgt_lang=English
domain_prefix=https://www.se.com/ca/en
sdl_region=amer1
region=game1
myse_store_id=106
fi

if [ "$job_name" == "ca_fr" ]; then
country_database=WMS_CA
bsl_country_code=CA 
country_code=CA
country_name="Canada"
crossref_country_name="CAN"
language_code=fr
tgt_lang=French
domain_prefix=https://www.se.com/ca/fr
sdl_region=amer1
region=game1
myse_store_id=106
fi

if [ "$job_name" == "ch_de" ]; then
country_database=WMS_CH
bsl_country_code=DE 
country_code=CH
country_name="Switzerland"
language_code=de
tgt_lang=German
domain_prefix=https://www.se.com/ch/de
sdl_region=euro3
region=emea1
myse_store_id=135
fi

if [ "$job_name" == "ch_fr" ]; then
country_database=WMS_CH
bsl_country_code=FR 
country_code=CH
country_name="Switzerland"
language_code=fr
tgt_lang=French
domain_prefix=https://www.se.com/ch/fr
sdl_region=euro3
region=emea1
myse_store_id=135
fi

if [ "$job_name" == "ch_en" ]; then
country_code=CH
language_code=en
tgt_lang=English
myse_store_id=135
fi

if [ "$job_name" == "ch_it" ]; then
country_code=CH
language_code=it
myse_store_id=135
tgt_lang=Italian
fi

if [ "$job_name" == "cl_en" ]; then
country_code=CL
language_code=en
tgt_lang=English
myse_store_id=119
fi

if [ "$job_name" == "cl_es" ]; then
country_database=WMS_CL
bsl_country_code=CL 
country_code=CL
country_name="Chile"
language_code=es
tgt_lang=Spanish
domain_prefix=https://www.se.com/cl/es
sdl_region=amer1
region=game1
myse_store_id=119
fi

if [ "$job_name" == "cn_en" ]; then
country_code=CN
language_code=en
tgt_lang=English
myse_store_id=107
fi

if [ "$job_name" == "cn_zh" ]; then
country_database=WMS_CN
bsl_country_code=CN 
country_code=CN
country_name="China"
language_code=zh
tgt_lang=Chinese
domain_prefix=https://www.schneider-electric.cn/zh
sdl_region=apac1
region=apac1
myse_store_id=107
fi

if [ "$job_name" == "co_en" ]; then
country_code=CO
language_code=en
tgt_lang=English
myse_store_id=156
fi

if [ "$job_name" == "co_es" ]; then
country_database=WMS_CO
bsl_country_code=CO 
country_code=CO
country_name="Colombia,Ecuador,Venezuela"
language_code=es
tgt_lang=Spanish
domain_prefix=https://www.se.com/co/es
sdl_region=amer1
region=game1
myse_store_id=156
fi

if [ "$job_name" == "cr_en" ]; then
country_code=CR
language_code=en
tgt_lang=English
myse_store_id=165
fi

if [ "$job_name" == "cr_es" ]; then
country_database=WMS_CR
bsl_country_code=CR 
country_code=CR
country_name="Central America"
language_code=es
tgt_lang=Spanish
domain_prefix=https://www.se.com/cr/es
sdl_region=amer1
region=game1
myse_store_id=165
fi

if [ "$job_name" == "cz_cs" ]; then
country_database=WMS_CZ
bsl_country_code=CZ
country_code=CZ
country_name="Czech Republic"
language_code=cs
tgt_lang=Czech
domain_prefix=https://www.se.com/cz/cs
sdl_region=euro2
region=emea1
#sdl_keys="b2b_standard,b2c_products,partners_standard,aboutus_standard,b2b_brands"
myse_store_id=161
fi

if [ "$job_name" == "cz_de" ]; then
country_code=CZ
language_code=de
tgt_lang=German
myse_store_id=161
fi

if [ "$job_name" == "cz_en" ]; then
country_code=CZ
language_code=en
tgt_lang=English
myse_store_id=161
fi

if [ "$job_name" == "cz_ru" ]; then
country_code=CZ
language_code=ru
tgt_lang=Russian
myse_store_id=161
fi

if [ "$job_name" == "de_de" ]; then
country_database=WMS_DE
bsl_country_code=DE 
country_code=DE
country_name="Germany"
language_code=de
tgt_lang=German
domain_prefix=https://www.se.com/de/de
sdl_region=euro2
region=emea1
myse_store_id=134
fi

if [ "$job_name" == "de_en" ]; then
country_code=DE
language_code=en
tgt_lang=English
myse_store_id=134
fi

if [ "$job_name" == "dk_da" ]; then
country_database=WMS_DK
bsl_country_code=DK 
country_code=DK
country_name="Denmark"
language_code=da
tgt_lang=Danish
domain_prefix=https://www.se.com/dk/da
sdl_region=euro2
region=emea1
#sdl_keys="b2b_standard,partners_standard,aboutus_standard,b2b_brands"
myse_store_id=123
fi

if [ "$job_name" == "dk_en" ]; then
country_code=DK
language_code=en
tgt_lang=English
myse_store_id=123
fi

if [ "$job_name" == "dz_en" ]; then
country_code=DZ
language_code=en
tgt_lang=English
myse_store_id=151
fi

if [ "$job_name" == "dz_fr" ]; then
country_database=WMS_DZ
bsl_country_code=FR 
country_code=DZ
country_name="Algeria"
language_code=fr
tgt_lang=French
domain_prefix=https://www.se.com/dz/fr
sdl_region=euro4
region=emea2
myse_store_id=151
fi

if [ "$job_name" == "ee_et" ]; then
country_database=WMS_EE
bsl_country_code=EE 
country_code=EE
country_name="Estonia"
language_code=et
tgt_lang=Estonian
domain_prefix=https://www.se.com/ee/et
sdl_region=euro4
region=emea2
fi

if [ "$job_name" == "eg_ar" ]; then
country_database=WMS_EG
bsl_country_code=EG 
country_code=EG
country_name="Egypt"
language_code=ar
tgt_lang=Arabic
domain_prefix=https://www.se.com/eg/ar
sdl_region=euro2
region=emea2
sdl_keys="b2b_standard,b2b_exception,b2c_standard,partners_standard,aboutus_standard,b2b_brands"
fi

if [ "$job_name" == "eg_en" ]; then
country_database=WMS_EG
bsl_country_code=GB 
country_code=EG
country_name="Egypt"
language_code=en
tgt_lang=English
domain_prefix=https://www.se.com/eg/en
sdl_region=euro2
region=emea2
myse_store_id=144
fi

if [ "$job_name" == "el_en" ]; then
country_code=EL
language_code=en
tgt_lang=English
myse_store_id=163
fi

if [ "$job_name" == "el_no" ]; then
country_code=EL
language_code=no
tgt_lang=Norwegian
myse_store_id=163
fi

if [ "$job_name" == "es_ar" ]; then
country_code=ES
language_code=ar
tgt_lang=Arabic
myse_store_id=104
fi

if [ "$job_name" == "es_en" ]; then
country_code=ES
language_code=en
tgt_lang=English
myse_store_id=104
fi

if [ "$job_name" == "es_es" ]; then
country_database=WMS_ES
bsl_country_code=ES 
country_code=ES
country_name="España"
language_code=es
tgt_lang=Spanish
domain_prefix=https://www.se.com/es/es
sdl_region=euro1
region=emea2
#sdl_keys="b2b_standard,b2c_standard,aboutus_standard,b2b_brands"
myse_store_id=104
fi

if [ "$job_name" == "fi_fi" ]; then
country_database=WMS_FI
bsl_country_code=FI 
country_code=FI
country_name="Finland"
language_code=fi
tgt_lang=Finnish
domain_prefix=https://www.se.com/fi/fi
sdl_region=euro2
region=emea1
myse_store_id=124
fi

if [ "$job_name" == "fi_en" ]; then
country_code=FI
language_code=en
tgt_lang=English
myse_store_id=124
fi

if [ "$job_name" == "fi_sv" ]; then
country_code=FI
language_code=sv
tgt_lang=Swedish
myse_store_id=124
fi

if [ "$job_name" == "fr_en" ]; then
country_code=FR
language_code=en
tgt_lang=English
myse_store_id=102
fi

if [ "$job_name" == "fr_fr" ]; then
country_database=WMS_FR
bsl_country_code=FR 
country_code=FR
country_name="France"
language_code=fr
tgt_lang=French
domain_prefix=https://www.se.com/fr/fr
sdl_region=euro1
region=emea2
sdl_keys="b2b_standard,b2b_exception,b2c_standard,partners_standard,aboutus_standard,b2b_brands"
myse_store_id=102
fi

if [ "$job_name" == "gb_en" ]; then
country_code=UK
language_code=en
tgt_lang=English
myse_store_id=108
fi

if [ "$job_name" == "gr_el" ]; then
country_database=WMS_GR
bsl_country_code=GR 
country_code=GR
country_name="Greece"
language_code=el
tgt_lang=Greek
domain_prefix=https://www.se.com/gr/el
sdl_region=euro2
region=emea2
myse_store_id=160
fi

if [ "$job_name" == "gr_en" ]; then
country_code=GR
language_code=en
tgt_lang=English
myse_store_id=160
fi

if [ "$job_name" == "hk_en" ]; then
country_database=WMS_HK
bsl_country_code=GB 
country_code=HK
country_name="Honk Kong"
language_code=en
tgt_lang=English
domain_prefix=https://www.se.com/hk/en
sdl_region=apac1
region=apac1
myse_store_id=131
fi

if [ "$job_name" == "hk_zz" ]; then
country_code=HK
language_code=zz
tgt_lang=Chinese
myse_store_id=131
fi

if [ "$job_name" == "hr_hr" ]; then
country_database=WMS_HR
bsl_country_code=HR 
country_code=HR
country_name="Croatia"
language_code=hr
tgt_lang=Croatian
domain_prefix=https://www.se.com/hr/hr
sdl_region=euro3
region=emea1
myse_store_id=115
fi

if [ "$job_name" == "hr_en" ]; then
country_code=HR
language_code=en
tgt_lang=English
myse_store_id=115
fi

if [ "$job_name" == "hu_en" ]; then
country_code=HU
language_code=en
tgt_lang=English
myse_store_id=113
fi

if [ "$job_name" == "hu_hu" ]; then
country_database=WMS_HU
bsl_country_code=HU 
country_code=HU
country_name="Hungary"
language_code=hu
tgt_lang=Hungarian
domain_prefix=https://www.se.com/hu/hu
sdl_region=euro3
region=emea1
myse_store_id=113
fi

if [ "$job_name" == "id_id" ]; then
country_database=WMS_ID
bsl_country_code=ID 
country_code=ID
country_name="Indonesia"
language_code=id
tgt_lang=Indonesian
domain_prefix=https://www.se.com/id/id
sdl_region=apac1
region=apac1
myse_store_id=136
fi

if [ "$job_name" == "id_en" ]; then
country_database=WMS_ID
bsl_country_code=GB 
country_code=ID
country_name="Indonesia"
language_code=en
tgt_lang=English
domain_prefix=https://www.se.com/id/en
sdl_region=apac1
region=apac1
myse_store_id=136
fi

if [ "$job_name" == "ie_en" ]; then
country_database=WMS_IE
bsl_country_code=GB 
country_code=IE
country_name="Ireland"
language_code=en
tgt_lang=English
domain_prefix=https://www.se.com/ie/en
sdl_region=euro3
region=emea1
myse_store_id=137
fi

if [ "$job_name" == "il_en" ]; then
country_database=WMS_IL
bsl_country_code=IL
country_code=IL
country_name="Israel"
language_code=en
tgt_lang=English
domain_prefix=https://www.se.com/il/en
sdl_region=euro2
region=emea2
myse_store_id=159
fi

if [ "$job_name" == "il_he" ]; then
country_database=WMS_IL
bsl_country_code=IL 
country_code=IL
country_name="Israel"
language_code=he
tgt_lang=Hebrew
domain_prefix=https://www.se.com/il/en
sdl_region=euro2
region=emea2
sdl_keys="b2b_standard,b2b_exception,b2c_standard,partners_standard,aboutus_standard,b2b_brands"
myse_store_id=159
fi

if [ "$job_name" == "in_en" ]; then
country_database=WMS_IN
bsl_country_code=GB 
country_code=IN
country_name="India"
language_code=en
tgt_lang=English
domain_prefix=https://www.se.com/in/en
sdl_region=apac1
region=apac1
myse_store_id=111
fi

if [ "$job_name" == "int_en" ]; then
country_code=INT
language_code=en
tgt_lang=English
myse_store_id=153
fi

if [ "$job_name" == "int_es" ]; then
country_code=INT
language_code=es
tgt_lang=Spanish
myse_store_id=153
fi

if [ "$job_name" == "int_fr" ]; then
country_code=INT
language_code=fr
tgt_lang=French
myse_store_id=153
fi

if [ "$job_name" == "it_it" ]; then
country_database=WMS_IT
bsl_country_code=IT 
country_code=IT
country_name="Italy"
language_code=it
tgt_lang=Italian
domain_prefix=https://www.se.com/it/it
sdl_region=euro1
region=emea1
fi

if [ "$job_name" == "jp_en" ]; then
country_code=JP
language_code=en
tgt_lang=English
myse_store_id=149
fi

if [ "$job_name" == "jp_ja" ]; then
country_database=WMS_JP
bsl_country_code=JP 
country_code=JP
country_name="Japan"
language_code=ja
tgt_lang=Japanese
domain_prefix=https://www.se.com/jp/ja
sdl_region=apac2
region=apac1
#sdl_keys="b2b_standard,partners_standard,aboutus_standard,b2b_brands"
myse_store_id=149
fi

if [ "$job_name" == "jp2_en" ]; then
country_code=JP2
language_code=en
tgt_lang=English
myse_store_id=166
fi

if [ "$job_name" == "jp2_ja" ]; then
country_code=JP2
language_code=ja
tgt_lang=Japanese
myse_store_id=166
fi

if [ "$job_name" == "kr_en" ]; then
country_code=KR
language_code=en
tgt_lang=English
myse_store_id=139
fi

if [ "$job_name" == "kr_ko" ]; then
country_database=WMS_KR
bsl_country_code=KR 
country_code=KR
country_name="Korea, South"
language_code=ko
tgt_lang=Korean
domain_prefix=https://www.se.com/kr/ko
sdl_region=apac2
region=apac1
myse_store_id=139
fi

if [ "$job_name" == "kz_en" ]; then
country_code=KZ
language_code=en
tgt_lang=English
myse_store_id=148
fi

if [ "$job_name" == "kz_ru" ]; then
country_database=WMS_KZ
bsl_country_code=RU 
country_code=KZ
country_name="Kazakhstan"
language_code=ru
tgt_lang=Russian
domain_prefix=https://www.se.com/kz/ru
sdl_region=apac2
region=apac1
myse_store_id=148
fi

if [ "$job_name" == "lt_lt" ]; then
country_database=WMS_LT
bsl_country_code=LT 
country_code=LT
country_name="Lithuania"
language_code=lt
tgt_lang=Lithuanian
domain_prefix=https://www.se.com/lt/lt
sdl_region=euro4
region=emea2
#sdl_keys="b2b_standard,b2c_standard,aboutus_standard,b2b_brands"
fi

if [ "$job_name" == "lv_lv" ]; then
country_database=WMS_LV
bsl_country_code=LV 
country_code=LV
country_name="Latvia"
language_code=lv
tgt_lang=Latvian
domain_prefix=https://www.se.com/lv/lv
sdl_region=euro4
region=emea2
#sdl_keys="b2b_standard,b2c_standard,aboutus_standard,b2b_brands"
fi

if [ "$job_name" == "ma_en" ]; then
country_code=MA
language_code=en
tgt_lang=English
myse_store_id=154
fi

if [ "$job_name" == "ma_fr" ]; then
country_code=MA
language_code=fr
tgt_lang=French
myse_store_id=154
fi

if [ "$job_name" == "mx_en" ]; then
country_code=MX
language_code=en
tgt_lang=English
myse_store_id=164
fi

if [ "$job_name" == "mx_es" ]; then
country_database=WMS_MX
bsl_country_code=MX 
country_code=MX
country_name="Mexico"
language_code=es
tgt_lang=Spanish
domain_prefix=https://www.se.com/mx/es
sdl_region=amer1
region=game1
myse_store_id=164
fi

if [ "$job_name" == "my_en" ]; then
country_database=WMS_MY
bsl_country_code=GB 
country_code=MY
country_name="Malaysia"
language_code=en
tgt_lang=English
domain_prefix=https://www.se.com/my/en
sdl_region=apac2
region=apac1
myse_store_id=141
fi

if [ "$job_name" == "ng_en" ]; then
country_database=WMS_NG
bsl_country_code=GB 
country_code=NG
country_name="Nigeria"
language_code=en
tgt_lang=English
domain_prefix=https://www.se.com/ng/en
sdl_region=euro3
region=emea2
myse_store_id=152
fi

if [ "$job_name" == "nl_en" ]; then
country_code=NL
language_code=en
tgt_lang=English
myse_store_id=133
fi

if [ "$job_name" == "nl_nl" ]; then
country_database=WMS_NL
bsl_country_code=NL 
country_code=NL
country_name="Netherlands"
language_code=nl
tgt_lang=Dutch
domain_prefix=https://www.se.com/nl/nl
sdl_region=euro2
region=emea1
#sdl_keys="b2b_standard,b2c_products,partners_standard,aboutus_standard,b2b_brands"
myse_store_id=133
fi

if [ "$job_name" == "no_en" ]; then
country_code=NO
language_code=en
tgt_lang=English
myse_store_id=122
fi

if [ "$job_name" == "no_no" ]; then
country_database=WMS_NO
bsl_country_code=NO 
country_code=NO
country_name="Norway"
language_code=no
tgt_lang=Norwegian
domain_prefix=https://www.se.com/no/no
sdl_region=euro3
region=emea1
#sdl_keys="b2b_standard,partners_standard,aboutus_standard,b2b_brands"
myse_store_id=122
fi

if [ "$job_name" == "nz_en" ]; then
country_database=WMS_NZ
bsl_country_code=GB 
country_code=NZ
country_name="New Zealand"
language_code=en
tgt_lang=English
domain_prefix=https://www.se.com/nz/en
sdl_region=apac1
region=apac1
myse_store_id=128
fi

if [ "$job_name" == "pe_en" ]; then
country_code=PE
language_code=en
tgt_lang=English
myse_store_id=140
fi

if [ "$job_name" == "pe_es" ]; then
country_database=WMS_PE
bsl_country_code=PE 
country_code=PE
country_name="Peru"
language_code=es
tgt_lang=Spanish
domain_prefix=https://www.se.com/pe/es
sdl_region=amer1
region=game1
myse_store_id=140
fi

if [ "$job_name" == "ph_en" ]; then
country_database=WMS_PH
bsl_country_code=GB 
country_code=PH
country_name="Philippines"
language_code=en
tgt_lang=English
domain_prefix=https://www.se.com/ph/en
sdl_region=apac2
region=apac1
myse_store_id=143
fi

if [ "$job_name" == "pl_pl" ]; then
country_database=WMS_PL
bsl_country_code=PL 
country_code=PL
country_name="Poland"
language_code=pl
tgt_lang=Polish
domain_prefix=https://www.se.com/pl/pl
sdl_region=euro2
region=emea1
myse_store_id=162
fi

if [ "$job_name" == "pl_en" ]; then
country_code=PL
language_code=en
tgt_lang=English
myse_store_id=162
fi

if [ "$job_name" == "pt_pt" ]; then
country_database=WMS_PT
bsl_country_code=PT 
country_code=PT
country_name="Portugal"
language_code=pt
tgt_lang=Portuguese
domain_prefix=https://www.se.com/pt/pt
sdl_region=euro2
region=emea1
myse_store_id=167
fi

if [ "$job_name" == "pt_en" ]; then
country_code=PT
language_code=en
tgt_lang=English
myse_store_id=167
fi

if [ "$job_name" == "ro_en" ]; then
country_code=RO
language_code=en
tgt_lang=English
myse_store_id=116
fi

if [ "$job_name" == "ro_ro" ]; then
country_database=WMS_RO
bsl_country_code=RO 
country_code=RO
country_name="Romania"
language_code=ro
tgt_lang=Romanian
domain_prefix=https://www.se.com/ro/ro
sdl_region=euro2
region=emea2
myse_store_id=116
fi

if [ "$job_name" == "rs_en" ]; then
country_code=RS
language_code=en
tgt_lang=English
myse_store_id=158
fi

if [ "$job_name" == "rs_sr" ]; then
country_database=WMS_RS
bsl_country_code=RS 
country_code=RS
country_name="Serbia"
language_code=sr
tgt_lang=Serbian
domain_prefix=https://www.se.com/rs/sr
sdl_region=euro2
region=emea2
myse_store_id=158
fi

if [ "$job_name" == "ru_ru" ]; then
country_database=WMS_RU
bsl_country_code=RU 
country_code=RU
country_name="Russia"
language_code=ru
tgt_lang=Russian
domain_prefix=https://www.se.com/ru/ru
sdl_region=euro1
region=emea2
myse_store_id=125
fi

if [ "$job_name" == "ru_en" ]; then
country_code=RU
language_code=en
tgt_lang=English
myse_store_id=125
fi

if [ "$job_name" == "sa_ar" ]; then
country_database=WMS_SA
bsl_country_code=SA 
country_code=SA
country_name="Saudi Arabia"
language_code=ar
tgt_lang=Arabic
domain_prefix=https://www.se.com/sa/ar
sdl_region=apac1
region=apac1
sdl_keys="b2b_standard,b2b_exception,b2c_standard,partners_standard,aboutus_standard,b2b_brands"
fi

if [ "$job_name" == "sa_en" ]; then
country_database=WMS_SA
bsl_country_code=GB 
country_code=SA
country_name="Saudi Arabia"
language_code=en
tgt_lang=English
domain_prefix=https://www.se.com/sa/en
sdl_region=apac1
region=apac1
myse_store_id=145
fi

if [ "$job_name" == "se_en" ]; then
country_code=SE
language_code=en
tgt_lang=English
myse_store_id=121
fi

if [ "$job_name" == "se_sv" ]; then
country_database=WMS_SE
bsl_country_code=SE 
country_code=SE
country_name="Sweden"
language_code=sv
tgt_lang=Swedish
domain_prefix=https://www.se.com/se/sv
sdl_region=euro1
region=emea1
myse_store_id=121
fi

if [ "$job_name" == "sg_en" ]; then
country_database=WMS_SG
bsl_country_code=GB 
country_code=SG
country_name="Singapore"
language_code=en
tgt_lang=English
domain_prefix=https://www.se.com/sg/en
sdl_region=apac1
region=apac1
myse_store_id=147
fi

if [ "$job_name" == "si_sl" ]; then
country_database=WMS_SI
bsl_country_code=SI 
country_code=SI
country_name="Slovenia"
language_code=sl
tgt_lang=Slovenian
domain_prefix=https://www.se.com/si/sl
sdl_region=euro2
region=emea1
myse_store_id=117
fi

if [ "$job_name" == "si_en" ]; then
country_code=SI
language_code=en
tgt_lang=English
myse_store_id=117
fi

if [ "$job_name" == "sk_sk" ]; then
country_database=WMS_SK
bsl_country_code=SK 
country_code=SK
country_name="Slovakia"
language_code=sk
tgt_lang=Slovak
domain_prefix=https://www.se.com/sk/sk
sdl_region=euro3
region=emea1
myse_store_id=118
fi

if [ "$job_name" == "sk_en" ]; then
country_code=SK
language_code=en
tgt_lang=English
myse_store_id=118
fi

if [ "$job_name" == "th_en" ]; then
country_database=WMS_TH
bsl_country_code=GB 
country_code=TH
country_name="Thailand"
language_code=en
tgt_lang=English
domain_prefix=https://www.se.com/th/en
sdl_region=apac2
region=apac1
myse_store_id=138
fi

if [ "$job_name" == "th_th" ]; then
country_database=WMS_TH
bsl_country_code=TH 
country_code=TH
country_name="Thailand"
language_code=th
tgt_lang=Thai
domain_prefix=https://www.se.com/th/th
sdl_region=apac2
region=apac1
sdl_keys="b2b_standard,b2b_exception,b2c_standard,partners_standard,aboutus_standard,b2b_brands"
myse_store_id=138
fi

if [ "$job_name" == "tr_tr" ]; then
country_database=WMS_TR
bsl_country_code=TR 
country_code=TR
country_name="Turkey"
language_code=tr
tgt_lang=Turkish
domain_prefix=https://www.se.com/tr/tr
sdl_region=euro1
region=emea2
myse_store_id=109
fi

if [ "$job_name" == "tr_en" ]; then
country_code=TR
language_code=en
tgt_lang=English
myse_store_id=109
fi

if [ "$job_name" == "tr_ru" ]; then
country_code=TR
language_code=ru
tgt_lang=Russian
myse_store_id=109
fi

if [ "$job_name" == "tw_en" ]; then
country_code=TW
language_code=en
tgt_lang=English
myse_store_id=132
fi

if [ "$job_name" == "tw_zh" ]; then
country_database=WMS_TW
bsl_country_code=TW 
country_code=TW
country_name="Taiwan"
language_code=zh
tgt_lang=Chinese
domain_prefix=https://www.se.com/tw/zh
sdl_region=apac2
region=apac1
myse_store_id=132
fi

if [ "$job_name" == "ua_en" ]; then
country_code=UA
language_code=en
tgt_lang=English
myse_store_id=126
fi

if [ "$job_name" == "ua_ru" ]; then
country_database=WMS_UA
bsl_country_code=RU 
country_code=UA
country_name="Ukraine"
language_code=ru
tgt_lang=Russian
domain_prefix=https://www.se.com/ua/ru
sdl_region=euro3
region=emea2
myse_store_id=126
fi

if [ "$job_name" == "ua_uk" ]; then
country_database=WMS_UA
bsl_country_code=UA 
country_code=UA
country_name="Ukraine"
language_code=uk
tgt_lang=Ukrainian
domain_prefix=https://www.se.com/ua/uk
sdl_region=euro3
region=emea2
myse_store_id=126
fi

if [ "$job_name" == "uk_en" ]; then
country_database=WMS_UK
bsl_country_code=GB 
country_code=UK
country_name="UK"
language_code=en
tgt_lang=English
domain_prefix=https://www.se.com/uk/en
sdl_region=euro1
region=emea1
fi

if [ "$job_name" == "us_en" ]; then
country_database=WMS_US
bsl_country_code=US 
country_code=US
country_name="USA"
crossref_country_name="USA"
language_code=en
tgt_lang=English
domain_prefix=https://www.se.com/us/en
sdl_region=amer1
region=game1
myse_store_id=105
fi

if [ "$job_name" == "us2_en" ]; then
country_code=US2
language_code=en
tgt_lang=English
myse_store_id=101
fi

if [ "$job_name" == "vn_en" ]; then
country_database=WMS_VN
bsl_country_code=GB 
country_code=VN
country_name="Vietnam"
language_code=en
tgt_lang=English
domain_prefix=https://www.se.com/vn/en
sdl_region=apac2
region=apac1
myse_store_id=142
fi

if [ "$job_name" == "vn_vi" ]; then
country_database=WMS_VN
bsl_country_code=VN 
country_code=VN
country_name="Vietnam"
language_code=vi
tgt_lang=Vietnamese
domain_prefix=https://www.se.com/vn/vi
sdl_region=apac2
region=apac1
myse_store_id=142
fi

if [ "$job_name" == "ww_en" ]; then
country_database=WMS_WW
bsl_country_code=GB 
country_code=WW
country_name="Global"
language_code=en
tgt_lang=English
domain_prefix=https://www.se.com/ww/en
sdl_region=corp
region=game1
fi

if [ "$job_name" == "ww_fr" ]; then
country_database=WMS_WW
bsl_country_code=FR
country_code=WW
country_name="Global"
language_code=fr
tgt_lang=French
domain_prefix=https://www.se.com/ww/fr
sdl_region=corp
region=game1
#sdl_keys="b2b_standard,partners_standard,aboutus_standard,b2b_brands"
fi

if [ "$job_name" == "xf_fr" ]; then
country_database=WMS_XF
bsl_country_code=FR 
country_code=XF
country_name="Afrique Francophone"
language_code=fr
tgt_lang=French
domain_prefix=https://www.se.com/africa/fr
sdl_region=euro3
region=emea2
fi

if [ "$job_name" == "za_en" ]; then
country_database=WMS_ZA
bsl_country_code=GB 
country_code=ZA
country_name="South Africa"
language_code=en
tgt_lang=English
domain_prefix=https://www.se.com/za/en
sdl_region=euro1
region=emea2
myse_store_id=155
fi

###Calculated values
country_code_lower=$(echo "$country_code" | tr '[:upper:]' '[:lower:]')
language_code_upper=$(echo "$language_code" | tr '[:lower:]' '[:upper:]')

##################################################################################################################
# Jobs configuration
##################################################################################################################

conn_job_line[0]="IndexDatabase=${country_database}"

if [ "$conn_name" == "bsldoc" ]; then
skip_job="false"
conn_job_lines=7
#conn_job_line[1]="DirectoryPathCSVs=${ddc_preprocessor_path}/ddc_out/dest/${country_code}/${language_code}_${bsl_country_code}"
conn_job_line[1]="DirectoryPathCSVs=${ddc_preprocessor_path}/${country_code_lower}/${language_code}/${conn_name}"
conn_job_line[2]="IngestAction2=META:cfg_keys_csv=ddc"
conn_job_line[3]="IngestAction3=META:LANGUAGE_CODE=${language_code}"
conn_job_line[4]="IngestAction4=META:COUNTRY_CODE=${country_code}"
conn_job_line[5]="IngestAction5=META:LOCALE_CODE=${language_code}_${bsl_country_code}"
conn_job_line[6]="IngestAction6=META:TGT_LANG=${tgt_lang}"
conn_job_line[7]="IngestAction7=META:SITE_DOMAIN_LOCALE=${domain_prefix}"

### exceptions ###
 if [ "$job_name" == "uk_en" ]; then
#  conn_job_line[1]="DirectoryPathCSVs=${ddc_preprocessor_path}/ddc_out/dest/${bsl_country_code}/${language_code}_${bsl_country_code}"
  conn_job_line[4]="IngestAction4=META:COUNTRY_CODE=${bsl_country_code}"
 fi

 if [ "$job_name" == "fr_fr" ]; then
  conn_job_line[2]="IngestAction2=META:cfg_keys_csv=ddc,bsldoc_fs_fr"
 fi

 if [ "$job_name" == "ww_fr" ]; then
  conn_job_line[4]="IngestAction4=META:COUNTRY_CODE=${bsl_country_code}"
  conn_job_line[7]="IngestAction6=META:SITE_DOMAIN_LOCALE=https://www.se.com/fr/fr"
 fi
fi

if [ "$conn_name" == "ecat" ]; then
skip_job="false"
conn_job_lines=6
### Temporarily
conn_subname=v2

#conn_job_line[1]="DirectoryPathCSVs=${onepop_preprocessor_path}/dest/${country_code}/${language_code}_${bsl_country_code}"
conn_job_line[1]="DirectoryPathCSVs=${onepop_preprocessor_path}/${country_code_lower}/${language_code}/${conn_name}"
conn_job_line[2]="IngestAction2=META:cfg_keys_csv=ecat_fs_v2"
conn_job_line[3]="IngestAction3=META:TARGET_CODE=CO${country_code}"
conn_job_line[4]="IngestAction4=META:LANGUAGE_CODE=${language_code}"
conn_job_line[5]="IngestAction5=META:TGT_LANG=${tgt_lang}"
conn_job_line[6]="IngestAction6=META:SITE_DOMAIN_LOCALE=${domain_prefix}"

### exceptions ###
 if [ "$job_name" == "uk_en" ]; then
#  conn_job_line[1]="DirectoryPathCSVs=${ddc_preprocessor_path}/ddc_out/dest/${bsl_country_code}/${language_code}_${bsl_country_code}"
  conn_job_line[3]="IngestAction3=META:TARGET_CODE=CO${bsl_country_code}"
 fi

 if [ "$job_name" == "ww_en" ]; then
#  conn_job_line[1]="DirectoryPathCSVs=${onepop_preprocessor_path}/dest/${country_code}/com_en"
  conn_job_line[2]="IngestAction2=META:cfg_keys_csv=ecat_fs_v2,ecat_fs_ww_en"
 fi

 if [ "$job_name" == "ww_fr" ]; then
  conn_job_line[3]="IngestAction3=META:TARGET_CODE=COFR"
  conn_job_line[6]="IngestAction6=META:SITE_DOMAIN_LOCALE=https://www.se.com/fr/fr"
 fi

 if [ "$job_name" == "sa_ar" ]; then
  conn_job_line[6]="IngestAction6=META:SITE_DOMAIN_LOCALE=https://www.se.com/sa/en"
 fi

fi

if [ "$conn_name" == "inquira" ]; then
skip_job="false"
skipped_jobs="at_de ch_de ch_fr eg_ar il_en il_he sa_ar ua_uk ww_fr de_de jp_ja fr_fr uk_en us_en ie_en in_en ca_en ca_fr au_en be_fr be_nl cn_zh es_es no_no ru_ru se_sv ww_en"
 for skip in $skipped_jobs; do
  if [ "$job_name" == "$skip" ]; then
    skip_job="true"
  fi
 done 

conn_job_lines=3
conn_job_line[1]="URL=${domain_prefix}/faqs/sitemap.xml"
conn_job_line[2]="IngestAction2=META:cfg_keys_csv=inquira_common"
conn_job_line[3]="IngestAction3=META:TGT_LANG=${tgt_lang}"

 if [ "$job_name" == "bg_bg" ] || [ "$job_name" == "ee_et" ] || [ "$job_name" == "hr_hr" ] || [ "$job_name" == "lt_lt" ] || [ "$job_name" == "lv_lv" ] || [ "$job_name" == "rs_sr" ] || [ "$job_name" == "si_sl" ]; then
  conn_job_lines=4
  conn_job_line[1]="URL=https://www.se.com/ww/en/faqs/sitemap.xml"
  conn_job_line[2]="IngestAction2=META:cfg_keys_csv=inquira_common,inquira_corp"
  conn_job_line[3]="IngestAction3=META:TGT_LANG=${tgt_lang}"
  conn_job_line[4]="IngestAction4=META:LANGUAGE_CODE=${language_code}"
 fi

 if [ "$job_name" == "ca_en" ] || [ "$job_name" == "ie_en" ]; then
  conn_job_lines=4
  conn_job_line[1]="URL0=${domain_prefix}/faqs/sitemap.xml"
  conn_job_line[2]="URL1=https://www.se.com/ww/en/faqs/sitemap.xml"
  conn_job_line[3]="IngestAction2=META:cfg_keys_csv=inquira_common"
  conn_job_line[4]="IngestAction3=META:TGT_LANG=${tgt_lang}"
 fi

 if [ "$job_name" == "cr_es" ]; then
  conn_job_line[1]="URL=https://www.se.com/mx/es/faqs/sitemap.xml"
 fi

fi

if [ "$conn_name" == "faq" ]; then
skip_job="false"
# allowed_jobs="at_de ch_de ch_fr de_de jp_ja fr_fr uk_en us_en ie_en in_en ca_en ca_fr au_en be_fr be_nl cn_zh es_es no_no ru_ru se_sv ww_en ww_fr il_he"
skipped_jobs="cn_zh ww_fr"
 for skip in $skipped_jobs; do
  if [ "$job_name" == "$skip" ]; then
    skip_job="true"
  fi
 done

conn_job_lines=4
conn_job_line[1]="DirectoryPathCSVs=${faq_preprocessor_path}/${country_code_lower}/${language_code}/${conn_name}"
conn_job_line[2]="IngestAction2=META:cfg_keys_csv=faq"
conn_job_line[3]="IngestAction3=META:TGT_LANG=${tgt_lang}"
conn_job_line[4]="IngestAction4=META:LANGUAGE_CODE=${language_code}"

global_jobs="au_en hk_en in_en my_en nz_en sg_en vn_en ae_en id_en ph_en sa_en th_en ie_en ng_en uk_en hr_hr rs_sr si_sl eg_en za_en bg_bg ee_et lt_lt lv_lv cr_es ca_en us_en" 
 for global in $global_jobs; do
  if [ "$job_name" == "$global" ]; then
    conn_job_lines=5
    conn_job_line[2]="IngestAction2=META:cfg_keys_csv=faq_XX"
    conn_job_line[5]="IngestAction5=META:FAQ_XX=en_WW"
  fi
 done

 if [ "$job_name" == "cr_es" ]; then
    conn_job_line[5]="IngestAction5=META:FAQ_XX=es_MX"
 fi

fi

if [ "$conn_name" == "pes" ]; then
skip_job="false"
skipped_jobs="eg_ar il_en sa_ar ww_fr"
 for skip in $skipped_jobs; do
  if [ "$job_name" == "$skip" ]; then
    skip_job="true"
  fi
 done

 if [ "$conn_subname" == "navigation" ]; then
   content_type="category"
   content_source="NAV_PES"
 fi
 if [ "$conn_subname" == "family" ]; then
   content_type="subcategory"
   content_source="FAMILY_PES"
 fi
 if [ "$conn_subname" == "offer" ]; then
   content_type="range"
   content_source="OFFER_PES"
 fi

conn_job_lines=8
conn_job_line[1]="URL=${domain_prefix}/all-products/sitemap_${content_type}"
conn_job_line[2]="IngestAction2=META:cfg_keys_csv=pes_web_common,pes_web_${conn_subname},cleanup"
conn_job_line[3]="IngestAction3=META:LANGUAGE_CODE=${language_code}"
conn_job_line[4]="IngestAction4=META:TGT_LANG=${tgt_lang}"
conn_job_line[5]="IngestAction5=META:CONTENT_SOURCE=${content_source}"
conn_job_line[6]="IngestAction6=META:COUNTRY_CODE=${country_code}"
conn_job_line[7]="IngestAction7=META:HIT_REPORT_FILEPATH=${hit_statistics_path}/${content_type}/${region}_${country_code_lower}_${language_code}_stat.xml"
conn_job_line[8]="IngestAction8=META:COUNTRY_NAME=${country_name}"

 if [ "$conn_subname" == "offer" ]; then
   if [ "$job_name" == "us_en" ]; then
     conn_job_line[2]="IngestAction2=META:cfg_keys_csv=pes_web_common,pes_web_offer_without_presentation,pes_web_${conn_subname},cleanup"
   else
     conn_job_line[2]="IngestAction2=META:cfg_keys_csv=pes_web_common,pes_web_offer_with_presentation,pes_web_${conn_subname},cleanup"
   fi
 fi 

fi

if [ "$conn_name" == "sdl" ]; then

 if [ "$conn_subname" == "web" ]; then
  skip_job="false"
  skipped_jobs="il_en"
  for skip in $skipped_jobs; do
    if [ "$job_name" == "$skip" ]; then
      skip_job="true"
    fi
  done

  conn_job_lines=7
#  conn_job_line[1]="URL0=http://www-prd.${sdl_region}.sdl.schneider-electric.com/${country_code_lower}/${language_code}/work/sitemap_work_b2b.xml"
#  conn_job_line[2]="URL1=http://www-prd.${sdl_region}.sdl.schneider-electric.com/${country_code_lower}/${language_code}/home/sitemap_home_b2c.xml"
#  conn_job_line[3]="URL2=http://www-prd.${sdl_region}.sdl.schneider-electric.com/${country_code_lower}/${language_code}/partners/sitemap_partners.xml"
#  conn_job_line[4]="URL3=http://www-prd.${sdl_region}.sdl.schneider-electric.com/${country_code_lower}/${language_code}/about-us/sitemap_aboutus.xml"
#  conn_job_line[5]="URL4=http://www-prd.${sdl_region}.sdl.schneider-electric.com/${country_code_lower}/${language_code}/brands/sitemap_brand_b2b.xml"
#  conn_job_line[1]="URL0=https://www-prd.corp.sdl.se.com/${country_code_lower}/${language_code}/work/sitemap_work_b2b.xml"
#  conn_job_line[2]="URL1=https://www-prd.corp.sdl.se.com/${country_code_lower}/${language_code}/home/sitemap_home_b2c.xml"
#  conn_job_line[3]="URL2=https://www-prd.corp.sdl.se.com/${country_code_lower}/${language_code}/partners/sitemap_partners.xml"
#  conn_job_line[4]="URL3=https://www-prd.corp.sdl.se.com/${country_code_lower}/${language_code}/about-us/sitemap_aboutus.xml"
#  conn_job_line[5]="URL4=https://www-prd.corp.sdl.se.com/${country_code_lower}/${language_code}/brands/sitemap_brand_b2b.xml"
  conn_job_line[1]="URL0=https://www-prd.start.sdl.se.com/${country_code_lower}/${language_code}/sitemap.xml"
  conn_job_line[2]="IngestAction2=META:cfg_keys_csv=common,common_web,${sdl_keys},cleanup"
  conn_job_line[3]="IngestAction3=META:LANGUAGE_CODE=${language_code}"
  conn_job_line[4]="IngestAction4=META:TGT_LANG=${tgt_lang}"
  conn_job_line[5]="IngestAction5=META:COUNTRY=${country_code}"
  conn_job_line[6]="IngestAction6=META:COUNTRY_NAME=${country_name}"
  conn_job_line[7]="IngestAction7=META:LOCAL_SITEMAP=${lp_sitemaps_path}/sdl_web_sitemap.xml"
 fi

 if [ "$conn_subname" == "lp" ]; then
  skip_job="false"
  skipped_jobs="eg_ar il_en sa_ar ww_fr"
  for skip in $skipped_jobs; do
   if [ "$job_name" == "$skip" ]; then
    skip_job="true"
   fi
  done

  conn_job_lines=6
  conn_job_line[1]="URLFile=${lp_sitemaps_path}/lp_${country_code_lower}_${language_code}_sitemap.txt"
  conn_job_line[2]="IngestAction2=META:cfg_keys_csv=common,lp,cleanup"
  conn_job_line[3]="IngestAction3=META:LANGUAGE_CODE=${language_code}"
  conn_job_line[4]="IngestAction4=META:TGT_LANG=${tgt_lang}"
  conn_job_line[5]="IngestAction5=META:COUNTRY=${country_code}"
  conn_job_line[6]="Depth=0"
 fi

fi

if [ "$conn_name" == "prodsub" ]; then

skip_job="true"
allowed_jobs="ae_en ar_es at_de au_en be_fr be_nl br_pt ch_de ch_fr cn_zh cz_cs de_de dk_da es_es fi_fi fr_fr id_id id_en ie_en in_en it_it kr_ko mx_es my_en nl_nl no_no ph_en pt_pt ru_ru se_sv sg_en sk_sk th_en th_th tr_tr uk_en vn_en vn_vi ww_en ww_fr za_en us_en ca_en ca_fr"
 for allow in $allowed_jobs; do
  if [ "$job_name" == "$allow" ]; then
    skip_job="false"
  fi
 done

conn_job_lines=4
#conn_job_line[1]="DirectoryPathCSVs=${prodsub_preprocessor_path}/output/${country_code}/${language_code}"
conn_job_line[1]="DirectoryPathCSVs=${prodsub_preprocessor_path}/${country_code_lower}/${language_code}/${conn_name}"
conn_job_line[2]="IngestAction2=META:cfg_keys_csv=prodsubst"
conn_job_line[3]="IngestAction3=META:TGT_LANG=${tgt_lang}"
conn_job_line[4]="IngestAction4=META:COUNTRY_OPENURL_PREFIX=${domain_prefix}/product-substitution/"

old_config_jobs="cn_zh"
 for old in $old_config_jobs; do
  if [ "$job_name" == "$old" ]; then
    conn_job_line[4]="IngestAction4=META:COUNTRY_OPENURL_PREFIX=https://www.schneider-electric.com/tools/substitution/${country_code}/${language_code}?ref="
  fi
 done

fi

if [ "$conn_name" == "oos" ]; then
skip_job="false"
skipped_jobs="il_en"
 for skip in $skipped_jobs; do
  if [ "$job_name" == "$skip" ]; then
    skip_job="true"
  fi
 done
conn_job_lines=5
#conn_job_line[1]="DirectoryPathCSVs=${onepop_preprocessor_path}/dest/${country_code}/outOfScope"
conn_job_line[1]="DirectoryPathCSVs=${onepop_preprocessor_path}/${country_code_lower}/${language_code}/${conn_name}"
conn_job_line[2]="IngestAction2=META:cfg_keys_csv=oos"
conn_job_line[3]="IngestAction3=META:COUNTRY_CODE=${country_code}"
conn_job_line[4]="IngestAction4=META:LANGUAGE_CODE=${language_code}"
conn_job_line[5]="IngestAction5=META:TGT_LANG=${tgt_lang}"

### exceptions ###
 if [ "$job_name" == "uk_en" ]; then
#  conn_job_line[1]="DirectoryPathCSVs=${ddc_preprocessor_path}/ddc_out/dest/${bsl_country_code}/outOfScope"
#  conn_job_line[1]="DirectoryPathCSVs=${onepop_preprocessor_path}/uk/${language_code}/${conn_name}"
  conn_job_line[3]="IngestAction3=META:COUNTRY_CODE=${bsl_country_code}"
 fi

fi

if [ "$conn_name" == "blog" ]; then

 skip_job="true"

 if [ "${language_code}" == "en" ]; then
  skip_job="false"
  conn_job_lines=8
  conn_job_line[1]="URL0=https://blog.se.com/post-sitemap1.xml"
  conn_job_line[2]="URL1=https://blog.se.com/post-sitemap2.xml"
  conn_job_line[3]="URL2=https://blog.se.com/post-sitemap3.xml"
  conn_job_line[4]="URL3=https://blog.se.com/post-sitemap4.xml"
  conn_job_line[5]="URL4=https://blog.se.com/post-sitemap5.xml"
  conn_job_line[6]="IngestAction2=META:cfg_keys_csv=common"
  conn_job_line[7]="IngestAction3=META:LANGUAGE_CODE=${language_code}"
  conn_job_line[8]="IngestAction4=META:TGT_LANG=${tgt_lang}"
 fi

 if [ "${language_code}" == "de" ] || [ "${language_code}" == "da" ] || [ "${language_code}" == "es" ] || [ "${language_code}" == "fr" ] || [ "${language_code}" == "pt" ]; then
  skip_job="false"
  conn_job_lines=4
  conn_job_line[1]="URL=https://blog.se.com/${language_code}/post-sitemap.xml"
  conn_job_line[2]="IngestAction2=META:cfg_keys_csv=common"
  conn_job_line[3]="IngestAction3=META:LANGUAGE_CODE=${language_code}"
  conn_job_line[4]="IngestAction4=META:TGT_LANG=${tgt_lang}"
  if [ "${language_code}" == "es" ]; then
   conn_job_line[1]="URL=https://blogespanol.se.com/post-sitemap.xml"
  fi
  if [ "${language_code}" == "pt" ]; then
   conn_job_line[1]="URL=https://blog.se.com/br/post-sitemap.xml"
  fi
  if [ "${language_code}" == "da" ]; then
   conn_job_line[1]="URL=https://blog.se.com/dk/post-sitemap.xml"
  fi
 fi

fi

if [ "$conn_name" == "video" ]; then

 if [ "$conn_subname" == "sdl" ]; then
  skip_job="false"
  skipped_jobs="cn_zh il_en"
  for skip in $skipped_jobs; do
   if [ "$job_name" == "$skip" ]; then
    skip_job="true"
   fi
  done
 fi

 if [ "$conn_subname" == "faq" ]; then
  skip_job="false"
  skipped_jobs="cn_zh eg_ar il_en il_he sa_ar ua_uk ww_fr"
  for skip in $skipped_jobs; do
   if [ "$job_name" == "$skip" ]; then
    skip_job="true"
   fi
  done
 fi
  
conn_subname_upper=$(echo "$conn_subname" | tr '[:lower:]' '[:upper:]')
conn_job_lines=4
#conn_job_line[1]="DirectoryPathCSVs=${video_preprocessor_path}/dest/${conn_subname}/${country_code}/${language_code}"
conn_job_line[1]="DirectoryPathCSVs=${video_preprocessor_path}/${country_code_lower}/${language_code}/${conn_name}/${conn_subname}"
conn_job_line[2]="IngestAction2=META:cfg_keys_csv=video_common"
conn_job_line[3]="IngestAction3=META:CONTENT_SOURCE=VIDEO_${conn_subname_upper}"
conn_job_line[4]="IngestAction6=META:TGT_LANG=${tgt_lang}"

fi

if [ "$conn_name" == "digest" ]; then

skip_job="true"
allowed_jobs="us_en"
 for allow in $allowed_jobs; do
  if [ "$job_name" == "$allow" ]; then
    skip_job="false"
  fi
 done

conn_job_lines=4

 if [ "$conn_subname" == "plus" ]; then
#  conn_job_line[1]="DirectoryPathCSVs=${digest_preprocessor_path}/digest177/output"
  conn_job_line[1]="DirectoryPathCSVs=${digest_preprocessor_path}/${country_code_lower}/${language_code}/${conn_name}/${conn_subname}"
 fi

 if [ "$conn_subname" == "supplemental" ]; then
#  conn_job_line[1]="DirectoryPathCSVs=${digest_preprocessor_path}/suplemental/output"
  conn_job_line[1]="DirectoryPathCSVs=${digest_preprocessor_path}/${country_code_lower}/${language_code}/${conn_name}/${conn_subname}"
 fi
  
conn_job_line[2]="IngestAction2=META:cfg_keys_csv=digest_plus"
conn_job_line[3]="IngestAction3=META:LANGUAGE_CODE=${language_code}"
conn_job_line[4]="IngestAction4=META:TGT_LANG=${tgt_lang}"

fi

if [ "$conn_name" == "thomasnet" ]; then

skip_job="true"
allowed_jobs="us_en"
 for allow in $allowed_jobs; do
  if [ "$job_name" == "$allow" ]; then
    skip_job="false"
  fi
 done

conn_job_lines=4
conn_job_line[1]="URL=https://schneider.thomasnet-navigator.com/"
conn_job_line[2]="IngestAction2=META:cfg_keys_csv=common"
conn_job_line[3]="IngestAction3=META:LANGUAGE_CODE=${language_code}"
conn_job_line[4]="IngestAction4=META:TGT_LANG=${tgt_lang}"

fi

if [ "$conn_name" == "traceparts" ]; then

skip_job="true"
allowed_jobs="us_en"
 for allow in $allowed_jobs; do
  if [ "$job_name" == "$allow" ]; then
    skip_job="false"
  fi
 done

conn_job_lines=4
#conn_job_line[1]="DirectoryPathCSVs=${traceparts_preprocessor_path}/dest/${country_code_lower}/${language_code}"
conn_job_line[1]="DirectoryPathCSVs=${traceparts_preprocessor_path}/${country_code_lower}/${language_code}/${conn_name}"
conn_job_line[2]="IngestAction2=META:cfg_keys_csv=traceparts_common"
conn_job_line[3]="IngestAction3=META:LANGUAGE_CODE=${language_code}"
conn_job_line[4]="IngestAction4=META:TGT_LANG=${tgt_lang}"

fi

if [ "$conn_name" == "crossref" ]; then

skip_job="true"
allowed_jobs="us_en ca_en ca_fr"
 for allow in $allowed_jobs; do
  if [ "$job_name" == "$allow" ]; then
    skip_job="false"
  fi
 done

conn_job_lines=8
conn_job_line[1]="ConnectionString = IDOL_INDEXER/IDOL_INDEXER@CBNADMIN"
conn_job_line[2]="Template = /hpe/idol/${conn_path}/${conn_name}/COMPETITOR_DATA.tmpl"
conn_job_line[3]="PrimaryKeys = COMPETITOR_PRODUCT_NO,COUNTRY"
conn_job_line[4]="SQL = select distinct COMPETITOR_PRODUCT_NO,COMPETITOR_NAME,COUNTRY from CBMADMIN.COMPETITOR_DATA where COUNTRY='${crossref_country_name}'"
conn_job_line[5]="IngestAction2=META:cfg_keys_csv=crossref"
conn_job_line[6]="IngestAction3=META:LANGUAGE_CODE=${language_code}"
conn_job_line[7]="IngestAction4=META:COUNTRY_CODE=${country_code}"
conn_job_line[8]="IngestAction5=META:TGT_LANG=${tgt_lang}"

 if [ "$country_code" == "US" ]; then
  conn_job_line[9]="IngestAction6=META:APPL_URL_PREFIX=https://tools.se.app/xref/index.html?crn="
 fi
 if [ "$country_code" == "CA" ]; then
  conn_job_line[9]="IngestAction6=META:APPL_URL_PREFIX=https://tools.se.app/xref/cross_${language_code}_ca.html?crn="
 fi

fi

if [ "$conn_name" == "clipsal" ]; then

skip_job="true"
allowed_jobs="au_en"
 for allow in $allowed_jobs; do
  if [ "$job_name" == "$allow" ]; then
    skip_job="false"
  fi
 done

conn_job_lines=4
conn_job_line[1]="URLFile=${clipsal_sitemaps_path}/clipsal_${country_code_lower}_${language_code}_sitemap.txt"
conn_job_line[2]="IngestAction2=META:cfg_keys_csv=common"
conn_job_line[3]="IngestAction3=META:LANGUAGE_CODE=${language_code}"
conn_job_line[4]="IngestAction4=META:TGT_LANG=${tgt_lang}"
fi

if [ "$conn_name" == "marketo" ]; then

skip_job="true"
allowed_jobs="ww_en us_en"
 for allow in $allowed_jobs; do
  if [ "$job_name" == "$allow" ]; then
    skip_job="false"
  fi
 done

conn_job_lines=4
conn_job_line[1]="DirectoryPathCSVs=${marketo_preprocessor_path}/${country_code_lower}/${language_code}/${conn_name}"
conn_job_line[2]="IngestAction2=META:cfg_keys_csv=common"
conn_job_line[3]="IngestAction3=META:LANGUAGE_CODE=${language_code}"
conn_job_line[4]="IngestAction4=META:TGT_LANG=${tgt_lang}"
fi


if [ "$conn_name" == "myse" ]; then

conn_job_lines=8
conn_job_line[0]="IndexDatabase=MYSE_${country_code}"
conn_job_line[1]="ConnectionString = ${myse_connection}"
conn_job_line[2]="Template = /hpe/idol/${conn_path}/${conn_name}/PRODUCT.tmpl"
conn_job_line[3]="PrimaryKeys = OID"
conn_job_line[4]="SQL = select p.OID,p.CATALOG_NUMBER,p.PRODUCT_NUMBER,p.EL_NUMBER,pt.NAME,pt.LANGUAGE,pt.CREATION_TIME,pt.LAST_MOD_TIME,p.STATUS,p.SALES_ORG,p.DISTRIBUTION_CHANNEL,p.DIVISION from PROD_EDIST.PRODUCT p, PROD_EDIST.PRODUCT_TRANSLATION pt where p.OID=pt.PRODUCT_OID and pt.STORE_ID=${myse_store_id}  and pt.LANGUAGE='${language_code_upper}'"
conn_job_line[5]="IngestAction2=META:cfg_keys_csv=myse_product"
conn_job_line[6]="IngestAction3=META:CT_CAT=PRODUCT"
conn_job_line[7]="IngestAction4=META:COUNTRY_CODE=${country_code}"
conn_job_line[8]="IngestAction5=META:TGT_LANG=${tgt_lang}"

fi

}


##################################################################################################################
### Scipt logic
##################################################################################################################

### Main configuration values
working_directory=$PWD
local_repository="${working_directory}/git/master"
binaries_path=${working_directory}
destination_path="/hpe/idol/${conn_path}/${conn_name}"
connector_path="${destination_path}/${conn_name}_${conn_type}"
cfs_path="${destination_path}/${conn_name}_cfs"
conn_conf_file="${connector_path}/${conn_name}_${conn_type}.cfg"
cfs_conf_file="${cfs_path}/${conn_name}_cfs.cfg"
main_conf_file=$destination_path/${conn_name}_config.cfg
common_path="/hpe/idol/${conn_path}/common"
today=`date +%Y%m%d`

### Binaries, scripts and configuration templates
if [ "$options" == "deployment" ]; then

 if [ ! -d $destination_path ]; then
  mkdir $destination_path
 fi
 if [ ! -d $connector_path ]; then
  mkdir $connector_path
 fi
 if test -z "`ls ${connector_path}`"; then
  echo "Copying binaries and configuration template for ${conn_name} connector"
  cp -r ${binaries_path}/${conn_type}/* $connector_path
 else
  echo "${connector_path} directory is not empty"
  exit
 fi
 if [ ! -d $cfs_path ]; then
  mkdir $cfs_path
 fi
 if test -z "`ls ${cfs_path}`"; then
  echo "Copying binaries and configuration template for ${conn_name} CFS"
  cp -r ${binaries_path}/cfs/* $cfs_path
 else
  echo "${cfs_path} directory is not empty"
  exit
 fi

 echo "Adapting binaries and scripts for ${conn_name} connector"
 mv "${connector_path}/conn_name_${conn_type}.exe" "${connector_path}/${conn_name}_${conn_type}.exe"
 sed s#conn_name#$conn_name#g ${connector_path}/start-${conn_type}_conn_name.sh > ${connector_path}/start-${conn_type}.sh
 sed s#conn_name#$conn_name#g $connector_path/stop-${conn_type}_conn_name.sh > ${connector_path}/stop-${conn_type}.sh
 sed -e s#conn_server_ip#$conn_server_ip#g -e s#conn_name#$conn_name#g ${connector_path}/cron_start_conn_name.sh > ${connector_path}/cron_start_${conn_name}.sh
 rm ${connector_path}/*conn_name*
 chmod 740 ${connector_path}/*.sh
 ln -s /hpe/idol/logs/${conn_path}/${conn_name}/${conn_name}_${conn_type} ${connector_path}/logs


 echo "Adapting binaries and scripts for ${conn_name} CFS"
 mv "${cfs_path}/conn_name_cfs.exe" "$cfs_path/${conn_name}_cfs.exe"
 sed s#conn_name#$conn_name#g ${cfs_path}/start-cfs_conn_name.sh > ${cfs_path}/start-cfs.sh
 sed s#conn_name#$conn_name#g ${cfs_path}/stop-cfs_conn_name.sh > ${cfs_path}/stop-cfs.sh
 rm ${cfs_path}/*conn_name*
 chmod 740 ${cfs_path}/*.sh
 ln -s /hpe/idol/logs/${conn_path}/${conn_name}/${conn_name}_cfs ${cfs_path}/logs

fi

### Connector configuration
if [ "$options" == "deployment" ] || [ "$options" == "connector" ]; then

 if [ -e $conn_conf_file ]; then
  echo "Renaming ${conn_conf_file} to ${conn_conf_file}_${today}"
  cp ${conn_conf_file} ${conn_conf_file}_${today}
 fi

 echo "Copying configuration template for ${conn_name}_${conn_type}.cfg"
 cp ${binaries_path}/${conn_type}/conn_name_${conn_type}.cfg ${connector_path}
 
 echo "Adapting basic configuration for ${conn_name}_${conn_type}.cfg"
 sed -e s#conn_server_ip#$conn_server_ip#g -e s#conn_server_name#$conn_server_name#g -e s#license_server_ip#$license_server_ip#g -e s#conn_path#$conn_path#g -e s#conn_name#$conn_name#g -e s#conn_port#$conn_port#g -e s#number_of_threads#$number_of_threads#g "${connector_path}/conn_name_${conn_type}.cfg" > ${conn_conf_file}
 rm ${connector_path}/conn_name_${conn_type}.cfg

 if [ "$conn_name" == "thomasnet" ]; then
  sed -e 's#LinkCheck=1#MustHaveCheck=129\nMustHaveCSVs=http://schneider.thomasnet-navigator.com/,*/item/*,*/category/*,*/viewitems/*\nCantHaveCheck=129\nCantHaveCSVs=*?sortid*\nLinkAllowCheck=1#g' -e 's#LinkAllowCSVs=\*sitemap\*#LinkAllowCSVs=http://schneider.thomasnet-navigator.com/,*/item/*,*/category /*,*/viewitems/*#g' -e 's#Depth=1#Depth=5#g' ${conn_conf_file} > ${conn_conf_file}_tmp
  mv ${conn_conf_file}_tmp ${conn_conf_file}
 fi

 if [ "$conn_name" == "digest" ]; then
   sed -e 's#DirectoryFileMatch=\*.xml#DirectoryFileMatch=*.xml*#g' ${conn_conf_file} > ${conn_conf_file}_tmp
   mv ${conn_conf_file}_tmp ${conn_conf_file}
 fi

 if [ "$conn_name" == "sdl" ]; then
  sed -e 's#LinkCheck=1#MustHaveCheck=4\nLinkCheck=1#g' -e 's#LinkAllowCSVs=\*sitemap\*#MustHaveCSVs=*<title>*\nLinkAllowCSVs=\*sitemap\*#g' ${conn_conf_file} > ${conn_conf_file}_tmp
  mv ${conn_conf_file}_tmp ${conn_conf_file}
 fi


 echo "Configuring jobs for ${conn_name} connector"
 job_number=0
 conn_job_header=""
 conn_job_config=""
 for conn_subname in $conn_subnames
 do
  for job_name in $jobs
  do
   skip_job="false"
   connector_jobs
   if [ "$skip_job" == "false" ]; then
     if [ "$conn_name" == "$conn_subname" ]; then
       conn_job_header="${conn_job_header}${job_number}=${conn_name}_${job_name}\n"
       conn_job_config="${conn_job_config}[${conn_name}_${job_name}]\n"
     else
       conn_job_header="${conn_job_header}${job_number}=${conn_name}_${conn_subname}_${job_name}\n"
       conn_job_config="${conn_job_config}[${conn_name}_${conn_subname}_${job_name}]\n"
     fi
     job_number=$((job_number+1))     
     for (( i=0; $i <= ${conn_job_lines}; i++ )) ; do
       conn_job_config="${conn_job_config}${conn_job_line[i]}\n"
     done
     conn_job_config="${conn_job_config}\n"
   fi
  done
 done 

 echo "Number=${job_number}" >> $conn_conf_file
 echo -e $conn_job_header >> $conn_conf_file
 echo -e $conn_job_config >> $conn_conf_file

 if [ -e ${conn_conf_file}_${today} ]; then
  echo "Diff listing between old and new connector configuration for ${conn_name}"
  echo "-------------------------------------------------------------------------------"
  diff ${conn_conf_file}_${today} $conn_conf_file | sed 's#^<#Old:#g' | sed 's#^>#New:#g'
  echo "-------------------------------------------------------------------------------"
 fi

fi

### CFS configuration
if [ "$options" == "deployment" ] || [ "$options" == "cfs" ]; then

 if [ -e $cfs_conf_file ]; then 
  echo "Renaming ${cfs_conf_file} to ${cfs_conf_file}_${today}"
  cp ${cfs_conf_file} ${cfs_conf_file}_${today}
 fi

 echo "Copying configuration template for ${conn_name}_cfs.cfg"
 cp ${binaries_path}/cfs/conn_name_cfs.cfg ${cfs_path}

 echo "Adapting basic configuration for ${conn_name}_cfs.cfg"
 sed -e s#conn_server_ip#$conn_server_ip#g -e s#conn_server_name#$conn_server_name#g -e s#license_server_ip#$license_server_ip#g -e s#conn_path#$conn_path#g -e s#conn_name#$conn_name#g -e s#conn_port#$conn_port#g -e s#indexer_port#$indexer_port#g -e s#indexer_batch_size#$indexer_batch_size#g -e s#indexer_time_interval#$indexer_time_interval#g -e s#error_server_port#$error_server_port#g -e s#number_of_threads#$number_of_threads#g ${cfs_path}/conn_name_cfs.cfg > ${cfs_conf_file}
 rm ${cfs_path}/conn_name_cfs.cfg

 echo "Configuring ImportTasks section for ${conn_name} CFS"

 echo "${cfs_task}" >> $cfs_conf_file

 if [ "$conn_type" == "httpc" ]; then
  echo "Configuring TextToDocs section for ${conn_name} CFS"
  echo "" >> $cfs_conf_file
  echo "${cfs_text}" >> $cfs_conf_file
 fi

 if [ "${xml}" == "true" ]; then
  echo "Configuring XMLParsing section for ${conn_name} CFS"
  echo "" >> $cfs_conf_file
  echo "${xml_parsing}" >> $cfs_conf_file
 fi

 if [ -e ${cfs_conf_file}_${today} ]; then
  echo "Diff listing between old and new CFS configuration for ${conn_name}"
  echo "-------------------------------------------------------------------------------"
  diff ${cfs_conf_file}_${today} $cfs_conf_file | sed 's#^<#Old:#g' | sed 's#^>#New:#g'
  echo "-------------------------------------------------------------------------------"
 fi

fi

### Main configuration
if [ "$options" == "deployment" ] || [ "$options" == "configuration" ]; then

 if [ -e $main_conf_file ]; then
  echo "Renaming ${main_conf_file} to ${main_conf_file}_${today}"
  cp ${main_conf_file} ${main_conf_file}_${today}
 fi

 echo "Updating GIT master repository"
 cd $local_repository
 git pull https://bitbucket.org/schneiderzia/cfs_connectors/src/master/

 echo "Copying main configuration file for ${conn_name} from GIT master repository to $destination_path"
 cp ${local_repository}/configuration/${conn_name}_config.cfg $destination_path

 if [ "$conn_name" == "crossref" ]; then
  echo "Copying database template file from GIT master repository to $destination_path"
  cp ${local_repository}/configuration/COMPETITOR_DATA.tmpl $destination_path
 fi

 if [ "$conn_name" == "myse" ]; then
  echo "Copying database template file from GIT master repository to $destination_path"
  cp ${local_repository}/configuration/PRODUCT.tmpl $destination_path
 fi


 if [ "$conn_name" == "bsldoc" ]; then
  echo "Adapting BSL import path in bsldoc main configuration"
  sed s#/Autonomy/Data/BSLData#$bsl_import_path#g $destination_path/${conn_name}_config.cfg > $destination_path/${conn_name}_config.cfg_temp
  mv $destination_path/${conn_name}_config.cfg_temp $destination_path/${conn_name}_config.cfg
 fi

 if [ "$conn_name" == "sdl" ]; then
  sdl_sitemap_store_script="sdl_dir=${destination_path}
sitemap_dir=${lp_sitemaps_path}
conn_dir=\${sdl_dir}/sdl_httpc
conf_file=\${conn_dir}/sdl_httpc.cfg
sitemap_file=\${sitemap_dir}/sdl_web_sitemap.xml
DD=\$(date +%Y-%m-%d)

echo \"<url>\" > \${sitemap_file}
for url in \`grep -i \"URL[[:digit:]]\" \${conf_file} | awk -F\"=\" '{print \$2;}'\`
do
echo \"Processing \$url\"
curl -gk \"\${url}\" 2>/dev/null| grep \"<url>\" | xmllint --format - | grep -e \"<loc>\" -e \"<lastmod>\" | tr '\n' ' '| sed 's/<loc>/\n\t<loc id=\"/g'| sed 's#</loc>#\"</loc>#'| sed 's#</loc>.*<lastmod>#>#'| sed \"s#</loc>#>\$DD</loc>#\"|sed 's#</lastmod>#</loc>#g' | sed -e 's#\([0-9]*-[0-9]*-[0-9]*\)T.*Z#\1#'  >> \${sitemap_file}
done
echo \"</url>\" >> \${sitemap_file}"
  echo "Creating ${connector_path}/store_sdl_sitemaps.sh script"
  echo "${sdl_sitemap_store_script}" > ${connector_path}/store_sdl_sitemaps.sh
  chmod 744 ${connector_path}/store_sdl_sitemaps.sh
 fi

 if [ -e ${main_conf_file}_${today} ]; then
  echo "Diff listing between old and new main configuration for ${conn_name}"
  echo "-------------------------------------------------------------------------------"
  diff ${main_conf_file}_${today} $main_conf_file | sed 's#^<#Old:#g' | sed 's#^>#New:#g'
  echo "-------------------------------------------------------------------------------"
 fi

 cd $working_directory

fi

### Lua scripts
if [ "$conn_name" == "common" ] && [ "$options" == "lua" ]; then

 if [ ! -d $common_path ]; then
  mkdir $common_path
 fi
 if [ ! -d $lua_path ]; then
  mkdir $lua_path
 fi

 if [ -e $lua_path/library.lua ];then
  echo "Archiving existing lua scipts in $lua_path/old directory"
  if [ ! -d $lua_path/old ]; then
   mkdir $lua_path/old
  fi
  cp $lua_path/*.lua $lua_path/old  
 fi

 echo "Updating GIT master repository" 
 cd $local_repository
 git pull https://bitbucket.org/schneiderzia/cfs_connectors/src/master/

 echo "Copying lua scripts from GIT master repository to $lua_path"
 cp ${local_repository}/lua/*.lua $lua_path
 sed s#.?.lua#${lua_path}/?.lua#g $lua_path/wms_doc_post_processing.lua > $lua_path/wms_doc_post_processing.lua_temp
 sed s#.?.lua#${lua_path}/?.lua#g $lua_path/wms_doc_pre_processing.lua > $lua_path/wms_doc_pre_processing.lua_temp
 sed s#.?.lua#${lua_path}/?.lua#g $lua_path/library.lua > $lua_path/library.lua_temp
 mv $lua_path/wms_doc_post_processing.lua_temp $lua_path/wms_doc_post_processing.lua
 mv $lua_path/wms_doc_pre_processing.lua_temp $lua_path/wms_doc_pre_processing.lua
 mv $lua_path/library.lua_temp $lua_path/library.lua
 cd $working_directory

fi

### Stopping connector and its CFS processes
if [ "$options" == "stop" ]; then

ps_check=`ps aux | grep ${cfs_path} | grep -v "grep"`
echo $ps_check
 if [ "$ps_check" == "" ]; then
  echo "${conn_name} CFS process is not running"
 else 
   echo "Stopping ${conn_name} CFS"
   cd ${cfs_path}
   if [ -f ${conn_name}_cfs.pid ]; then
    kill -15 `cat ${conn_name}_cfs.pid`
   else
     echo "Could not locate ${conn_name}_cfs.pid - unable to stop CFS"
   fi
   echo "Checking if CFS process has stopped"
  while [ "$ps_check" != "" ]; do
   sleep 1
   ps_check=`ps aux | grep ${cfs_path} | grep -v "grep"`
  done
  echo "${conn_name} CFS process has been stopped"
 fi

ps_check=`ps aux | grep ${connector_path} | grep -v "grep"`
echo $ps_check
 if [ "$ps_check" == "" ]; then
  echo "${conn_name} connector process is not running"
 else
   echo "Stopping ${conn_name} connector"
   cd ${connector_path}
   if [ -f ${conn_name}_${conn_type}.pid ]; then
    kill -15 `cat ${conn_name}_${conn_type}.pid`
   else
     echo "Could not locate ${conn_name}_${conn_type}.pid - unable to stop connector"
   fi
   echo "Checking if connector process has stopped"
  while [ "$ps_check" != "" ]; do
   sleep 1
   ps_check=`ps aux | grep ${connector_path} | grep -v "grep"`
  done
  echo "${conn_name} connector process has been stopped"
 fi

 cd ${working_directory}

fi

### Starting connector and its CFS processes
if [ "$options" == "start" ]; then

ps_check=`ps aux | grep ${cfs_path} | grep -v "grep"`
 if [ "$ps_check" != "" ]; then
  echo "${conn_name} CFS process is already running"
 else
  while [ "$ps_check" == "" ]; do
   echo "Starting ${conn_name} CFS"
   cd ${cfs_path}
   nohup $PWD/${conn_name}_cfs.exe > ${conn_name}_cfs.out 2> ${conn_name}_cfs.err &
   echo "Checking if CFS process has started"
   ps_check=`ps aux | grep ${cfs_path} | grep -v "grep"`
  done
  echo "${conn_name} CFS process has been started"
 fi

ps_check=`ps aux | grep ${connector_path} | grep -v "grep"`
 if [ "$ps_check" != "" ]; then
  echo "${conn_name} connector process is already running"
 else
  while [ "$ps_check" == "" ]; do
   echo "Starting ${conn_name} connector"
   cd ${connector_path}
   export LD_LIBRARY_PATH=./:../bin:./ffmpeg:./filters:$LD_LIBRARY_PATH
   if [ "$conn_name" == "crossref" ]; then
    export PATH=$PATH:.
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${connector_path}:/hpe/idol/common/opt/oracle/instantclient_11_1:/hpe/idol/common/opt/oracle/tnsnames
    export SHLIB_PATH=$SHLIB_PATH:/hpe/idol/zone2/cfs_connectors/crossref/crossref_dbc:/hpe/idol/common/opt/oracle/instantclient_11_1:/hpe/idol/common/opt/oracle/tnsnames
    export TNS_ADMIN=/hpe/idol/common/opt/oracle/tnsadmin
   fi
   nohup $PWD/${conn_name}_${conn_type}.exe > ${conn_name}_${conn_type}.out 2> ${conn_name}_${conn_type}.err &
   echo "Checking if connector process has started"
   ps_check=`ps aux | grep ${connector_path} | grep -v "grep"`
  done
  echo "${conn_name} connector process has been started"
 fi

ps aux | grep ${destination_path} | grep -v "grep"

fi

### Cleanup for fresh indexation
if [ "$options" == "cleanup" ]; then

./connector_deployment.sh $environment $zone $conn_name "stop"

echo "Removing CFS idx files, logs and working folders in ${cfs_path}"
cd ${cfs_path}
rm -rf actions failed indextemp outgoing temp ./logs/* *.idx*

cd $working_directory

echo "Removing connector datastores, logs and working folders in ${connector_path}"
cd ${connector_path} 
rm -rf actions Temp ./logs/* *.db* 

 if [ "$conn_type" == "httpc" ]; then 
  conn_name_upper=$(echo "$conn_name" | tr '[:lower:]' '[:upper:]')
  if [ "$conn_name_upper" != "" ]; then
   rm -rf ${conn_name_upper}_*
  fi
 fi

cd $working_directory
./connector_deployment.sh $environment $zone $conn_name "start"

fi

## Run indexation
if [ "$options" == "index" ]; then

echo "Running all jobs indexation for ${conn_name}"
cd ${connector_path}
./cron_start_${conn_name}.sh

echo "Preview synchronize.log"
sleep 10
tail logs/synchronize.log

cd $working_directory

fi

### SDL landing page sitemaps 
if [ "$conn_name" == "sdl" ] && ([ "$options" == "deployment" ] || [ "$options" == "sitemaps" ]); then

 if [ ! -d ${lp_sitemaps_path} ]; then
  mkdir ${lp_sitemaps_path}
 fi

 if test -z "`ls ${lp_sitemaps_path}`"; then
  echo "Creating LP sitemaps"
 fi

  for job_name in $jobs; do
   skip_job="false"
   connector_jobs
   if [ "$skip_job" == "false" ]; then
    echo "${domain_prefix}/all-products/" > ${lp_sitemaps_path}/lp_${country_code_lower}_${language_code}_sitemap.txt
    echo "${domain_prefix}/product-substitution/" >> ${lp_sitemaps_path}/lp_${country_code_lower}_${language_code}_sitemap.txt    
    echo "${domain_prefix}/product-finder/" >> ${lp_sitemaps_path}/lp_${country_code_lower}_${language_code}_sitemap.txt
    echo "${domain_prefix}/product-range-az/" >> ${lp_sitemaps_path}/lp_${country_code_lower}_${language_code}_sitemap.txt
    echo "${domain_prefix}/download/" >> ${lp_sitemaps_path}/lp_${country_code_lower}_${language_code}_sitemap.txt
    echo "${domain_prefix}/faqs/home/" >> ${lp_sitemaps_path}/lp_${country_code_lower}_${language_code}_sitemap.txt
   fi
  done

fi

