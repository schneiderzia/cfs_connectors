package.path = '.?.lua;' .. package.path
require('toolbox')
require('library')
local configuration = get_config()
log = get_log(configuration, "IndexLogStream")

local config_key_fieldname = "cfg_keys_csv"

function handler(document)
  local config_file_name = getMyFieldValue(document,"cfg_file")
  local config =get_config(config_file_name)
        local config_keys = getMyFieldValue(document,config_key_fieldname)
  if config_keys then
    local config_keys_list = split(config_keys,",")
    for _,config_key in ipairs(config_keys_list) do
      local fieldsConfig = { config:getValues( config_key, "post" ) }
      for _ , processingLine in ipairs(fieldsConfig) do
        local processingLineSplitList = split(processingLine,";")
        local operation = processingLineSplitList[1]
          if ( operation == "noop" ) then

          elseif ( operation == "add_field_from_xml" ) then
            mapXMLTagsToFields(document,processingLineSplitList)
          elseif ( operation == "add_field_value" ) then
            addFields(document,processingLineSplitList)
          elseif ( operation == "add_field_value_from_regex_match" ) then
            addFieldValueFromRegexMatch(document,processingLineSplitList)       
          elseif ( operation == "add_conditional_field_value_from_regex_match" ) then
            addConditionalFieldValueFromRegexMatch(document,processingLineSplitList)
          elseif ( operation == "add_date_field" ) then
            addDateField(document,processingLineSplitList)
          elseif ( operation == "indexation_musthave_condition" ) then
            if not(is_musthave_condition_valid(document,processingLineSplitList)) then return false end
          elseif ( operation == "indexation_musthave_condition_two_re" ) then
            if not(is_musthave_condition_valid_two_re(document,processingLineSplitList)) then return false end
          elseif ( operation == "indexation_canthave_condition" ) then
            if not(is_canthave_condition_valid(document,processingLineSplitList)) then return false end
          elseif ( operation == "import_document" ) then
            importDocument(document,processingLineSplitList)
          elseif ( operation == "replace_all_values" ) then
            replaceAllValues(document,processingLineSplitList)
          elseif ( operation == "replace_values" ) then
            replaceValues(document,processingLineSplitList)
          elseif ( operation == "compact_field_values" ) then
            compactFieldValues(document,processingLineSplitList)
          elseif ( operation == "expand_field_values" ) then
            expandFieldValues(document,processingLineSplitList)
          elseif ( operation == "delete_fields" ) then
            deleteFields(document,processingLineSplitList)
          elseif ( operation == "convert_html_entities" ) then
            HTMLToText(document,processingLineSplitList)
          elseif ( operation == "upper" ) then
            toUpper(document,processingLineSplitList)
          elseif ( operation == "lower" ) then
            toLower(document,processingLineSplitList)
          elseif ( operation == "substr" ) then
            substring(document,processingLineSplitList)
          elseif ( operation == "gs_add_new_fields" ) then
            gs_add_fields(document)
          elseif ( operation == "set_field_attribute" ) then
            setFieldAttributeValue(document,params)
          elseif ( operation == "set_field_attribute_from_xml" ) then
            setFieldAttributeXMLValue(document,params)
          else
            log:write_line( log_level_always() , "no cfg operation key valid : "..operation)

          end
      end
    end
  end
  return true
end
