function handler(document)
  local content_source = document:getFieldValue("CONTENT_SOURCE")
  if content_source == "ECAT_BSL" then
    local what = document:getFieldValue("PRODUCT_REF")
    local min = 3
    local max = what:len()-1
    local items = {}
    local num = 1
    local fieldname = "KEYWORDS"
--    local value = document:getFieldValue(fieldname)
    local value = ""
    for ngram_len = min, max do
        for j=1, what:len()-ngram_len+1 do
            token = what:sub(j,j+ngram_len-1)
            --log:write_line( log_level_always() , "j="..j .. " ngram_len=" .. ngram_len .. "> " .. token)
            items[num]= token
            num=num+1;
        end
    end
    for k,v in pairs(items) do 
	--log:write_line( log_level_always() , "v="..v) 
        value = value .. ", " .. v
        --log:write_line( log_level_always() , "value="..value)
        if string.find(v,"o") ~= nil then
          v2 = string.gsub(v,"o",0)
          value = value .. ", " .. v2
        end
        if string.find(v,"O") ~= nil then
          v3 = string.gsub(v,"O",0)
          value = value .. ", " .. v3
        end
        if string.find(v,"0") ~= nil then
          v4 = string.gsub(v,0,"O")
          value = value .. ", " .. v4
        end
    end
    if value and value ~= "" then
        document:setFieldValue(fieldname, value)
    end
  end
  return true
end
