package.path = '.?.lua;' .. package.path
require('toolbox')
configuration = get_config()
log = get_log(configuration, "IndexLogStream")


function tablelength(T)
  local count = 0
  for _ in pairs(T) do count = count + 1 end
  return count
end

function fieldZip(document,params)
	
	local toField=params[2]
	local fieldA=params[3]
	local glue=params[4]
	local fieldB=params[5]
	
--	xpcall(function()

		local f1={ document:getFields(fieldA) }
		local f2={ document:getFields(fieldB) }

--		local size1=tablelength(f1)
--		local size2=tablelength(f2)

		local size1=#f1
		local size2=#f2
   log:write_line( log_level_always() , "size1 : "..size1)
   log:write_line( log_level_always() , "size2 : "..size2)

		local value=""
		local size=0
		if size1>size2 then
			size=size2
		else
			size=size1
		end

		for i=1,size do
   
   log:write_line( log_level_always() , "size : "..size)
   log:write_line( log_level_always() , "f1[i] : "..f1[i])
   log:write_line( log_level_always() , "f2[i] : "..f2[i])
   log:write_line( log_level_always() , "value : "..value)
--			addMyFieldValue(document, toField, value])
                        value=f1[i] .. glue
                        value=value .. f2[i]
                        document:addField(toField,value)
			document:deleteField(fieldA,f1[i])
			document:deleteField(fieldB,f2[i])
		end
		
--	end,
--	function(err) document:addField("zipField_fail",err) end
--	)
end

function addNodesFromXml(document,params)

  local fieldName=params[2]
  local xpathQuery=params[3]
  
  local xmlFile = document:getReference()
  local xml = parse_xml( read_file( xmlFile ) )
  
  local nodes = xml:XPathExecute( xpathQuery )
  
  if nodes:size() ~= 0 then
    
    xpcall( function()
    for i=1,nodes:size(),1  
      do
        document:addField(fieldName,"")	
      end
    end, 
    function(err) document:addField("FAIL1",err) end
    )
    
    local fields = {document:getFields(fieldName)}
    
    xpcall(
        function() 
      for i=1,nodes:size(),1 
      do
        local f=fields[i]
        f:insertXml( nodes:at(i) )
      end
      end,
    function(err) document:addField("FAIL2",err) end
  )
  else
  end

end

function mapXMLTagsToFields(document,params)
  local fieldName = params[2]
  local xpathQuery = params[3]
  local xml_path_field = params[4]
  local override = params[5]
  local xml_path = ""
  if xml_path_field and document:hasField(xml_path_field) then
    xml_path =  getMyFieldValue(document,xml_path_field)
  else
    xml_path = document:getReference()
  end
   --log:write_line( log_level_always() , "XML Path : "..xml_path)
  local xml = parse_xml(read_file(xml_path))
  xpathQuery = resolveVariablesBetweenBrackets(document,xpathQuery)
  if xml ~= nil then
    local values = {xml:XPathValues(xpathQuery)}
    for i, value in ipairs(values) do
     -- log:write_line( log_level_always() , "XPathValue : "..value..fieldName..xpathQuery)
      if value and value ~= "" then
        setMyFieldValue(document, fieldName, value, override)
      end
    end
  else
    log:write_line( log_level_always() , "Error in parsing xml file: "..xml_path ..xpathQuery)
  end
end

function addFields(document,params)
  local is_pattern_valid = true
  local fieldName = params[2]
  local valPattern = params[3]
  local strictMode = params[4]
  local override = params[5]
  local valPatternList = split(valPattern,",")
  local value =""
  for n,unitPattern in pairs(valPatternList) do
      local append_value = getFieldValueWithBracket(document, unitPattern)
      if append_value == nil and strictMode == "true" then
          is_pattern_valid = false
          break
      elseif(append_value ~= nil) then
        value = value..append_value
      end
  end
  if is_pattern_valid == true then
    if (fieldName=="DREREFERENCE") then
      document:setReference(value)
    elseif (fieldName=="DRECONTENT") then
      document:deleteField("DRECONTENT",false)
      document:setContent(value)
    else
      if override ~= nil then
      --log:write_line( log_level_always() , "Override value : "..override..fieldName..valPattern)
      end
      setMyFieldValue(document, fieldName, value, override)
    end
  end
end

function addFieldValueFromRegexMatch(document,params)
  local sourceFieldName = params[2]
  local destinationFieldName = params[3]
  local patterns = params[4]
  local override = params[5]
  local valPatternList = split(patterns,",")
  local sourceFieldValues = getMyFieldValues(document,sourceFieldName)
  for i,  sourceFieldValue in ipairs(sourceFieldValues) do
    for n,unitPattern in pairs(valPatternList) do
      unitPattern = resolveVariablesBetweenBrackets(document,unitPattern)
      local match = string.match(sourceFieldValue, unitPattern)
      if match then
        setMyFieldValue(document, destinationFieldName, match, override)
      end
    end
  end
end

function addConditionalFieldValueFromRegexMatch(document,params)
  local sourceFieldName = params[2]
  local destinationFieldName = params[3]
  local patterns = params[4]
  local destinationFieldValueifOK = params[5]
  local destinationFieldValueifKO = params[6]
  local override = params[7]
  local sourceFieldValues = getMyFieldValues(document,sourceFieldName)
  local valPatternList = split(patterns,",")
  for i,  sourceFieldValue in ipairs(sourceFieldValues) do
    sourceFieldValue = string.gsub(sourceFieldValue, "%%" , "XXXX_PROC")
    for n,unitPattern in pairs(valPatternList) do
       unitPattern = resolveVariablesBetweenBrackets(document,unitPattern)
      local match = string.match(sourceFieldValue, unitPattern)
      if match then
        --log:write_line( log_level_always() , "Source : "..i.." "..sourceFieldValue.." Pattern "..unitPattern.." Match "..match.." OK Val "..destinationFieldValueifOK.." KO Val "..destinationFieldValueifKO)
        destinationFieldValueifOK = resolveVariablesBetweenBrackets(document,destinationFieldValueifOK)
        setMyFieldValue(document, destinationFieldName, destinationFieldValueifOK, override)
        break
      elseif destinationFieldValueifKO ~= nil and destinationFieldValueifKO ~= " " then
        --log:write_line( log_level_always() , "Source : "..i.." "..sourceFieldValue.." Pattern "..unitPattern.." Match ".."NOT matched".." OK Val "..destinationFieldValueifOK.." KO Val "..destinationFieldValueifKO)
        destinationFieldValueifKO = resolveVariablesBetweenBrackets(document,destinationFieldValueifKO)
        setMyFieldValue(document, destinationFieldName, destinationFieldValueifKO, override)
      end
    end
  end
end


function is_musthave_condition_valid(document,params)
  local is_valid = false
  local sourceFieldName = params[2]
  local checkValue = params[3]
  local sourceFieldValues = getMyFieldValues(document,sourceFieldName)
  --log:write_line( log_level_always() , "Check before resolving : "..checkValue)
  checkValue = resolveVariablesBetweenBrackets(document,checkValue)
  --log:write_line( log_level_always() , "Check after resolving : "..checkValue)
  for _, sourceFieldValue in ipairs(sourceFieldValues) do
    local match = string.match(sourceFieldValue, checkValue)
    if match then
      is_valid = is_valid or true
-- AD 2021.05.24
      break
-- AD 2021.05.24
    else
      is_valid = is_valid or false
    end
  end
  return is_valid
end

-- AD 2021.05.24
function is_musthave_condition_valid_two_re(document,params)
  local is_valid = false
  local sourceFieldName = params[2]
  local checkValue = params[3]
  local checkValue1 = params[4]
  local sourceFieldValues = getMyFieldValues(document,sourceFieldName)
  --log:write_line( log_level_always() , "Check before resolving : "..checkValue)
  checkValue = resolveVariablesBetweenBrackets(document,checkValue)
  checkValue1 = resolveVariablesBetweenBrackets(document,checkValue1)
  --log:write_line( log_level_always() , "Check after resolving : "..checkValue)
  for _, sourceFieldValue in ipairs(sourceFieldValues) do
    local match = string.match(sourceFieldValue, checkValue)
    local match1 = string.match(sourceFieldValue, checkValue1)
    if match or match1 then
      is_valid =  true
      break
    end
  end
  return is_valid
end
-- AD 2021.05.24

function is_canthave_condition_valid(document,params)
  local is_valid = true
  local sourceFieldName = params[2]
  local checkValue = params[3]
  local sourceFieldValues = getMyFieldValues(document,sourceFieldName)
  checkValue = resolveVariablesBetweenBrackets(document,checkValue)
  for i,  sourceFieldValue in ipairs(sourceFieldValues) do
    local match = string.match(sourceFieldValue, checkValue)
    if match then
      is_valid = is_valid and false
    else
      is_valid = is_valid and true
    end
  end
  return is_valid
end

function addDateField(document,params)
  local sourceFieldName = params[2]
  local DestinationFieldName = params[3]
  local sourceFormat = params[4]
  local DestinationFormat = params[5]
  setDateField (document, sourceFieldName, DestinationFieldName, sourceFormat, DestinationFormat)
end

function importDocument(document,params)
  local sourceFileSpecifier = params[2]
  local checkField = params[3]
  local checkValue = params[4]
  local override = params[5]
  if override == true or gobble_whitespace(string.lower(tostring(override)))=="true" then
     local value =""
     document:deleteField("DRECONTENT",false)
     document:setContent(value)
  end
  if checkField and CheckValue and document:hasField(checkField) then
    local checkFieldValue = getMyFieldValue(document,checkField)
    local match = string.match(checkFieldValue, checkValue)
    if match then
      sourceFileSpecifier = resolveVariablesBetweenBrackets(document,sourceFileSpecifier)
      --log:write_line( log_level_always() , "PDF file Path : "..sourceFileSpecifier)
      import_into_this_document( document, sourceFileSpecifier )
    end
  else
      sourceFileSpecifier = resolveVariablesBetweenBrackets(document,sourceFileSpecifier)
      --log:write_line( log_level_always() , "PDF file Path : "..sourceFileSpecifier)
      import_into_this_document( document, sourceFileSpecifier )
  end
end

function replaceAllValues(document,params)
  local sourceFieldName = params[2]
  local destinationFieldName = params[3]
  local pattern = params[4]
  local replacement = params[5]
  local override = params[6]
  pattern = resolveVariablesBetweenBrackets(document,pattern)
  local sourceFieldValue = getMyFieldValue(document,sourceFieldName)
  if ( sourceFieldValue and pattern and replacement ) then
    new_value = string.gsub(sourceFieldValue, pattern , replacement)
    --if override then log:write_line( log_level_always() , "override : ".. override) end
    setMyFieldValue(document, destinationFieldName, new_value, override)
  end
end

function replaceValues(document,params)
  local sourceFieldName = params[2]
  local destinationFieldName = params[3]
  local pattern = params[4]
  local replacement = params[5]
  local override = params[6]
  pattern = resolveVariablesBetweenBrackets(document,pattern)
  replacement = string.gsub(replacement, "empty", "")
  local sourceFieldValue = getMyFieldValue(document,sourceFieldName)
  if ( sourceFieldValue and pattern and replacement ) then
      new_value  = string.gsub(sourceFieldValue, pattern , replacement)
      --if override then log:write_line( log_level_always() , "override : ".. override) end
      if new_value ~= sourceFieldValue then
        setMyFieldValue(document, destinationFieldName, new_value, override)
      end
  end
end
    
function compactFieldValues(document,params)
  local sourceFieldName = params[2]
  local separator = params[3]
  local sourceFieldValues = getMyFieldValues(document,sourceFieldName)
  local concatenated_value =""
  for i,  sourceFieldValue in ipairs(sourceFieldValues) do
    concatenated_value = concatenated_value .. sourceFieldValue
    if i > 0 and i < length(sourceFieldValues) then
      concatenated_value = concatenated_value .. separator
    end
    deleteMyFieldValue(document,sourceFieldName,sourceFieldValue)
  end
  if string.len(concatenated_value) > 0 then
    setMyFieldValue(document, sourceFieldName, concatenated_value, true)
  end
end

function expandFieldValues(document,params)
   local sourceFieldName = params[2]
   local destinationFieldName= params[3]
   local separator = params[4]
   local enumerate = params[5]
   local sourceFieldValues = getMyFieldValues(document,sourceFieldName)
   --log:write_line( log_level_always() , "separator="..separator)
   --log:write_line( log_level_always() , "FieldName="..sourceFieldName)
   for i,  sourceFieldValue in ipairs(sourceFieldValues) do
     --log:write_line( log_level_always() , "Full Content Value="..sourceFieldValue)
     local values = split(sourceFieldValue, separator)
     for n,value in pairs(values) do
       -- log:write_line( log_level_always() , "Content split= : ".. value .. n .. sourceFieldValue)
      if  enumerate == true or gobble_whitespace(string.lower(tostring(enumerate)))=="true" then
        addMyFieldValue(document, destinationFieldName.."_"..n, value)
      else
        addMyFieldValue(document, destinationFieldName, value)
      end
     end
     if sourceFieldName == destinationFieldName  then
      deleteMyFieldValue(document,sourceFieldName,sourceFieldValue)
     end
   end
end

function applyTextToDocs(document,params)
  local texToDocsSectionName = params[2]
  local file = get_filename(document)
  --log:write_line( log_level_always() , "File name "..file)
  local docs = text_to_docs(document, texToDocsSectionName, file)
  for i, doc in ipairs(docs) do
      doc:addField("PROCESSED", "YES")
      ingest(doc)
  end
  return true
end

function HTMLToText(document,params)
  local fieldName = params[2]
  local fieldValue = getMyFieldValue(document,fieldName)
  if fieldValue then
    local clean_value = extract_text(fieldValue, true)
    if clean_value then
      setMyFieldValue(document, fieldName, gobble_whitespace(clean_value), true)
    end
  end
end

function deleteFields(document,params)
  local fieldName = params[2]
  while document:hasField(fieldName,true) do
    deleteMyField(document, fieldName)
  end
end

function toUpper(document,params)
  local sourceFieldName = params[2]
  local destinationFieldName = params[3]
  local override = params[4]
  local sourceFieldValues = getMyFieldValues(document,sourceFieldName)
  for i,  sourceFieldValue in ipairs(sourceFieldValues) do
    local new_value = string.upper(sourceFieldValue)
    if override == true or gobble_whitespace(string.lower(tostring(override)))=="true" then
      deleteMyFieldValue(document, destinationFieldName, sourceFieldValue)
      addMyFieldValue(document, destinationFieldName, new_value)
    else
      addMyFieldValue(document, destinationFieldName, new_value)
    end
  end
end

function toLower(document,params)
  local sourceFieldName = params[2]
  local destinationFieldName = params[3]
  local override = params[4]
  local sourceFieldValues = getMyFieldValues(document,sourceFieldName)
  for i,  sourceFieldValue in ipairs(sourceFieldValues) do
    local new_value = string.lower(sourceFieldValue)
    if override == true or gobble_whitespace(string.lower(tostring(override)))=="true" then
      document:deleteField( destinationFieldName, sourceFieldValue)
      addMyFieldValue(document, destinationFieldName, new_value)
    else
      addMyFieldValue(document, destinationFieldName, new_value)
    end
  end
end

function substring(document,params)
  local sourceFieldName = params[2]
  local destinationFieldName = params[3]
  local fromPosition = tonumber(params[4])
  local toPosition =  tonumber(params[5])
  local override = params[6]
  local sourceFieldValue = getMyFieldValue(document,sourceFieldName)
  if sourceFieldValue ~= nil then
    if toPosition < length(sourceFieldValue) then
      new_value = string.sub(sourceFieldValue, fromPosition, toPosition)
    else
      new_value = sourceFieldValue
    end
    if fromPosition >= length(sourceFieldValue) then
      new_value = "."
    end
    if override == true or gobble_whitespace(string.lower(tostring(override)))=="true" then
      document:deleteField( destinationFieldName, sourceFieldValue)
      addMyFieldValue(document, destinationFieldName, new_value)
    else
      addMyFieldValue(document, destinationFieldName, new_value)
    end
  else
    new_value = "."
    addMyFieldValue(document, destinationFieldName, new_value)
  end
end

function setFieldAttributeValue(document,params)
  local fieldName = params[2]
  local attributeName = params[3]
  local attributeValue = params[4]
  setMyFieldAttributeValue (document, fieldName, attributeName, attributeValue)
end

function setFieldAttributeXMLValue(document,params)
  local fieldName = params[2]
  local attributeName = params[3]
  local xpathQuery = params[4]
  local xml_path = document:getReference()
  local xml = parse_xml(read_file(xml_path))
  xpathQuery = resolveVariablesBetweenBrackets(document,xpathQuery)
  local values = {xml:XPathValues(xpathQuery.."/text()")}
  for i, value in ipairs(values) do
    if value and value ~= "" then
	setMyFieldAttributeValue (document, fieldName, attributeName, value)
    end
  end
end

function addXML(document,params)
  local override = params[2]
  local xml_path = document:getReference()
  log:write_line( log_level_always() , "XML Path : "..xml_path)
  local xml = parse_xml(read_file(xml_path))
  local node = xml:root()
	document:insertXML(node)
end
