function handler(document)
        local title_fieldname = "Title"
	local description_fieldname = "description"
        local title = document:getFieldValue(title_fieldname)
        local description = document:getFieldValue(description_fieldname)
	local dbname = document:getFieldValue("DREDBNAME")
	local cc = string.sub(dbname, 5, 6)
	local language = document:getFieldValue("LANGUAGE")
	local lc = string.sub(language, 1, 2)

        if cc == "WW" then country = "Global"
        elseif cc == "DZ" then country = "Algérie"
        elseif cc == "AR" then country = "Argentina"
        elseif cc == "AU" then country = "Australia"
        elseif cc == "AT" then country = "Österreich"
        elseif cc == "BE" and lc == "nl"  then country = "België"
        elseif cc == "BE" and lc == "fr" then country = "Belgique"
        elseif cc == "BR" then country = "Brasil"
        elseif cc == "BG" then country = "България"
        elseif cc == "CA" and lc == "en" then country = "Canada"
        elseif cc == "CA" and lc == "fr" then country = "Canada"
        elseif cc == "CL" then country = "Chile"
        elseif cc == "CO" then country = "Colombia"
        elseif cc == "HR" then country = "Hrvatska"
        elseif cc == "CZ" then country = "Česká Republika"
        elseif cc == "DK" then country = "Danmark"
        elseif cc == "EG" and lc == "en" then country = "Egypt"
        elseif cc == "EG" and lc == "ar" then country = "مصر"
        elseif cc == "AE" then country = "UAE"
        elseif cc == "EE" then country = "Eesti"
        elseif cc == "FI" then country = "Suomi"
        elseif cc == "FR" then country = "France"
        elseif cc == "XF" then country = "Afrique francophone"
        elseif cc == "DE" then country = "Deutschland"
        elseif cc == "GR" then country = "Ελλάς"
        elseif cc == "HK" then country = "Hong Kong, China"
        elseif cc == "HU" then country = "Magyarország"
        elseif cc == "IN" then country = "India"
        elseif cc == "ID" and lc == "en" then country = "Indonesia"
        elseif cc == "ID" and lc == "id" then country = "Indonesia"
        elseif cc == "IE" then country = "Ireland"
        elseif cc == "IL" then country = "ישראל"
        elseif cc == "IT" then country = "Italia"
        elseif cc == "JP" then country = "日本"
        elseif cc == "KZ" then country = "Казахстан"
        elseif cc == "KR" then country = "대한민국"
        elseif cc == "LV" then country = "Latvija"
        elseif cc == "LT" then country = "Lietuva"
        elseif cc == "MY" then country = "Malaysia"
        elseif cc == "MX" then country = "México"
        elseif cc == "MA" then country = "Maroc"
        elseif cc == "NL" then country = "Nederland"
        elseif cc == "NZ" then country = "New Zealand"
        elseif cc == "NG" then country = "Nigeria"
        elseif cc == "NO" then country = "Norge"
        elseif cc == "PE" then country = "Perú"
        elseif cc == "PH" then country = "Philippines"
        elseif cc == "PL" then country = "Polska"
        elseif cc == "PT" then country = "Portugal"
        elseif cc == "RO" then country = "România"
        elseif cc == "RU" then country = "Россия"
        elseif cc == "SA" and lc == "en" then country = "Saudi Arabia"
        elseif cc == "SA" and lc == "ar" then country = "المملكة العربية السعودية"
        elseif cc == "RS" then country = "Србија"
        elseif cc == "SG" then country = "Singapore"
        elseif cc == "SK" then country = "Slovensko"
        elseif cc == "SI" then country = "Slovenija"
        elseif cc == "ZA" then country = "South Africa"
        elseif cc == "ES" then country = "España"
        elseif cc == "SE" then country = "Sverige"
        elseif cc == "CH" and lc == "de"  then country = "Schweiz"
        elseif cc == "CH" and lc == "fr" then country = "Suisse"
        elseif cc == "TW" then country = "Taiwan, China"
        elseif cc == "TH" and lc == "th" then country = "ประเทศไทย"
        elseif cc == "TH" and lc == "en" then country = "Thailand"
        elseif cc == "TR" then country = "Türkiye"
        elseif cc == "UA" and lc == "ru" then country = "Украина"
        elseif cc == "UA" and lc == "uk" then country = "Україна"
        elseif cc == "UK" then country = "UK"
        elseif cc == "US" then country = "USA"
        elseif cc == "VN" and lc == "en" then country = "Vietnam"
        elseif cc == "VN" and lc == "vi" then country = "Việt Nam"
	elseif cc == "CN" then country = ""
	end

     if cc == "FR" then
	local prof_cat = document:getFieldValue("PROF_CAT")
	if prof_cat == "CAT_B2C" then
	    cat = "Particuliers"
	else
	    cat = "Professionnels"
	end
	
        if title ~= nil and title ~= "" then
                title = string.gsub(title,"[%p%c%s]*"..cat.."%s*|%s*Schneider.*$", "")
                document:setFieldValue(title_fieldname,title)
        end

        if description ~= nil and description ~= "" then
                description = string.gsub(description,"^.*"..country..".*"..cat.."[%p%c%s]*", "")
                document:setFieldValue(description_fieldname,description)
        end
     elseif cc == "CN" then
	if title ~= nil and title ~= "" then
                title = string.gsub(title,"%s*|%s*Schneider.*$", "")
                document:setFieldValue(title_fieldname,title)
        end
     else
	if title ~= nil and title ~= "" then
        --        title = string.gsub(title,"%s*|.*$", "")
		title = string.gsub(title,"%s*|%s*Schneider.*$", "")
		document:setFieldValue(title_fieldname,title)
	end

	if description ~= nil and description ~= "" then
                description = string.gsub(description,"^.*"..country.."[%p%c%s]*", "")
		document:setFieldValue(description_fieldname,description)
	end
     end

        return true
end

