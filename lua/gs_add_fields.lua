function handler(document)
        local guided_scope_fieldname = "GUIDED_CAT"
        local ct_cat = document:getFieldValue("CT_CAT")
        if ct_cat == "CAT_PRD_DATA" then
                document:setFieldValue(guided_scope_fieldname, "PRODUCT")
        elseif ct_cat == "CAT_PRD_OFFER" then
                document:setFieldValue(guided_scope_fieldname, "RANGE")
        elseif ct_cat == "CAT_PRD_NAV" then
                document:setFieldValue(guided_scope_fieldname, "CATEGORY")
        else
                document:setFieldValue(guided_scope_fieldname, "OTHER")
        end
        local title =document:getFieldValue("GS_KEYWORDS")
        if title ~= nil and title ~= "" then
                title = string.gsub(title,"^[%p%c%s]*(.-)", "%1")
                if title ~= nil and title ~= "" then
                        title = string.gsub(title, "-", "")
                        if title ~= nil and title ~= "" then
                        title = string.gsub(title,"^%s*(.-)%s*$", "%1")
                                if title ~= nil and title ~= "" then
                                        local match = string.match(title,"^([^%s]+)")
                                        if match ~= nil then
                                                document:setFieldValue("FIRST_WORD", match)
                                        end
                                end
                        end
                end
        end
	local prof_cat = document:getFieldValue("PROF_CAT")
	if prof_cat == nil or prof_cat == "" then
		document:setFieldValue("PROF_CAT", "CAT_B2B")
	end

        local dretitle = document:getFieldValue("DRETITLE")
	local language = document:getFieldValue("LANGUAGE")
        if dretitle ~= nil and dretitle ~= "" then
	  if language == "de_utf" or language == "en_utf" then	
		clean_title = string.gsub(dretitle,"%sf[uo]r%s.*$", "")
		clean_title = string.gsub(clean_title,"%sfür%s.*$", "")
		clean_title = string.gsub(clean_title,"%sfuer%s.*$", "")
		clean_title = string.gsub(clean_title,"%sf%.%s.*$", "")
	  else
		clean_title = dretitle
	  end
	end

	if clean_title ~= nil and clean_title ~= "" then
	  document:setFieldValue("CLEAN_TITLE", clean_title)
	end

        return true
end

