-- library utility


function getMyFieldValue(document, fieldName)
        local fieldValue = document:getFieldValue(fieldName)

        return fieldValue
end

function getMyFieldValues(document, fieldName)
        local fieldValues = { document:getFieldValues(fieldName) }

        return fieldValues
end

function addMyFieldValue(document, fieldName, fieldValue)

        document:addField(fieldName, fieldValue);

        return true

end

function setMyFieldValue(document, fieldName, fieldValue, override)
        if override ~= nil then
            if gobble_whitespace(string.lower(tostring(override))) == "true" or override == true then
              --log:write_line( log_level_always() , "setting field "..fieldName .. " with value : ".. fieldValue .." with override enabled "..tostring(override))
              document:setFieldValue(fieldName, fieldValue);
            elseif string.lower(tostring(override)) == "false" or override == false then
              addMyFieldValue(document, fieldName, fieldValue)
            end
        else
          --log:write_line( log_level_always() , "setting field "..fieldName .. " with value : ".. fieldValue .. " with override disabled")
          addMyFieldValue(document, fieldName, fieldValue)
        end

        return true

end


function copyMyField(document, sourceFieldName, targetFieldName )

        local fieldValue = getMyFieldValue(document, sourceFieldName)

        if fieldValue then
                setMyFieldValue(document, targetFieldName , fieldValue );
        end

        return true
end

function deleteMyField(document, fieldName)
        document:deleteField(fieldName)

        return true
end

function deleteMyFieldValue(document, fieldName , fieldValue)
        document:deleteField(fieldName,fieldValue)

        return true
end

function setDateField (document, fromField, toField, format, extractDateToFormat)
  local extractDateFromFieldValue
	if string.lower(fromField) == "now" then
    extractDateFromFieldValue = tostring(os.time)
  else
    extractDateFromFieldValue  = getMyFieldValue(document, fromField)
  end
	if extractDateFromFieldValue then 
    
    --log:write_line( log_level_always() , "Epoch Time "..extractDateFromFieldValue .. " From Forma: ".. format .." To Format "..extractDateToFormat)
		local resultDateFormat = convertDateTime(extractDateFromFieldValue, format, extractDateToFormat)

		setMyFieldValue(document, toField, resultDateFormat)
	
	end 
	
	return true

end

function convertDateTime(extractDateFromFieldValue, format, extractDateToFormat) 

	local result = ""

	if extractDateFromFieldValue then 

	  result = convert_date_time(extractDateFromFieldValue, format, extractDateToFormat)

	end 

	return result
end 


-- reduces multiple adjacent white spaces (tabs, carriage returns, spaces, and so on
function reduceAdjacentWhiteSpaces(input) 

  local output = ""


  if input then

	output = gobble_whitespace(input)

  end 


  return output


end



-- field glue
function fieldGlue (document, destinationField, sourceField, separator) 

   local value1 = getMyFieldValue(document, destinationField)
   local value2 = getMyFieldValue(document, sourceField)




  local finalResult =  value1 .. separator .. value2 

  
  setMyFieldValue(document, destinationField, finalResult)



  return true

end 
------------------------------------------------------------
-- split a string based on a pattern
------------------------------------------------------------
function split_old(str,pat)
    local tbl = {}
    local fpat = "(.-)" .. pat
    local last_end = 1
    local s, e, cap = string.find(str,fpat,last_end)
    while s do
        if (s ~= 1 or cap ~= "") then
            table.insert(tbl,cap)
        end
        last_end = e + 1
        s, e, cap = string.find(str,fpat,last_end)
    end
    if (last_end <= #str) then
        cap = string.sub(str,last_end)
        table.insert(tbl,cap)
    end
    return tbl
end

function valid(data, array)
 local valid = {}
 for i = 1, #array do
  valid[array[i]] = true
 end
 if valid[data] then
  return true
 else
  return false
 end
end 


function file_exists(file)
  local f = io.open(file, "r")
  if f then f:close() end
  return f ~= nil
end

function read_file(file)
  if not file_exists(file) then return "Empty" end
  content = ""
  local f = io.open(file, "r")
  content = f:read("*all")
  --for line in io.lines(file) do 
  -- content = content..line
  --end
  return content
end

-- This is an attempt to handle CSV strings allowing for quoted values with quote escaping,
-- or unquoted values with comma escaping.  It does not cover every possible case but should
-- support a sufficiently large set.
function split(str, delim)
    local result = {}
    local num = 1
    local lastPos = 1
    local quotepat = '^"(.-[^\\])"'..delim..'()'
    local quotepat2 = '^"(.-[^\\]\\\\)"'..delim..'()'
    local quotefpat = '^"(.-[^\\])"$'
    local quotefpat2 = '^"(.-[^\\]\\\\)"$'
    local commapat = '^(.-[^\\])'..delim..'()'
    local quotef = false
    local s, e, value, pos = string.find(str, quotepat, 1)
    if s == nil then
    	s, e, value, pos = string.find(str, quotepat2, 1)
    end	
    if s == nil then
    	s, e, value = string.find(str, quotefpat, 1)
    	quotef = true
    end	
    if s == nil then
    	s, e, value = string.find(str, quotefpat2, 1)
    	quotef = true
    end	
    if s == nil then
    	quotef = false
    	s, e, value, pos = string.find(str, commapat, 1)
    end	
    while s do
		result[num] = string.gsub(value, "\\(.)", "%1")
		if quotef == true then
			return result
		end
		num = num + 1
		lastPos = pos
		s, e, value, pos = string.find(str, quotepat, lastPos)
		if s == nil then
			s, e, value, pos = string.find(str, quotepat2, lastPos)
		end	
		if s == nil then
			s, e, value = string.find(str, quotefpat, lastPos)
	    	quotef = true
		end	
		if s == nil then
			s, e, value = string.find(str, quotefpat2, lastPos)
	    	quotef = true
		end	
		if s == nil then
	    	quotef = false
			s, e, value, pos = string.find(str, commapat, lastPos)
		end	
    end
	result[num] = string.gsub(string.sub(str, lastPos), "\\(.)", "%1")
    return result
end

-- Convert  parameter fieldname between bracket to field value or return original string.
function getFieldValueWithBracket( document, str )
	local fieldName = string.match(str, "^{(.-)}$")
	if nil ~= fieldName then
		local value = document:getFieldValue(fieldName)
		return value  
	else
		return str
	end
end
-- read an xpath with fieldnames between brackets, and return the expanded expression of the xpath
function resolveVariablesBetweenBrackets( document, str)
  while true do
    local fieldName = string.match(str, ".*{(.-)}.*")
    if fieldName == nil then break end
    local value = document:getFieldValue(fieldName)
    if value then
      value= string.gsub(value, "%%", "PROC_XXX")
      str = string.gsub(str,"(.*)({.-})(.*)","%1"..value.."%3",1)
      str = string.gsub(str, "PROC_XXX", "%%")
    else
      str = string.gsub(str,"(.*)({.-})(.*)","%1".."".."%3",1)
    end
  end
  --log:write_line( log_level_always() , str)
  return str
end

function length(t)
  return #t
end

function setMyFieldAttributeValue(document, fieldName, attributeName, attributeValue)
		local myField = document:getField(fieldName)
		if document:hasField(fieldName) then
			myField:setAttributeValue(attributeName, attributeValue)
		end

		return true
		
end
