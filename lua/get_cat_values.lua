function handler(document)
	local dbname = document:getFieldValue("DREDBNAME")
	local content_source = document:getFieldValue("CONTENT_SOURCE")

	local doc_type_group = document:getFieldValue("DOC_TYPE_GROUP_NAME")
	local doc_type_group_translation = ""

	if doc_type_group ~= nil and doc_type_group ~= "" then
		doc_type_group_translation = string.gmatch(doc_type_group, "[^#]+$")()
	end

	local doc_type = document:getFieldValue("DOC_TYPE_NAME")
	local doc_type_translation = document:getFieldValue("DOC_TYPE_TRANSLATION")

	local language = document:getFieldValue("LANGUAGE")
	local country = string.sub(dbname, 5, 6)
	local f1_cat_fieldname = "F1_CAT"
	local f2_cat_fieldname = "F2_CAT"
	local f3_cat_fieldname = "F3_CAT"
	local f1_cat_value = ""
	local f2_cat_value = ""
	local f3_cat_value = ""


    if content_source == "BSL_DOC" and doc_type == "GSD files" then
        if language == "de_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "no_utf" then
            f1_cat_value = "Programvare"
            f2_cat_value = doc_type_translation
        elseif language == "fi_utf" then
            f1_cat_value = "Ohjelmistot"
            f2_cat_value = doc_type_translation
        elseif language == "ru_utf" then
            f1_cat_value = "Программное обеспечение"
            f2_cat_value = doc_type_translation
        elseif language == "bg_utf" then
            f1_cat_value = "Софтуер"
            f2_cat_value = doc_type_translation
        elseif language == "lt_utf" then
            f1_cat_value = "Programinė įranga"
            f2_cat_value = doc_type_translation
        elseif language == "hr_utf" then
            f1_cat_value = "Softver"
            f2_cat_value = doc_type_translation
        elseif language == "lv_utf" then
            f1_cat_value = "Programmatūra"
            f2_cat_value = doc_type_translation
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "fr_utf" then
            f1_cat_value = "Logiciels"
            f2_cat_value = doc_type_translation
        elseif language == "hu_utf" then
            f1_cat_value = "Szoftver"
            f2_cat_value = doc_type_translation
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "軟體"
            f2_cat_value = doc_type_translation
        elseif language == "uk_utf" then
            f1_cat_value = "Програмне забезпечення"
            f2_cat_value = doc_type_translation
        elseif language == "sk_utf" then
            f1_cat_value = "Softvér"
            f2_cat_value = doc_type_translation
        elseif language == "sl_utf" then
            f1_cat_value = "Programska oprema"
            f2_cat_value = doc_type_translation
        elseif language == "id_utf" then
            f1_cat_value = "Perangkat Lunak"
            f2_cat_value = doc_type_translation
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "sr_utf" then
            f1_cat_value = "Softver"
            f2_cat_value = doc_type_translation
        elseif language == "sv_utf" then
            f1_cat_value = "Programvara"
            f2_cat_value = doc_type_translation
        elseif language == "ko_utf" then
            f1_cat_value = "소프트웨어"
            f2_cat_value = doc_type_translation
        elseif language == "el_utf" then
            f1_cat_value = "Λογισμικό"
            f2_cat_value = doc_type_translation
        elseif language == "en_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "it_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "et_utf" then
            f1_cat_value = "Tarkvara"
            f2_cat_value = doc_type_translation
        elseif language == "cs_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "ar_utf" then
            f1_cat_value = "البرامج"
            f2_cat_value = doc_type_translation
        elseif language == "vi_utf" then
            f1_cat_value = "Phần mềm"
            f2_cat_value = doc_type_translation
        elseif language == "th_utf" then
            f1_cat_value = "ซอฟต์แวร์"
            f2_cat_value = doc_type_translation
        elseif language == "ja_utf" then
            f1_cat_value = "ソフトウェア"
            f2_cat_value = doc_type_translation
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "软件"
            f2_cat_value = doc_type_translation
        elseif language == "pl_utf" then
            f1_cat_value = "Oprogramowanie"
            f2_cat_value = doc_type_translation
        elseif language == "da_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "he_utf" then
            f1_cat_value = "תוכנה"
            f2_cat_value = doc_type_translation
        elseif language == "ro_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "nl_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "tr_utf" then
            f1_cat_value = "Yazılımlar"
            f2_cat_value = doc_type_translation
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        end
    elseif content_source == "BSL_DOC" and doc_type == "Firmware" then
        if language == "de_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "no_utf" then
            f1_cat_value = "Programvare"
            f2_cat_value = doc_type_translation
        elseif language == "fi_utf" then
            f1_cat_value = "Ohjelmistot"
            f2_cat_value = doc_type_translation
        elseif language == "ru_utf" then
            f1_cat_value = "Программное обеспечение"
            f2_cat_value = doc_type_translation
        elseif language == "bg_utf" then
            f1_cat_value = "Софтуер"
            f2_cat_value = doc_type_translation
        elseif language == "lt_utf" then
            f1_cat_value = "Programinė įranga"
            f2_cat_value = doc_type_translation
        elseif language == "hr_utf" then
            f1_cat_value = "Softver"
            f2_cat_value = doc_type_translation
        elseif language == "lv_utf" then
            f1_cat_value = "Programmatūra"
            f2_cat_value = doc_type_translation
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "fr_utf" then
            f1_cat_value = "Logiciels"
            f2_cat_value = doc_type_translation
        elseif language == "hu_utf" then
            f1_cat_value = "Szoftver"
            f2_cat_value = doc_type_translation
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "軟體"
            f2_cat_value = doc_type_translation
        elseif language == "uk_utf" then
            f1_cat_value = "Програмне забезпечення"
            f2_cat_value = doc_type_translation
        elseif language == "sk_utf" then
            f1_cat_value = "Softvér"
            f2_cat_value = doc_type_translation
        elseif language == "sl_utf" then
            f1_cat_value = "Programska oprema"
            f2_cat_value = doc_type_translation
        elseif language == "id_utf" then
            f1_cat_value = "Perangkat Lunak"
            f2_cat_value = doc_type_translation
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "sr_utf" then
            f1_cat_value = "Softver"
            f2_cat_value = doc_type_translation
        elseif language == "sv_utf" then
            f1_cat_value = "Programvara"
            f2_cat_value = doc_type_translation
        elseif language == "ko_utf" then
            f1_cat_value = "소프트웨어"
            f2_cat_value = doc_type_translation
        elseif language == "el_utf" then
            f1_cat_value = "Λογισμικό"
            f2_cat_value = doc_type_translation
        elseif language == "en_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "it_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "et_utf" then
            f1_cat_value = "Tarkvara"
            f2_cat_value = doc_type_translation
        elseif language == "cs_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "ar_utf" then
            f1_cat_value = "البرامج"
            f2_cat_value = doc_type_translation
        elseif language == "vi_utf" then
            f1_cat_value = "Phần mềm"
            f2_cat_value = doc_type_translation
        elseif language == "th_utf" then
            f1_cat_value = "ซอฟต์แวร์"
            f2_cat_value = doc_type_translation
        elseif language == "ja_utf" then
            f1_cat_value = "ソフトウェア"
            f2_cat_value = doc_type_translation
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "软件"
            f2_cat_value = doc_type_translation
        elseif language == "pl_utf" then
            f1_cat_value = "Oprogramowanie"
            f2_cat_value = doc_type_translation
        elseif language == "da_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "he_utf" then
            f1_cat_value = "תוכנה"
            f2_cat_value = doc_type_translation
        elseif language == "ro_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "nl_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "tr_utf" then
            f1_cat_value = "Yazılımlar"
            f2_cat_value = doc_type_translation
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        end
    elseif content_source == "BSL_DOC" and doc_type == "EDS files" then
        if language == "de_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "no_utf" then
            f1_cat_value = "Programvare"
            f2_cat_value = doc_type_translation
        elseif language == "fi_utf" then
            f1_cat_value = "Ohjelmistot"
            f2_cat_value = doc_type_translation
        elseif language == "ru_utf" then
            f1_cat_value = "Программное обеспечение"
            f2_cat_value = doc_type_translation
        elseif language == "bg_utf" then
            f1_cat_value = "Софтуер"
            f2_cat_value = doc_type_translation
        elseif language == "lt_utf" then
            f1_cat_value = "Programinė įranga"
            f2_cat_value = doc_type_translation
        elseif language == "hr_utf" then
            f1_cat_value = "Softver"
            f2_cat_value = doc_type_translation
        elseif language == "lv_utf" then
            f1_cat_value = "Programmatūra"
            f2_cat_value = doc_type_translation
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "fr_utf" then
            f1_cat_value = "Logiciels"
            f2_cat_value = doc_type_translation
        elseif language == "hu_utf" then
            f1_cat_value = "Szoftver"
            f2_cat_value = doc_type_translation
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "軟體"
            f2_cat_value = doc_type_translation
        elseif language == "uk_utf" then
            f1_cat_value = "Програмне забезпечення"
            f2_cat_value = doc_type_translation
        elseif language == "sk_utf" then
            f1_cat_value = "Softvér"
            f2_cat_value = doc_type_translation
        elseif language == "sl_utf" then
            f1_cat_value = "Programska oprema"
            f2_cat_value = doc_type_translation
        elseif language == "id_utf" then
            f1_cat_value = "Perangkat Lunak"
            f2_cat_value = doc_type_translation
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "sr_utf" then
            f1_cat_value = "Softver"
            f2_cat_value = doc_type_translation
        elseif language == "sv_utf" then
            f1_cat_value = "Programvara"
            f2_cat_value = doc_type_translation
        elseif language == "ko_utf" then
            f1_cat_value = "소프트웨어"
            f2_cat_value = doc_type_translation
        elseif language == "el_utf" then
            f1_cat_value = "Λογισμικό"
            f2_cat_value = doc_type_translation
        elseif language == "en_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "it_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "et_utf" then
            f1_cat_value = "Tarkvara"
            f2_cat_value = doc_type_translation
        elseif language == "cs_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "ar_utf" then
            f1_cat_value = "البرامج"
            f2_cat_value = doc_type_translation
        elseif language == "vi_utf" then
            f1_cat_value = "Phần mềm"
            f2_cat_value = doc_type_translation
        elseif language == "th_utf" then
            f1_cat_value = "ซอฟต์แวร์"
            f2_cat_value = doc_type_translation
        elseif language == "ja_utf" then
            f1_cat_value = "ソフトウェア"
            f2_cat_value = doc_type_translation
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "软件"
            f2_cat_value = doc_type_translation
        elseif language == "pl_utf" then
            f1_cat_value = "Oprogramowanie"
            f2_cat_value = doc_type_translation
        elseif language == "da_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "he_utf" then
            f1_cat_value = "תוכנה"
            f2_cat_value = doc_type_translation
        elseif language == "ro_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "nl_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "tr_utf" then
            f1_cat_value = "Yazılımlar"
            f2_cat_value = doc_type_translation
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        end
    elseif content_source == "BSL_DOC" and doc_type == "Movies" then
        if language == "de_utf" then
            f1_cat_value = "Über uns"
            f2_cat_value = "Erfolgsgeschichten von Kunden"
        elseif language == "no_utf" then
            f1_cat_value = "Selskap"
            f2_cat_value = "Våre kunders suksesshistorier"
        elseif language == "fi_utf" then
            f1_cat_value = "Yritys"
            f2_cat_value = "Asiakkaiden menestystarinat"
        elseif language == "ru_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "Истории успеха наших клиентов"
        elseif language == "bg_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "Истории за успех на клиенти"
        elseif language == "lt_utf" then
            f1_cat_value = "Mūsų Įmonė"
            f2_cat_value = "Užsakovų sėkmės istorijos"
        elseif language == "hr_utf" then
            f1_cat_value = "Poduzeće"
            f2_cat_value = "Svjedočanstva klijenata"
        elseif language == "lv_utf" then
            f1_cat_value = "Uzņēmums"
            f2_cat_value = "Klientu veiksmes stāsti"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Cases de sucesso dos clientes"
        elseif language == "fr_utf" then
            f1_cat_value = "Société"
            f2_cat_value = "Témoignages clients"
        elseif language == "hu_utf" then
            f1_cat_value = "Céges információk"
            f2_cat_value = "Ügyfeleink sikertörténetei"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "公司"
            f2_cat_value = "客戶成功案例"
        elseif language == "uk_utf" then
            f1_cat_value = "Компанія"
            f2_cat_value = "Історії успіху наших користувачів"
        elseif language == "sk_utf" then
            f1_cat_value = "Spoločnosť"
            f2_cat_value = "Príbehy o úspechoch našich zákazníkov"
        elseif language == "sl_utf" then
            f1_cat_value = "Podjetje"
            f2_cat_value = "Zgodbe o uspehu strank"
        elseif language == "id_utf" then
            f1_cat_value = "Perusahaan"
            f2_cat_value = "Kisah Sukses Pelanggan"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Casos de éxito de clientes"
        elseif language == "sr_utf" then
            f1_cat_value = "Kompanija"
            f2_cat_value = "Priče o uspehu klijenata"
        elseif language == "sv_utf" then
            f1_cat_value = "Företaget"
            f2_cat_value = "Kundreferenser"
        elseif language == "ko_utf" then
            f1_cat_value = "회사"
            f2_cat_value = "고객 성공 사례"
        elseif language == "el_utf" then
            f1_cat_value = "Εταιρεία"
            f2_cat_value = "Ιστορίες επιτυχίας πελατών"
        elseif language == "en_utf" then
            f1_cat_value = "Company"
            f2_cat_value = "Customer Success Stories"
        elseif language == "it_utf" then
            f1_cat_value = "Società"
            f2_cat_value = "Storie di successo dei clienti"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Casos de éxito de clientes"
        elseif language == "et_utf" then
            f1_cat_value = "Ettevõte"
            f2_cat_value = "Klientide edulood"
        elseif language == "cs_utf" then
            f1_cat_value = "Společnost"
            f2_cat_value = "Úspěšné příběhy zákazníků"
        elseif language == "ar_utf" then
            f1_cat_value = "الشركة"
            f2_cat_value = "قصص نجاح العملاء"
        elseif language == "vi_utf" then
            f1_cat_value = "Công ty"
            f2_cat_value = "Câu chuyện thành công của khách hàng"
        elseif language == "th_utf" then
            f1_cat_value = "บริษัท"
            f2_cat_value = "เรื่องราวความสำเร็จของลูกค้า"
        elseif language == "ja_utf" then
            f1_cat_value = "会社名"
            f2_cat_value = "お客様の導入事例"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "公司"
            f2_cat_value = "客户成功案例"
        elseif language == "pl_utf" then
            f1_cat_value = "Firma"
            f2_cat_value = "Historia sukcesu naszych Klientów"
        elseif language == "da_utf" then
            f1_cat_value = "Virksomhed"
            f2_cat_value = "Kundehistorier"
        elseif language == "he_utf" then
            f1_cat_value = "חברה"
            f2_cat_value = "סיפורי הצלחה של לקוחות"
        elseif language == "ro_utf" then
            f1_cat_value = "Companie"
            f2_cat_value = "Povesti de succes ale clientilor"
        elseif language == "nl_utf" then
            f1_cat_value = "Bedrijf"
            f2_cat_value = "Succesverhalen van klanten"
        elseif language == "tr_utf" then
            f1_cat_value = "Şirket"
            f2_cat_value = "Müşteri Başarı Hikayeleri"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Casos de sucesso"
        end
    elseif content_source == "BSL_DOC" and doc_type == "Software - Demos & Trial" then
        if language == "de_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "no_utf" then
            f1_cat_value = "Programvare"
            f2_cat_value = doc_type_translation
        elseif language == "fi_utf" then
            f1_cat_value = "Ohjelmistot"
            f2_cat_value = doc_type_translation
        elseif language == "ru_utf" then
            f1_cat_value = "Программное обеспечение"
            f2_cat_value = doc_type_translation
        elseif language == "bg_utf" then
            f1_cat_value = "Софтуер"
            f2_cat_value = doc_type_translation
        elseif language == "lt_utf" then
            f1_cat_value = "Programinė įranga"
            f2_cat_value = doc_type_translation
        elseif language == "hr_utf" then
            f1_cat_value = "Softver"
            f2_cat_value = doc_type_translation
        elseif language == "lv_utf" then
            f1_cat_value = "Programmatūra"
            f2_cat_value = doc_type_translation
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "fr_utf" then
            f1_cat_value = "Logiciels"
            f2_cat_value = doc_type_translation
        elseif language == "hu_utf" then
            f1_cat_value = "Szoftver"
            f2_cat_value = doc_type_translation
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "軟體"
            f2_cat_value = doc_type_translation
        elseif language == "uk_utf" then
            f1_cat_value = "Програмне забезпечення"
            f2_cat_value = doc_type_translation
        elseif language == "sk_utf" then
            f1_cat_value = "Softvér"
            f2_cat_value = doc_type_translation
        elseif language == "sl_utf" then
            f1_cat_value = "Programska oprema"
            f2_cat_value = doc_type_translation
        elseif language == "id_utf" then
            f1_cat_value = "Perangkat Lunak"
            f2_cat_value = doc_type_translation
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "sr_utf" then
            f1_cat_value = "Softver"
            f2_cat_value = doc_type_translation
        elseif language == "sv_utf" then
            f1_cat_value = "Programvara"
            f2_cat_value = doc_type_translation
        elseif language == "ko_utf" then
            f1_cat_value = "소프트웨어"
            f2_cat_value = doc_type_translation
        elseif language == "el_utf" then
            f1_cat_value = "Λογισμικό"
            f2_cat_value = doc_type_translation
        elseif language == "en_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "it_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "et_utf" then
            f1_cat_value = "Tarkvara"
            f2_cat_value = doc_type_translation
        elseif language == "cs_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "ar_utf" then
            f1_cat_value = "البرامج"
            f2_cat_value = doc_type_translation
        elseif language == "vi_utf" then
            f1_cat_value = "Phần mềm"
            f2_cat_value = doc_type_translation
        elseif language == "th_utf" then
            f1_cat_value = "ซอฟต์แวร์"
            f2_cat_value = doc_type_translation
        elseif language == "ja_utf" then
            f1_cat_value = "ソフトウェア"
            f2_cat_value = doc_type_translation
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "软件"
            f2_cat_value = doc_type_translation
        elseif language == "pl_utf" then
            f1_cat_value = "Oprogramowanie"
            f2_cat_value = doc_type_translation
        elseif language == "da_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "he_utf" then
            f1_cat_value = "תוכנה"
            f2_cat_value = doc_type_translation
        elseif language == "ro_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "nl_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "tr_utf" then
            f1_cat_value = "Yazılımlar"
            f2_cat_value = doc_type_translation
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        end
    elseif content_source == "BSL_DOC" and doc_type == "Software - Release Notes" then
        if language == "de_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "no_utf" then
            f1_cat_value = "Programvare"
            f2_cat_value = doc_type_translation
        elseif language == "fi_utf" then
            f1_cat_value = "Ohjelmistot"
            f2_cat_value = doc_type_translation
        elseif language == "ru_utf" then
            f1_cat_value = "Программное обеспечение"
            f2_cat_value = doc_type_translation
        elseif language == "bg_utf" then
            f1_cat_value = "Софтуер"
            f2_cat_value = doc_type_translation
        elseif language == "lt_utf" then
            f1_cat_value = "Programinė įranga"
            f2_cat_value = doc_type_translation
        elseif language == "hr_utf" then
            f1_cat_value = "Softver"
            f2_cat_value = doc_type_translation
        elseif language == "lv_utf" then
            f1_cat_value = "Programmatūra"
            f2_cat_value = doc_type_translation
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "fr_utf" then
            f1_cat_value = "Logiciels"
            f2_cat_value = doc_type_translation
        elseif language == "hu_utf" then
            f1_cat_value = "Szoftver"
            f2_cat_value = doc_type_translation
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "軟體"
            f2_cat_value = doc_type_translation
        elseif language == "uk_utf" then
            f1_cat_value = "Програмне забезпечення"
            f2_cat_value = doc_type_translation
        elseif language == "sk_utf" then
            f1_cat_value = "Softvér"
            f2_cat_value = doc_type_translation
        elseif language == "sl_utf" then
            f1_cat_value = "Programska oprema"
            f2_cat_value = doc_type_translation
        elseif language == "id_utf" then
            f1_cat_value = "Perangkat Lunak"
            f2_cat_value = doc_type_translation
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "sr_utf" then
            f1_cat_value = "Softver"
            f2_cat_value = doc_type_translation
        elseif language == "sv_utf" then
            f1_cat_value = "Programvara"
            f2_cat_value = doc_type_translation
        elseif language == "ko_utf" then
            f1_cat_value = "소프트웨어"
            f2_cat_value = doc_type_translation
        elseif language == "el_utf" then
            f1_cat_value = "Λογισμικό"
            f2_cat_value = doc_type_translation
        elseif language == "en_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "it_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "et_utf" then
            f1_cat_value = "Tarkvara"
            f2_cat_value = doc_type_translation
        elseif language == "cs_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "ar_utf" then
            f1_cat_value = "البرامج"
            f2_cat_value = doc_type_translation
        elseif language == "vi_utf" then
            f1_cat_value = "Phần mềm"
            f2_cat_value = doc_type_translation
        elseif language == "th_utf" then
            f1_cat_value = "ซอฟต์แวร์"
            f2_cat_value = doc_type_translation
        elseif language == "ja_utf" then
            f1_cat_value = "ソフトウェア"
            f2_cat_value = doc_type_translation
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "软件"
            f2_cat_value = doc_type_translation
        elseif language == "pl_utf" then
            f1_cat_value = "Oprogramowanie"
            f2_cat_value = doc_type_translation
        elseif language == "da_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "he_utf" then
            f1_cat_value = "תוכנה"
            f2_cat_value = doc_type_translation
        elseif language == "ro_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "nl_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "tr_utf" then
            f1_cat_value = "Yazılımlar"
            f2_cat_value = doc_type_translation
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        end
    elseif content_source == "BSL_DOC" and doc_type == "DTM files" then
        if language == "de_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "no_utf" then
            f1_cat_value = "Programvare"
            f2_cat_value = doc_type_translation
        elseif language == "fi_utf" then
            f1_cat_value = "Ohjelmistot"
            f2_cat_value = doc_type_translation
        elseif language == "ru_utf" then
            f1_cat_value = "Программное обеспечение"
            f2_cat_value = doc_type_translation
        elseif language == "bg_utf" then
            f1_cat_value = "Софтуер"
            f2_cat_value = doc_type_translation
        elseif language == "lt_utf" then
            f1_cat_value = "Programinė įranga"
            f2_cat_value = doc_type_translation
        elseif language == "hr_utf" then
            f1_cat_value = "Softver"
            f2_cat_value = doc_type_translation
        elseif language == "lv_utf" then
            f1_cat_value = "Programmatūra"
            f2_cat_value = doc_type_translation
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "fr_utf" then
            f1_cat_value = "Logiciels"
            f2_cat_value = doc_type_translation
        elseif language == "hu_utf" then
            f1_cat_value = "Szoftver"
            f2_cat_value = doc_type_translation
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "軟體"
            f2_cat_value = doc_type_translation
        elseif language == "uk_utf" then
            f1_cat_value = "Програмне забезпечення"
            f2_cat_value = doc_type_translation
        elseif language == "sk_utf" then
            f1_cat_value = "Softvér"
            f2_cat_value = doc_type_translation
        elseif language == "sl_utf" then
            f1_cat_value = "Programska oprema"
            f2_cat_value = doc_type_translation
        elseif language == "id_utf" then
            f1_cat_value = "Perangkat Lunak"
            f2_cat_value = doc_type_translation
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "sr_utf" then
            f1_cat_value = "Softver"
            f2_cat_value = doc_type_translation
        elseif language == "sv_utf" then
            f1_cat_value = "Programvara"
            f2_cat_value = doc_type_translation
        elseif language == "ko_utf" then
            f1_cat_value = "소프트웨어"
            f2_cat_value = doc_type_translation
        elseif language == "el_utf" then
            f1_cat_value = "Λογισμικό"
            f2_cat_value = doc_type_translation
        elseif language == "en_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "it_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "et_utf" then
            f1_cat_value = "Tarkvara"
            f2_cat_value = doc_type_translation
        elseif language == "cs_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "ar_utf" then
            f1_cat_value = "البرامج"
            f2_cat_value = doc_type_translation
        elseif language == "vi_utf" then
            f1_cat_value = "Phần mềm"
            f2_cat_value = doc_type_translation
        elseif language == "th_utf" then
            f1_cat_value = "ซอฟต์แวร์"
            f2_cat_value = doc_type_translation
        elseif language == "ja_utf" then
            f1_cat_value = "ソフトウェア"
            f2_cat_value = doc_type_translation
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "软件"
            f2_cat_value = doc_type_translation
        elseif language == "pl_utf" then
            f1_cat_value = "Oprogramowanie"
            f2_cat_value = doc_type_translation
        elseif language == "da_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "he_utf" then
            f1_cat_value = "תוכנה"
            f2_cat_value = doc_type_translation
        elseif language == "ro_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "nl_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "tr_utf" then
            f1_cat_value = "Yazılımlar"
            f2_cat_value = doc_type_translation
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        end
    elseif content_source == "BSL_DOC" and doc_type == "Firmware - Updates" then
        if language == "de_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "no_utf" then
            f1_cat_value = "Programvare"
            f2_cat_value = doc_type_translation
        elseif language == "fi_utf" then
            f1_cat_value = "Ohjelmistot"
            f2_cat_value = doc_type_translation
        elseif language == "ru_utf" then
            f1_cat_value = "Программное обеспечение"
            f2_cat_value = doc_type_translation
        elseif language == "bg_utf" then
            f1_cat_value = "Софтуер"
            f2_cat_value = doc_type_translation
        elseif language == "lt_utf" then
            f1_cat_value = "Programinė įranga"
            f2_cat_value = doc_type_translation
        elseif language == "hr_utf" then
            f1_cat_value = "Softver"
            f2_cat_value = doc_type_translation
        elseif language == "lv_utf" then
            f1_cat_value = "Programmatūra"
            f2_cat_value = doc_type_translation
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "fr_utf" then
            f1_cat_value = "Logiciels"
            f2_cat_value = doc_type_translation
        elseif language == "hu_utf" then
            f1_cat_value = "Szoftver"
            f2_cat_value = doc_type_translation
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "軟體"
            f2_cat_value = doc_type_translation
        elseif language == "uk_utf" then
            f1_cat_value = "Програмне забезпечення"
            f2_cat_value = doc_type_translation
        elseif language == "sk_utf" then
            f1_cat_value = "Softvér"
            f2_cat_value = doc_type_translation
        elseif language == "sl_utf" then
            f1_cat_value = "Programska oprema"
            f2_cat_value = doc_type_translation
        elseif language == "id_utf" then
            f1_cat_value = "Perangkat Lunak"
            f2_cat_value = doc_type_translation
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "sr_utf" then
            f1_cat_value = "Softver"
            f2_cat_value = doc_type_translation
        elseif language == "sv_utf" then
            f1_cat_value = "Programvara"
            f2_cat_value = doc_type_translation
        elseif language == "ko_utf" then
            f1_cat_value = "소프트웨어"
            f2_cat_value = doc_type_translation
        elseif language == "el_utf" then
            f1_cat_value = "Λογισμικό"
            f2_cat_value = doc_type_translation
        elseif language == "en_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "it_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "et_utf" then
            f1_cat_value = "Tarkvara"
            f2_cat_value = doc_type_translation
        elseif language == "cs_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "ar_utf" then
            f1_cat_value = "البرامج"
            f2_cat_value = doc_type_translation
        elseif language == "vi_utf" then
            f1_cat_value = "Phần mềm"
            f2_cat_value = doc_type_translation
        elseif language == "th_utf" then
            f1_cat_value = "ซอฟต์แวร์"
            f2_cat_value = doc_type_translation
        elseif language == "ja_utf" then
            f1_cat_value = "ソフトウェア"
            f2_cat_value = doc_type_translation
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "软件"
            f2_cat_value = doc_type_translation
        elseif language == "pl_utf" then
            f1_cat_value = "Oprogramowanie"
            f2_cat_value = doc_type_translation
        elseif language == "da_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "he_utf" then
            f1_cat_value = "תוכנה"
            f2_cat_value = doc_type_translation
        elseif language == "ro_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "nl_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "tr_utf" then
            f1_cat_value = "Yazılımlar"
            f2_cat_value = doc_type_translation
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        end
    elseif content_source == "BSL_DOC" and doc_type == "Programming Example" then
        if language == "de_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "no_utf" then
            f1_cat_value = "Programvare"
            f2_cat_value = doc_type_translation
        elseif language == "fi_utf" then
            f1_cat_value = "Ohjelmistot"
            f2_cat_value = doc_type_translation
        elseif language == "ru_utf" then
            f1_cat_value = "Программное обеспечение"
            f2_cat_value = doc_type_translation
        elseif language == "bg_utf" then
            f1_cat_value = "Софтуер"
            f2_cat_value = doc_type_translation
        elseif language == "lt_utf" then
            f1_cat_value = "Programinė įranga"
            f2_cat_value = doc_type_translation
        elseif language == "hr_utf" then
            f1_cat_value = "Softver"
            f2_cat_value = doc_type_translation
        elseif language == "lv_utf" then
            f1_cat_value = "Programmatūra"
            f2_cat_value = doc_type_translation
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "fr_utf" then
            f1_cat_value = "Logiciels"
            f2_cat_value = doc_type_translation
        elseif language == "hu_utf" then
            f1_cat_value = "Szoftver"
            f2_cat_value = doc_type_translation
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "軟體"
            f2_cat_value = doc_type_translation
        elseif language == "uk_utf" then
            f1_cat_value = "Програмне забезпечення"
            f2_cat_value = doc_type_translation
        elseif language == "sk_utf" then
            f1_cat_value = "Softvér"
            f2_cat_value = doc_type_translation
        elseif language == "sl_utf" then
            f1_cat_value = "Programska oprema"
            f2_cat_value = doc_type_translation
        elseif language == "id_utf" then
            f1_cat_value = "Perangkat Lunak"
            f2_cat_value = doc_type_translation
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "sr_utf" then
            f1_cat_value = "Softver"
            f2_cat_value = doc_type_translation
        elseif language == "sv_utf" then
            f1_cat_value = "Programvara"
            f2_cat_value = doc_type_translation
        elseif language == "ko_utf" then
            f1_cat_value = "소프트웨어"
            f2_cat_value = doc_type_translation
        elseif language == "el_utf" then
            f1_cat_value = "Λογισμικό"
            f2_cat_value = doc_type_translation
        elseif language == "en_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "it_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "et_utf" then
            f1_cat_value = "Tarkvara"
            f2_cat_value = doc_type_translation
        elseif language == "cs_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "ar_utf" then
            f1_cat_value = "البرامج"
            f2_cat_value = doc_type_translation
        elseif language == "vi_utf" then
            f1_cat_value = "Phần mềm"
            f2_cat_value = doc_type_translation
        elseif language == "th_utf" then
            f1_cat_value = "ซอฟต์แวร์"
            f2_cat_value = doc_type_translation
        elseif language == "ja_utf" then
            f1_cat_value = "ソフトウェア"
            f2_cat_value = doc_type_translation
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "软件"
            f2_cat_value = doc_type_translation
        elseif language == "pl_utf" then
            f1_cat_value = "Oprogramowanie"
            f2_cat_value = doc_type_translation
        elseif language == "da_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "he_utf" then
            f1_cat_value = "תוכנה"
            f2_cat_value = doc_type_translation
        elseif language == "ro_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "nl_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "tr_utf" then
            f1_cat_value = "Yazılımlar"
            f2_cat_value = doc_type_translation
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        end
    elseif content_source == "BSL_DOC" and doc_type == "Software - ETS" then
        if language == "de_utf" then
            f1_cat_value = "Software"
            f2_cat_value = "Software - ETS"
        elseif language == "no_utf" then
            f1_cat_value = "Programvare"
            f2_cat_value = "Software - ETS"
        elseif language == "fi_utf" then
            f1_cat_value = "Ohjelmistot"
            f2_cat_value = "Ohjelmistot - ETS"
        elseif language == "ru_utf" then
            f1_cat_value = "Программное обеспечение"
            f2_cat_value = "Программное обеспечение - ETS"
        elseif language == "bg_utf" then
            f1_cat_value = "Софтуер"
            f2_cat_value = "Софтуер - ETS"
        elseif language == "lt_utf" then
            f1_cat_value = "Programinė įranga"
            f2_cat_value = "Prog. Įrangos - ETS"
        elseif language == "hr_utf" then
            f1_cat_value = "Softver"
            f2_cat_value = "Softver - ETS"
        elseif language == "lv_utf" then
            f1_cat_value = "Programmatūra"
            f2_cat_value = "Programmatūra - ETS"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Software"
            f2_cat_value = "Software - ETS"
        elseif language == "fr_utf" then
            f1_cat_value = "Logiciels"
            f2_cat_value = "Logiciel - ETS"
        elseif language == "hu_utf" then
            f1_cat_value = "Szoftver"
            f2_cat_value = "Szoftver - ETS"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "軟體"
            f2_cat_value = "軟體 - ETS"
        elseif language == "uk_utf" then
            f1_cat_value = "Програмне забезпечення"
            f2_cat_value = "ПЗ - ETS"
        elseif language == "sk_utf" then
            f1_cat_value = "Softvér"
            f2_cat_value = "Software - ETS"
        elseif language == "sl_utf" then
            f1_cat_value = "Programska oprema"
            f2_cat_value = "Programska oprema - ETS"
        elseif language == "id_utf" then
            f1_cat_value = "Perangkat Lunak"
            f2_cat_value = "perangkat lunak - ETS"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Software"
            f2_cat_value = "Software - ETS"
        elseif language == "sr_utf" then
            f1_cat_value = "Softver"
            f2_cat_value = "Softver - ETS"
        elseif language == "sv_utf" then
            f1_cat_value = "Programvara"
            f2_cat_value = "Mjukvara - ETS"
        elseif language == "ko_utf" then
            f1_cat_value = "소프트웨어"
            f2_cat_value = "소프트웨어-ETS"
        elseif language == "el_utf" then
            f1_cat_value = "Λογισμικό"
            f2_cat_value = "Λογισμικό - ETS"
        elseif language == "en_utf" then
            f1_cat_value = "Software"
            f2_cat_value = "Software - ETS"
        elseif language == "it_utf" then
            f1_cat_value = "Software"
            f2_cat_value = "Software - ETS"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Software"
            f2_cat_value = "Software - ETS"
        elseif language == "et_utf" then
            f1_cat_value = "Tarkvara"
            f2_cat_value = "Tarkvara - ETS"
        elseif language == "cs_utf" then
            f1_cat_value = "Software"
            f2_cat_value = "Software - ETS"
        elseif language == "ar_utf" then
            f1_cat_value = "البرامج"
            f2_cat_value = "البرمجيات - ETS"
        elseif language == "vi_utf" then
            f1_cat_value = "Phần mềm"
            f2_cat_value = "Phần mềm-ETS"
        elseif language == "th_utf" then
            f1_cat_value = "ซอฟต์แวร์"
            f2_cat_value = "ซอฟต์แวร์ - ETS"
        elseif language == "ja_utf" then
            f1_cat_value = "ソフトウェア"
            f2_cat_value = "ソフトウェア - ETS"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "软件"
            f2_cat_value = "软件-ETS"
        elseif language == "pl_utf" then
            f1_cat_value = "Oprogramowanie"
            f2_cat_value = "Oprogramowanie - ETS"
        elseif language == "da_utf" then
            f1_cat_value = "Software"
            f2_cat_value = "Software - ETS"
        elseif language == "he_utf" then
            f1_cat_value = "תוכנה"
            f2_cat_value = "תוכנה - ETS"
        elseif language == "ro_utf" then
            f1_cat_value = "Software"
            f2_cat_value = "Software - ETS"
        elseif language == "nl_utf" then
            f1_cat_value = "Software"
            f2_cat_value = "Software - ETS"
        elseif language == "tr_utf" then
            f1_cat_value = "Yazılımlar"
            f2_cat_value = "Yazılım - ETS"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Software"
            f2_cat_value = "Software - ETS"
        end
    elseif content_source == "BSL_DOC" and doc_type == "Software - Release" then
        if language == "de_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "no_utf" then
            f1_cat_value = "Programvare"
            f2_cat_value = doc_type_translation
        elseif language == "fi_utf" then
            f1_cat_value = "Ohjelmistot"
            f2_cat_value = doc_type_translation
        elseif language == "ru_utf" then
            f1_cat_value = "Программное обеспечение"
            f2_cat_value = doc_type_translation
        elseif language == "bg_utf" then
            f1_cat_value = "Софтуер"
            f2_cat_value = doc_type_translation
        elseif language == "lt_utf" then
            f1_cat_value = "Programinė įranga"
            f2_cat_value = doc_type_translation
        elseif language == "hr_utf" then
            f1_cat_value = "Softver"
            f2_cat_value = doc_type_translation
        elseif language == "lv_utf" then
            f1_cat_value = "Programmatūra"
            f2_cat_value = doc_type_translation
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "fr_utf" then
            f1_cat_value = "Logiciels"
            f2_cat_value = doc_type_translation
        elseif language == "hu_utf" then
            f1_cat_value = "Szoftver"
            f2_cat_value = doc_type_translation
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "軟體"
            f2_cat_value = doc_type_translation
        elseif language == "uk_utf" then
            f1_cat_value = "Програмне забезпечення"
            f2_cat_value = doc_type_translation
        elseif language == "sk_utf" then
            f1_cat_value = "Softvér"
            f2_cat_value = doc_type_translation
        elseif language == "sl_utf" then
            f1_cat_value = "Programska oprema"
            f2_cat_value = doc_type_translation
        elseif language == "id_utf" then
            f1_cat_value = "Perangkat Lunak"
            f2_cat_value = doc_type_translation
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "sr_utf" then
            f1_cat_value = "Softver"
            f2_cat_value = doc_type_translation
        elseif language == "sv_utf" then
            f1_cat_value = "Programvara"
            f2_cat_value = doc_type_translation
        elseif language == "ko_utf" then
            f1_cat_value = "소프트웨어"
            f2_cat_value = doc_type_translation
        elseif language == "el_utf" then
            f1_cat_value = "Λογισμικό"
            f2_cat_value = doc_type_translation
        elseif language == "en_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "it_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "et_utf" then
            f1_cat_value = "Tarkvara"
            f2_cat_value = doc_type_translation
        elseif language == "cs_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "ar_utf" then
            f1_cat_value = "البرامج"
            f2_cat_value = doc_type_translation
        elseif language == "vi_utf" then
            f1_cat_value = "Phần mềm"
            f2_cat_value = doc_type_translation
        elseif language == "th_utf" then
            f1_cat_value = "ซอฟต์แวร์"
            f2_cat_value = doc_type_translation
        elseif language == "ja_utf" then
            f1_cat_value = "ソフトウェア"
            f2_cat_value = doc_type_translation
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "软件"
            f2_cat_value = doc_type_translation
        elseif language == "pl_utf" then
            f1_cat_value = "Oprogramowanie"
            f2_cat_value = doc_type_translation
        elseif language == "da_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "he_utf" then
            f1_cat_value = "תוכנה"
            f2_cat_value = doc_type_translation
        elseif language == "ro_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "nl_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "tr_utf" then
            f1_cat_value = "Yazılımlar"
            f2_cat_value = doc_type_translation
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        end
    elseif content_source == "BSL_DOC" and doc_type == "Software - Betas" then
        if language == "de_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "no_utf" then
            f1_cat_value = "Programvare"
            f2_cat_value = doc_type_translation
        elseif language == "fi_utf" then
            f1_cat_value = "Ohjelmistot"
            f2_cat_value = doc_type_translation
        elseif language == "ru_utf" then
            f1_cat_value = "Программное обеспечение"
            f2_cat_value = doc_type_translation
        elseif language == "bg_utf" then
            f1_cat_value = "Софтуер"
            f2_cat_value = doc_type_translation
        elseif language == "lt_utf" then
            f1_cat_value = "Programinė įranga"
            f2_cat_value = doc_type_translation
        elseif language == "hr_utf" then
            f1_cat_value = "Softver"
            f2_cat_value = doc_type_translation
        elseif language == "lv_utf" then
            f1_cat_value = "Programmatūra"
            f2_cat_value = doc_type_translation
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "fr_utf" then
            f1_cat_value = "Logiciels"
            f2_cat_value = doc_type_translation
        elseif language == "hu_utf" then
            f1_cat_value = "Szoftver"
            f2_cat_value = doc_type_translation
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "軟體"
            f2_cat_value = doc_type_translation
        elseif language == "uk_utf" then
            f1_cat_value = "Програмне забезпечення"
            f2_cat_value = doc_type_translation
        elseif language == "sk_utf" then
            f1_cat_value = "Softvér"
            f2_cat_value = doc_type_translation
        elseif language == "sl_utf" then
            f1_cat_value = "Programska oprema"
            f2_cat_value = doc_type_translation
        elseif language == "id_utf" then
            f1_cat_value = "Perangkat Lunak"
            f2_cat_value = doc_type_translation
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "sr_utf" then
            f1_cat_value = "Softver"
            f2_cat_value = doc_type_translation
        elseif language == "sv_utf" then
            f1_cat_value = "Programvara"
            f2_cat_value = doc_type_translation
        elseif language == "ko_utf" then
            f1_cat_value = "소프트웨어"
            f2_cat_value = doc_type_translation
        elseif language == "el_utf" then
            f1_cat_value = "Λογισμικό"
            f2_cat_value = doc_type_translation
        elseif language == "en_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "it_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "et_utf" then
            f1_cat_value = "Tarkvara"
            f2_cat_value = doc_type_translation
        elseif language == "cs_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "ar_utf" then
            f1_cat_value = "البرامج"
            f2_cat_value = doc_type_translation
        elseif language == "vi_utf" then
            f1_cat_value = "Phần mềm"
            f2_cat_value = doc_type_translation
        elseif language == "th_utf" then
            f1_cat_value = "ซอฟต์แวร์"
            f2_cat_value = doc_type_translation
        elseif language == "ja_utf" then
            f1_cat_value = "ソフトウェア"
            f2_cat_value = doc_type_translation
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "软件"
            f2_cat_value = doc_type_translation
        elseif language == "pl_utf" then
            f1_cat_value = "Oprogramowanie"
            f2_cat_value = doc_type_translation
        elseif language == "da_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "he_utf" then
            f1_cat_value = "תוכנה"
            f2_cat_value = doc_type_translation
        elseif language == "ro_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "nl_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "tr_utf" then
            f1_cat_value = "Yazılımlar"
            f2_cat_value = doc_type_translation
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        end
    elseif content_source == "BSL_DOC" and doc_type == "Customer NewsLetter" then
        if language == "de_utf" then
            f1_cat_value = "Über uns"
            f2_cat_value = "Pressemitteilungen"
        elseif language == "no_utf" then
            f1_cat_value = "Selskap"
            f2_cat_value = "Trykk"
        elseif language == "fi_utf" then
            f1_cat_value = "Yritys"
            f2_cat_value = "Lehdistötiedote"
        elseif language == "ru_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "Пресса"
        elseif language == "bg_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "Преса"
        elseif language == "lt_utf" then
            f1_cat_value = "Mūsų Įmonė"
            f2_cat_value = "Spauda"
        elseif language == "hr_utf" then
            f1_cat_value = "Poduzeće"
            f2_cat_value = "Press"
        elseif language == "lv_utf" then
            f1_cat_value = "Uzņēmums"
            f2_cat_value = "Prese"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Comunicado de imprensa"
        elseif language == "fr_utf" then
            f1_cat_value = "Société"
            f2_cat_value = "Presse"
        elseif language == "hu_utf" then
            f1_cat_value = "Céges információk"
            f2_cat_value = "Présgép"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "公司"
            f2_cat_value = "媒體"
        elseif language == "uk_utf" then
            f1_cat_value = "Компанія"
            f2_cat_value = "ЗМІ"
        elseif language == "sk_utf" then
            f1_cat_value = "Spoločnosť"
            f2_cat_value = "Lis"
        elseif language == "sl_utf" then
            f1_cat_value = "Podjetje"
            f2_cat_value = "Medijski kotiček"
        elseif language == "id_utf" then
            f1_cat_value = "Perusahaan"
            f2_cat_value = "Tekan"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Prensa"
        elseif language == "sr_utf" then
            f1_cat_value = "Kompanija"
            f2_cat_value = "Novosti"
        elseif language == "sv_utf" then
            f1_cat_value = "Företaget"
            f2_cat_value = "Press"
        elseif language == "ko_utf" then
            f1_cat_value = "회사"
            f2_cat_value = "보도 자료"
        elseif language == "el_utf" then
            f1_cat_value = "Εταιρεία"
            f2_cat_value = "Νέα"
        elseif language == "en_utf" then
            f1_cat_value = "Company"
            f2_cat_value = "Press"
        elseif language == "it_utf" then
            f1_cat_value = "Società"
            f2_cat_value = "Stampa"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Prensa"
        elseif language == "et_utf" then
            f1_cat_value = "Ettevõte"
            f2_cat_value = "Vajutage"
        elseif language == "cs_utf" then
            f1_cat_value = "Společnost"
            f2_cat_value = "Lis"
        elseif language == "ar_utf" then
            f1_cat_value = "الشركة"
            f2_cat_value = "الصحافة"
        elseif language == "vi_utf" then
            f1_cat_value = "Công ty"
            f2_cat_value = "Báo chí"
        elseif language == "th_utf" then
            f1_cat_value = "บริษัท"
            f2_cat_value = "ข่าวประชาสัมพันธ์"
        elseif language == "ja_utf" then
            f1_cat_value = "会社名"
            f2_cat_value = "プレスリリース"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "公司"
            f2_cat_value = "新闻"
        elseif language == "pl_utf" then
            f1_cat_value = "Firma"
            f2_cat_value = "Prasa"
        elseif language == "da_utf" then
            f1_cat_value = "Virksomhed"
            f2_cat_value = "Presse"
        elseif language == "he_utf" then
            f1_cat_value = "חברה"
            f2_cat_value = "עיתונות"
        elseif language == "ro_utf" then
            f1_cat_value = "Companie"
            f2_cat_value = "Presa"
        elseif language == "nl_utf" then
            f1_cat_value = "Bedrijf"
            f2_cat_value = "Pers"
        elseif language == "tr_utf" then
            f1_cat_value = "Şirket"
            f2_cat_value = "Basın"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Comunicado de imprensa"
        end
    elseif content_source == "BSL_DOC" and doc_type == "Customer success story" then
        if language == "de_utf" then
            f1_cat_value = "Über uns"
            f2_cat_value = "Erfolgsgeschichten von Kunden"
        elseif language == "no_utf" then
            f1_cat_value = "Selskap"
            f2_cat_value = "Våre kunders suksesshistorier"
        elseif language == "fi_utf" then
            f1_cat_value = "Yritys"
            f2_cat_value = "Asiakkaiden menestystarinat"
        elseif language == "ru_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "Истории успеха наших клиентов"
        elseif language == "bg_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "Истории за успех на клиенти"
        elseif language == "lt_utf" then
            f1_cat_value = "Mūsų Įmonė"
            f2_cat_value = "Užsakovų sėkmės istorijos"
        elseif language == "hr_utf" then
            f1_cat_value = "Poduzeće"
            f2_cat_value = "Svjedočanstva klijenata"
        elseif language == "lv_utf" then
            f1_cat_value = "Uzņēmums"
            f2_cat_value = "Klientu veiksmes stāsti"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Cases de sucesso dos clientes"
        elseif language == "fr_utf" then
            f1_cat_value = "Société"
            f2_cat_value = "Témoignages clients"
        elseif language == "hu_utf" then
            f1_cat_value = "Céges információk"
            f2_cat_value = "Ügyfeleink sikertörténetei"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "公司"
            f2_cat_value = "客戶成功案例"
        elseif language == "uk_utf" then
            f1_cat_value = "Компанія"
            f2_cat_value = "Історії успіху наших користувачів"
        elseif language == "sk_utf" then
            f1_cat_value = "Spoločnosť"
            f2_cat_value = "Príbehy o úspechoch našich zákazníkov"
        elseif language == "sl_utf" then
            f1_cat_value = "Podjetje"
            f2_cat_value = "Zgodbe o uspehu strank"
        elseif language == "id_utf" then
            f1_cat_value = "Perusahaan"
            f2_cat_value = "Kisah Sukses Pelanggan"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Casos de éxito de clientes"
        elseif language == "sr_utf" then
            f1_cat_value = "Kompanija"
            f2_cat_value = "Priče o uspehu klijenata"
        elseif language == "sv_utf" then
            f1_cat_value = "Företaget"
            f2_cat_value = "Kundreferenser"
        elseif language == "ko_utf" then
            f1_cat_value = "회사"
            f2_cat_value = "고객 성공 사례"
        elseif language == "el_utf" then
            f1_cat_value = "Εταιρεία"
            f2_cat_value = "Ιστορίες επιτυχίας πελατών"
        elseif language == "en_utf" then
            f1_cat_value = "Company"
            f2_cat_value = "Customer Success Stories"
        elseif language == "it_utf" then
            f1_cat_value = "Società"
            f2_cat_value = "Storie di successo dei clienti"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Casos de éxito de clientes"
        elseif language == "et_utf" then
            f1_cat_value = "Ettevõte"
            f2_cat_value = "Klientide edulood"
        elseif language == "cs_utf" then
            f1_cat_value = "Společnost"
            f2_cat_value = "Úspěšné příběhy zákazníků"
        elseif language == "ar_utf" then
            f1_cat_value = "الشركة"
            f2_cat_value = "قصص نجاح العملاء"
        elseif language == "vi_utf" then
            f1_cat_value = "Công ty"
            f2_cat_value = "Câu chuyện thành công của khách hàng"
        elseif language == "th_utf" then
            f1_cat_value = "บริษัท"
            f2_cat_value = "เรื่องราวความสำเร็จของลูกค้า"
        elseif language == "ja_utf" then
            f1_cat_value = "会社名"
            f2_cat_value = "お客様の導入事例"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "公司"
            f2_cat_value = "客户成功案例"
        elseif language == "pl_utf" then
            f1_cat_value = "Firma"
            f2_cat_value = "Historia sukcesu naszych Klientów"
        elseif language == "da_utf" then
            f1_cat_value = "Virksomhed"
            f2_cat_value = "Kundehistorier"
        elseif language == "he_utf" then
            f1_cat_value = "חברה"
            f2_cat_value = "סיפורי הצלחה של לקוחות"
        elseif language == "ro_utf" then
            f1_cat_value = "Companie"
            f2_cat_value = "Povesti de succes ale clientilor"
        elseif language == "nl_utf" then
            f1_cat_value = "Bedrijf"
            f2_cat_value = "Succesverhalen van klanten"
        elseif language == "tr_utf" then
            f1_cat_value = "Şirket"
            f2_cat_value = "Müşteri Başarı Hikayeleri"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Casos de sucesso"
        end
    elseif content_source == "BSL_DOC" and doc_type == "Application solutions" then
        if language == "de_utf" then
            f1_cat_value = "Lösungen"
            f2_cat_value = "Für Ihr Business"
        elseif language == "no_utf" then
            f1_cat_value = "Løsninger"
            f2_cat_value = "For bedrift"
        elseif language == "fi_utf" then
            f1_cat_value = "Ratkaisut"
            f2_cat_value = "Yritykselle"
        elseif language == "ru_utf" then
            f1_cat_value = "Решения"
            f2_cat_value = "Для бизнеса"
        elseif language == "bg_utf" then
            f1_cat_value = "Решения"
            f2_cat_value = "За бизнеса"
        elseif language == "lt_utf" then
            f1_cat_value = "Sprendimai"
            f2_cat_value = "Verslui"
        elseif language == "hr_utf" then
            f1_cat_value = "Rješenja"
            f2_cat_value = "Rješenja za poslovanje"
        elseif language == "lv_utf" then
            f1_cat_value = "Risinājumi"
            f2_cat_value = "Biznesam"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Soluções"
            f2_cat_value = "Para o negócio"
        elseif language == "fr_utf" then
            f1_cat_value = "Solutions"
            f2_cat_value = "Pour les professionnels"
        elseif language == "hu_utf" then
            f1_cat_value = "Megoldások"
            f2_cat_value = "Üzleti megoldások"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "解决方案"
            f2_cat_value = "營業用"
        elseif language == "uk_utf" then
            f1_cat_value = "Рішення"
            f2_cat_value = "Для бізнесу"
        elseif language == "sk_utf" then
            f1_cat_value = "Riešenia"
            f2_cat_value = "Pre Podnikateľov"
        elseif language == "sl_utf" then
            f1_cat_value = "Rešitve"
            f2_cat_value = "Rešitve za podjetja"
        elseif language == "id_utf" then
            f1_cat_value = "Solusi"
            f2_cat_value = "Untuk bisnis"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Soluciones"
            f2_cat_value = "Para empresas"
        elseif language == "sr_utf" then
            f1_cat_value = "Rešenja"
            f2_cat_value = "Za Vaš biznis"
        elseif language == "sv_utf" then
            f1_cat_value = "Lösningar"
            f2_cat_value = "För företag"
        elseif language == "ko_utf" then
            f1_cat_value = "솔루션"
            f2_cat_value = "파트너 및 기업용솔루션"
        elseif language == "el_utf" then
            f1_cat_value = "Λύσεις"
            f2_cat_value = "Για την επιχείρηση"
        elseif language == "en_utf" then
            f1_cat_value = "Solutions"
            f2_cat_value = "For Business"
        elseif language == "it_utf" then
            f1_cat_value = "Soluzioni"
            f2_cat_value = "Soluzioni per il business"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Soluciones"
            f2_cat_value = "Para empresas"
        elseif language == "et_utf" then
            f1_cat_value = "Lahendused"
            f2_cat_value = "Ettevõtetele"
        elseif language == "cs_utf" then
            f1_cat_value = "Řešení"
            f2_cat_value = "Pro vaše podnikání"
        elseif language == "ar_utf" then
            f1_cat_value = "الحلول"
            f2_cat_value = "للعمل"
        elseif language == "vi_utf" then
            f1_cat_value = "Giải pháp"
            f2_cat_value = "Dành cho Doanh nghiệp"
        elseif language == "th_utf" then
            f1_cat_value = "โซลูชั่น"
            f2_cat_value = "สำหรับธุรกิจ"
        elseif language == "ja_utf" then
            f1_cat_value = "ソリューション"
            f2_cat_value = "事業用"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "解决方案"
            f2_cat_value = "商业用途"
        elseif language == "pl_utf" then
            f1_cat_value = "Rozwiązania"
            f2_cat_value = "Dla biznesu"
        elseif language == "da_utf" then
            f1_cat_value = "Løsninger"
            f2_cat_value = "Til brancher"
        elseif language == "he_utf" then
            f1_cat_value = "פתרונות"
            f2_cat_value = "לעסקים"
        elseif language == "ro_utf" then
            f1_cat_value = "Solutii"
            f2_cat_value = "Pentru afaceri"
        elseif language == "nl_utf" then
            f1_cat_value = "Oplossingen"
            f2_cat_value = "Voor bedrijven"
        elseif language == "tr_utf" then
            f1_cat_value = "Çözümler"
            f2_cat_value = "İş için"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Soluções"
            f2_cat_value = "Para o negócio"
        end
    elseif content_source == "BSL_DOC" and doc_type == "Software Pre-Sales" then
        if language == "de_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "no_utf" then
            f1_cat_value = "Programvare"
            f2_cat_value = doc_type_translation
        elseif language == "fi_utf" then
            f1_cat_value = "Ohjelmistot"
            f2_cat_value = doc_type_translation
        elseif language == "ru_utf" then
            f1_cat_value = "Программное обеспечение"
            f2_cat_value = doc_type_translation
        elseif language == "bg_utf" then
            f1_cat_value = "Софтуер"
            f2_cat_value = doc_type_translation
        elseif language == "lt_utf" then
            f1_cat_value = "Programinė įranga"
            f2_cat_value = doc_type_translation
        elseif language == "hr_utf" then
            f1_cat_value = "Softver"
            f2_cat_value = doc_type_translation
        elseif language == "lv_utf" then
            f1_cat_value = "Programmatūra"
            f2_cat_value = doc_type_translation
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "fr_utf" then
            f1_cat_value = "Logiciels"
            f2_cat_value = doc_type_translation
        elseif language == "hu_utf" then
            f1_cat_value = "Szoftver"
            f2_cat_value = doc_type_translation
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "軟體"
            f2_cat_value = doc_type_translation
        elseif language == "uk_utf" then
            f1_cat_value = "Програмне забезпечення"
            f2_cat_value = doc_type_translation
        elseif language == "sk_utf" then
            f1_cat_value = "Softvér"
            f2_cat_value = doc_type_translation
        elseif language == "sl_utf" then
            f1_cat_value = "Programska oprema"
            f2_cat_value = doc_type_translation
        elseif language == "id_utf" then
            f1_cat_value = "Perangkat Lunak"
            f2_cat_value = doc_type_translation
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "sr_utf" then
            f1_cat_value = "Softver"
            f2_cat_value = doc_type_translation
        elseif language == "sv_utf" then
            f1_cat_value = "Programvara"
            f2_cat_value = doc_type_translation
        elseif language == "ko_utf" then
            f1_cat_value = "소프트웨어"
            f2_cat_value = doc_type_translation
        elseif language == "el_utf" then
            f1_cat_value = "Λογισμικό"
            f2_cat_value = doc_type_translation
        elseif language == "en_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "it_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "et_utf" then
            f1_cat_value = "Tarkvara"
            f2_cat_value = doc_type_translation
        elseif language == "cs_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "ar_utf" then
            f1_cat_value = "البرامج"
            f2_cat_value = doc_type_translation
        elseif language == "vi_utf" then
            f1_cat_value = "Phần mềm"
            f2_cat_value = doc_type_translation
        elseif language == "th_utf" then
            f1_cat_value = "ซอฟต์แวร์"
            f2_cat_value = doc_type_translation
        elseif language == "ja_utf" then
            f1_cat_value = "ソフトウェア"
            f2_cat_value = doc_type_translation
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "软件"
            f2_cat_value = doc_type_translation
        elseif language == "pl_utf" then
            f1_cat_value = "Oprogramowanie"
            f2_cat_value = doc_type_translation
        elseif language == "da_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "he_utf" then
            f1_cat_value = "תוכנה"
            f2_cat_value = doc_type_translation
        elseif language == "ro_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "nl_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "tr_utf" then
            f1_cat_value = "Yazılımlar"
            f2_cat_value = doc_type_translation
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        end
    elseif content_source == "BSL_DOC" and doc_type == "Customer references" then
        if language == "de_utf" then
            f1_cat_value = "Über uns"
            f2_cat_value = "Erfolgsgeschichten von Kunden"
        elseif language == "no_utf" then
            f1_cat_value = "Selskap"
            f2_cat_value = "Våre kunders suksesshistorier"
        elseif language == "fi_utf" then
            f1_cat_value = "Yritys"
            f2_cat_value = "Asiakkaiden menestystarinat"
        elseif language == "ru_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "Истории успеха наших клиентов"
        elseif language == "bg_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "Истории за успех на клиенти"
        elseif language == "lt_utf" then
            f1_cat_value = "Mūsų Įmonė"
            f2_cat_value = "Užsakovų sėkmės istorijos"
        elseif language == "hr_utf" then
            f1_cat_value = "Poduzeće"
            f2_cat_value = "Svjedočanstva klijenata"
        elseif language == "lv_utf" then
            f1_cat_value = "Uzņēmums"
            f2_cat_value = "Klientu veiksmes stāsti"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Cases de sucesso dos clientes"
        elseif language == "fr_utf" then
            f1_cat_value = "Société"
            f2_cat_value = "Témoignages clients"
        elseif language == "hu_utf" then
            f1_cat_value = "Céges információk"
            f2_cat_value = "Ügyfeleink sikertörténetei"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "公司"
            f2_cat_value = "客戶成功案例"
        elseif language == "uk_utf" then
            f1_cat_value = "Компанія"
            f2_cat_value = "Історії успіху наших користувачів"
        elseif language == "sk_utf" then
            f1_cat_value = "Spoločnosť"
            f2_cat_value = "Príbehy o úspechoch našich zákazníkov"
        elseif language == "sl_utf" then
            f1_cat_value = "Podjetje"
            f2_cat_value = "Zgodbe o uspehu strank"
        elseif language == "id_utf" then
            f1_cat_value = "Perusahaan"
            f2_cat_value = "Kisah Sukses Pelanggan"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Casos de éxito de clientes"
        elseif language == "sr_utf" then
            f1_cat_value = "Kompanija"
            f2_cat_value = "Priče o uspehu klijenata"
        elseif language == "sv_utf" then
            f1_cat_value = "Företaget"
            f2_cat_value = "Kundreferenser"
        elseif language == "ko_utf" then
            f1_cat_value = "회사"
            f2_cat_value = "고객 성공 사례"
        elseif language == "el_utf" then
            f1_cat_value = "Εταιρεία"
            f2_cat_value = "Ιστορίες επιτυχίας πελατών"
        elseif language == "en_utf" then
            f1_cat_value = "Company"
            f2_cat_value = "Customer Success Stories"
        elseif language == "it_utf" then
            f1_cat_value = "Società"
            f2_cat_value = "Storie di successo dei clienti"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Casos de éxito de clientes"
        elseif language == "et_utf" then
            f1_cat_value = "Ettevõte"
            f2_cat_value = "Klientide edulood"
        elseif language == "cs_utf" then
            f1_cat_value = "Společnost"
            f2_cat_value = "Úspěšné příběhy zákazníků"
        elseif language == "ar_utf" then
            f1_cat_value = "الشركة"
            f2_cat_value = "قصص نجاح العملاء"
        elseif language == "vi_utf" then
            f1_cat_value = "Công ty"
            f2_cat_value = "Câu chuyện thành công của khách hàng"
        elseif language == "th_utf" then
            f1_cat_value = "บริษัท"
            f2_cat_value = "เรื่องราวความสำเร็จของลูกค้า"
        elseif language == "ja_utf" then
            f1_cat_value = "会社名"
            f2_cat_value = "お客様の導入事例"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "公司"
            f2_cat_value = "客户成功案例"
        elseif language == "pl_utf" then
            f1_cat_value = "Firma"
            f2_cat_value = "Historia sukcesu naszych Klientów"
        elseif language == "da_utf" then
            f1_cat_value = "Virksomhed"
            f2_cat_value = "Kundehistorier"
        elseif language == "he_utf" then
            f1_cat_value = "חברה"
            f2_cat_value = "סיפורי הצלחה של לקוחות"
        elseif language == "ro_utf" then
            f1_cat_value = "Companie"
            f2_cat_value = "Povesti de succes ale clientilor"
        elseif language == "nl_utf" then
            f1_cat_value = "Bedrijf"
            f2_cat_value = "Succesverhalen van klanten"
        elseif language == "tr_utf" then
            f1_cat_value = "Şirket"
            f2_cat_value = "Müşteri Başarı Hikayeleri"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Casos de sucesso"
        end
    elseif content_source == "BSL_DOC" and doc_type == "Software" then
        if language == "de_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "no_utf" then
            f1_cat_value = "Programvare"
            f2_cat_value = doc_type_translation
        elseif language == "fi_utf" then
            f1_cat_value = "Ohjelmistot"
            f2_cat_value = doc_type_translation
        elseif language == "ru_utf" then
            f1_cat_value = "Программное обеспечение"
            f2_cat_value = doc_type_translation
        elseif language == "bg_utf" then
            f1_cat_value = "Софтуер"
            f2_cat_value = doc_type_translation
        elseif language == "lt_utf" then
            f1_cat_value = "Programinė įranga"
            f2_cat_value = doc_type_translation
        elseif language == "hr_utf" then
            f1_cat_value = "Softver"
            f2_cat_value = doc_type_translation
        elseif language == "lv_utf" then
            f1_cat_value = "Programmatūra"
            f2_cat_value = doc_type_translation
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "fr_utf" then
            f1_cat_value = "Logiciels"
            f2_cat_value = doc_type_translation
        elseif language == "hu_utf" then
            f1_cat_value = "Szoftver"
            f2_cat_value = doc_type_translation
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "軟體"
            f2_cat_value = doc_type_translation
        elseif language == "uk_utf" then
            f1_cat_value = "Програмне забезпечення"
            f2_cat_value = doc_type_translation
        elseif language == "sk_utf" then
            f1_cat_value = "Softvér"
            f2_cat_value = doc_type_translation
        elseif language == "sl_utf" then
            f1_cat_value = "Programska oprema"
            f2_cat_value = doc_type_translation
        elseif language == "id_utf" then
            f1_cat_value = "Perangkat Lunak"
            f2_cat_value = doc_type_translation
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "sr_utf" then
            f1_cat_value = "Softver"
            f2_cat_value = doc_type_translation
        elseif language == "sv_utf" then
            f1_cat_value = "Programvara"
            f2_cat_value = doc_type_translation
        elseif language == "ko_utf" then
            f1_cat_value = "소프트웨어"
            f2_cat_value = doc_type_translation
        elseif language == "el_utf" then
            f1_cat_value = "Λογισμικό"
            f2_cat_value = doc_type_translation
        elseif language == "en_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "it_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "et_utf" then
            f1_cat_value = "Tarkvara"
            f2_cat_value = doc_type_translation
        elseif language == "cs_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "ar_utf" then
            f1_cat_value = "البرامج"
            f2_cat_value = doc_type_translation
        elseif language == "vi_utf" then
            f1_cat_value = "Phần mềm"
            f2_cat_value = doc_type_translation
        elseif language == "th_utf" then
            f1_cat_value = "ซอฟต์แวร์"
            f2_cat_value = doc_type_translation
        elseif language == "ja_utf" then
            f1_cat_value = "ソフトウェア"
            f2_cat_value = doc_type_translation
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "软件"
            f2_cat_value = doc_type_translation
        elseif language == "pl_utf" then
            f1_cat_value = "Oprogramowanie"
            f2_cat_value = doc_type_translation
        elseif language == "da_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "he_utf" then
            f1_cat_value = "תוכנה"
            f2_cat_value = doc_type_translation
        elseif language == "ro_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "nl_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "tr_utf" then
            f1_cat_value = "Yazılımlar"
            f2_cat_value = doc_type_translation
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        end
    elseif content_source == "BSL_DOC" and doc_type == "Software - Archive" then
        if language == "de_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "no_utf" then
            f1_cat_value = "Programvare"
            f2_cat_value = doc_type_translation
        elseif language == "fi_utf" then
            f1_cat_value = "Ohjelmistot"
            f2_cat_value = doc_type_translation
        elseif language == "ru_utf" then
            f1_cat_value = "Программное обеспечение"
            f2_cat_value = doc_type_translation
        elseif language == "bg_utf" then
            f1_cat_value = "Софтуер"
            f2_cat_value = doc_type_translation
        elseif language == "lt_utf" then
            f1_cat_value = "Programinė įranga"
            f2_cat_value = doc_type_translation
        elseif language == "hr_utf" then
            f1_cat_value = "Softver"
            f2_cat_value = doc_type_translation
        elseif language == "lv_utf" then
            f1_cat_value = "Programmatūra"
            f2_cat_value = doc_type_translation
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "fr_utf" then
            f1_cat_value = "Logiciels"
            f2_cat_value = doc_type_translation
        elseif language == "hu_utf" then
            f1_cat_value = "Szoftver"
            f2_cat_value = doc_type_translation
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "軟體"
            f2_cat_value = doc_type_translation
        elseif language == "uk_utf" then
            f1_cat_value = "Програмне забезпечення"
            f2_cat_value = doc_type_translation
        elseif language == "sk_utf" then
            f1_cat_value = "Softvér"
            f2_cat_value = doc_type_translation
        elseif language == "sl_utf" then
            f1_cat_value = "Programska oprema"
            f2_cat_value = doc_type_translation
        elseif language == "id_utf" then
            f1_cat_value = "Perangkat Lunak"
            f2_cat_value = doc_type_translation
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "sr_utf" then
            f1_cat_value = "Softver"
            f2_cat_value = doc_type_translation
        elseif language == "sv_utf" then
            f1_cat_value = "Programvara"
            f2_cat_value = doc_type_translation
        elseif language == "ko_utf" then
            f1_cat_value = "소프트웨어"
            f2_cat_value = doc_type_translation
        elseif language == "el_utf" then
            f1_cat_value = "Λογισμικό"
            f2_cat_value = doc_type_translation
        elseif language == "en_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "it_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "et_utf" then
            f1_cat_value = "Tarkvara"
            f2_cat_value = doc_type_translation
        elseif language == "cs_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "ar_utf" then
            f1_cat_value = "البرامج"
            f2_cat_value = doc_type_translation
        elseif language == "vi_utf" then
            f1_cat_value = "Phần mềm"
            f2_cat_value = doc_type_translation
        elseif language == "th_utf" then
            f1_cat_value = "ซอฟต์แวร์"
            f2_cat_value = doc_type_translation
        elseif language == "ja_utf" then
            f1_cat_value = "ソフトウェア"
            f2_cat_value = doc_type_translation
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "软件"
            f2_cat_value = doc_type_translation
        elseif language == "pl_utf" then
            f1_cat_value = "Oprogramowanie"
            f2_cat_value = doc_type_translation
        elseif language == "da_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "he_utf" then
            f1_cat_value = "תוכנה"
            f2_cat_value = doc_type_translation
        elseif language == "ro_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "nl_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "tr_utf" then
            f1_cat_value = "Yazılımlar"
            f2_cat_value = doc_type_translation
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        end
    elseif content_source == "BSL_DOC" and doc_type == "Software - Hotfix & Patch" then
        if language == "de_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "no_utf" then
            f1_cat_value = "Programvare"
            f2_cat_value = doc_type_translation
        elseif language == "fi_utf" then
            f1_cat_value = "Ohjelmistot"
            f2_cat_value = doc_type_translation
        elseif language == "ru_utf" then
            f1_cat_value = "Программное обеспечение"
            f2_cat_value = doc_type_translation
        elseif language == "bg_utf" then
            f1_cat_value = "Софтуер"
            f2_cat_value = doc_type_translation
        elseif language == "lt_utf" then
            f1_cat_value = "Programinė įranga"
            f2_cat_value = doc_type_translation
        elseif language == "hr_utf" then
            f1_cat_value = "Softver"
            f2_cat_value = doc_type_translation
        elseif language == "lv_utf" then
            f1_cat_value = "Programmatūra"
            f2_cat_value = doc_type_translation
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "fr_utf" then
            f1_cat_value = "Logiciels"
            f2_cat_value = doc_type_translation
        elseif language == "hu_utf" then
            f1_cat_value = "Szoftver"
            f2_cat_value = doc_type_translation
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "軟體"
            f2_cat_value = doc_type_translation
        elseif language == "uk_utf" then
            f1_cat_value = "Програмне забезпечення"
            f2_cat_value = doc_type_translation
        elseif language == "sk_utf" then
            f1_cat_value = "Softvér"
            f2_cat_value = doc_type_translation
        elseif language == "sl_utf" then
            f1_cat_value = "Programska oprema"
            f2_cat_value = doc_type_translation
        elseif language == "id_utf" then
            f1_cat_value = "Perangkat Lunak"
            f2_cat_value = doc_type_translation
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "sr_utf" then
            f1_cat_value = "Softver"
            f2_cat_value = doc_type_translation
        elseif language == "sv_utf" then
            f1_cat_value = "Programvara"
            f2_cat_value = doc_type_translation
        elseif language == "ko_utf" then
            f1_cat_value = "소프트웨어"
            f2_cat_value = doc_type_translation
        elseif language == "el_utf" then
            f1_cat_value = "Λογισμικό"
            f2_cat_value = doc_type_translation
        elseif language == "en_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "it_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "et_utf" then
            f1_cat_value = "Tarkvara"
            f2_cat_value = doc_type_translation
        elseif language == "cs_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "ar_utf" then
            f1_cat_value = "البرامج"
            f2_cat_value = doc_type_translation
        elseif language == "vi_utf" then
            f1_cat_value = "Phần mềm"
            f2_cat_value = doc_type_translation
        elseif language == "th_utf" then
            f1_cat_value = "ซอฟต์แวร์"
            f2_cat_value = doc_type_translation
        elseif language == "ja_utf" then
            f1_cat_value = "ソフトウェア"
            f2_cat_value = doc_type_translation
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "软件"
            f2_cat_value = doc_type_translation
        elseif language == "pl_utf" then
            f1_cat_value = "Oprogramowanie"
            f2_cat_value = doc_type_translation
        elseif language == "da_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "he_utf" then
            f1_cat_value = "תוכנה"
            f2_cat_value = doc_type_translation
        elseif language == "ro_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "nl_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "tr_utf" then
            f1_cat_value = "Yazılımlar"
            f2_cat_value = doc_type_translation
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        end
    elseif content_source == "BSL_DOC" and doc_type == "Press" then
        if language == "de_utf" then
            f1_cat_value = "Über uns"
            f2_cat_value = "Pressemitteilungen"
        elseif language == "no_utf" then
            f1_cat_value = "Selskap"
            f2_cat_value = "Trykk"
        elseif language == "fi_utf" then
            f1_cat_value = "Yritys"
            f2_cat_value = "Lehdistötiedote"
        elseif language == "ru_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "Пресса"
        elseif language == "bg_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "Преса"
        elseif language == "lt_utf" then
            f1_cat_value = "Mūsų Įmonė"
            f2_cat_value = "Spauda"
        elseif language == "hr_utf" then
            f1_cat_value = "Poduzeće"
            f2_cat_value = "Press"
        elseif language == "lv_utf" then
            f1_cat_value = "Uzņēmums"
            f2_cat_value = "Prese"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Comunicado de imprensa"
        elseif language == "fr_utf" then
            f1_cat_value = "Société"
            f2_cat_value = "Presse"
        elseif language == "hu_utf" then
            f1_cat_value = "Céges információk"
            f2_cat_value = "Présgép"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "公司"
            f2_cat_value = "媒體"
        elseif language == "uk_utf" then
            f1_cat_value = "Компанія"
            f2_cat_value = "ЗМІ"
        elseif language == "sk_utf" then
            f1_cat_value = "Spoločnosť"
            f2_cat_value = "Lis"
        elseif language == "sl_utf" then
            f1_cat_value = "Podjetje"
            f2_cat_value = "Medijski kotiček"
        elseif language == "id_utf" then
            f1_cat_value = "Perusahaan"
            f2_cat_value = "Tekan"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Prensa"
        elseif language == "sr_utf" then
            f1_cat_value = "Kompanija"
            f2_cat_value = "Novosti"
        elseif language == "sv_utf" then
            f1_cat_value = "Företaget"
            f2_cat_value = "Press"
        elseif language == "ko_utf" then
            f1_cat_value = "회사"
            f2_cat_value = "보도 자료"
        elseif language == "el_utf" then
            f1_cat_value = "Εταιρεία"
            f2_cat_value = "Νέα"
        elseif language == "en_utf" then
            f1_cat_value = "Company"
            f2_cat_value = "Press"
        elseif language == "it_utf" then
            f1_cat_value = "Società"
            f2_cat_value = "Stampa"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Prensa"
        elseif language == "et_utf" then
            f1_cat_value = "Ettevõte"
            f2_cat_value = "Vajutage"
        elseif language == "cs_utf" then
            f1_cat_value = "Společnost"
            f2_cat_value = "Lis"
        elseif language == "ar_utf" then
            f1_cat_value = "الشركة"
            f2_cat_value = "الصحافة"
        elseif language == "vi_utf" then
            f1_cat_value = "Công ty"
            f2_cat_value = "Báo chí"
        elseif language == "th_utf" then
            f1_cat_value = "บริษัท"
            f2_cat_value = "ข่าวประชาสัมพันธ์"
        elseif language == "ja_utf" then
            f1_cat_value = "会社名"
            f2_cat_value = "プレスリリース"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "公司"
            f2_cat_value = "新闻"
        elseif language == "pl_utf" then
            f1_cat_value = "Firma"
            f2_cat_value = "Prasa"
        elseif language == "da_utf" then
            f1_cat_value = "Virksomhed"
            f2_cat_value = "Presse"
        elseif language == "he_utf" then
            f1_cat_value = "חברה"
            f2_cat_value = "עיתונות"
        elseif language == "ro_utf" then
            f1_cat_value = "Companie"
            f2_cat_value = "Presa"
        elseif language == "nl_utf" then
            f1_cat_value = "Bedrijf"
            f2_cat_value = "Pers"
        elseif language == "tr_utf" then
            f1_cat_value = "Şirket"
            f2_cat_value = "Basın"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Comunicado de imprensa"
        end
    elseif content_source == "BSL_DOC" and doc_type == "Software - ETS3" then
        if language == "de_utf" then
            f1_cat_value = "Software"
            f2_cat_value = "Software - ETS"
        elseif language == "no_utf" then
            f1_cat_value = "Programvare"
            f2_cat_value = "Software - ETS"
        elseif language == "fi_utf" then
            f1_cat_value = "Ohjelmistot"
            f2_cat_value = "Ohjelmistot - ETS"
        elseif language == "ru_utf" then
            f1_cat_value = "Программное обеспечение"
            f2_cat_value = "Программное обеспечение - ETS"
        elseif language == "bg_utf" then
            f1_cat_value = "Софтуер"
            f2_cat_value = "Софтуер - ETS"
        elseif language == "lt_utf" then
            f1_cat_value = "Programinė įranga"
            f2_cat_value = "Prog. Įrangos - ETS"
        elseif language == "hr_utf" then
            f1_cat_value = "Softver"
            f2_cat_value = "Softver - ETS"
        elseif language == "lv_utf" then
            f1_cat_value = "Programmatūra"
            f2_cat_value = "Programmatūra - ETS"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Software"
            f2_cat_value = "Software - ETS"
        elseif language == "fr_utf" then
            f1_cat_value = "Logiciels"
            f2_cat_value = "Logiciel - ETS"
        elseif language == "hu_utf" then
            f1_cat_value = "Szoftver"
            f2_cat_value = "Szoftver - ETS"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "軟體"
            f2_cat_value = "軟體 - ETS"
        elseif language == "uk_utf" then
            f1_cat_value = "Програмне забезпечення"
            f2_cat_value = "ПЗ - ETS"
        elseif language == "sk_utf" then
            f1_cat_value = "Softvér"
            f2_cat_value = "Software - ETS"
        elseif language == "sl_utf" then
            f1_cat_value = "Programska oprema"
            f2_cat_value = "Programska oprema - ETS"
        elseif language == "id_utf" then
            f1_cat_value = "Perangkat Lunak"
            f2_cat_value = "perangkat lunak - ETS"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Software"
            f2_cat_value = "Software - ETS"
        elseif language == "sr_utf" then
            f1_cat_value = "Softver"
            f2_cat_value = "Softver - ETS"
        elseif language == "sv_utf" then
            f1_cat_value = "Programvara"
            f2_cat_value = "Mjukvara - ETS"
        elseif language == "ko_utf" then
            f1_cat_value = "소프트웨어"
            f2_cat_value = "소프트웨어-ETS"
        elseif language == "el_utf" then
            f1_cat_value = "Λογισμικό"
            f2_cat_value = "Λογισμικό - ETS"
        elseif language == "en_utf" then
            f1_cat_value = "Software"
            f2_cat_value = "Software - ETS"
        elseif language == "it_utf" then
            f1_cat_value = "Software"
            f2_cat_value = "Software - ETS"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Software"
            f2_cat_value = "Software - ETS"
        elseif language == "et_utf" then
            f1_cat_value = "Tarkvara"
            f2_cat_value = "Tarkvara - ETS"
        elseif language == "cs_utf" then
            f1_cat_value = "Software"
            f2_cat_value = "Software - ETS"
        elseif language == "ar_utf" then
            f1_cat_value = "البرامج"
            f2_cat_value = "البرمجيات - ETS"
        elseif language == "vi_utf" then
            f1_cat_value = "Phần mềm"
            f2_cat_value = "Phần mềm-ETS"
        elseif language == "th_utf" then
            f1_cat_value = "ซอฟต์แวร์"
            f2_cat_value = "ซอฟต์แวร์ - ETS"
        elseif language == "ja_utf" then
            f1_cat_value = "ソフトウェア"
            f2_cat_value = "ソフトウェア - ETS"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "软件"
            f2_cat_value = "软件-ETS"
        elseif language == "pl_utf" then
            f1_cat_value = "Oprogramowanie"
            f2_cat_value = "Oprogramowanie - ETS"
        elseif language == "da_utf" then
            f1_cat_value = "Software"
            f2_cat_value = "Software - ETS"
        elseif language == "he_utf" then
            f1_cat_value = "תוכנה"
            f2_cat_value = "תוכנה - ETS"
        elseif language == "ro_utf" then
            f1_cat_value = "Software"
            f2_cat_value = "Software - ETS"
        elseif language == "nl_utf" then
            f1_cat_value = "Software"
            f2_cat_value = "Software - ETS"
        elseif language == "tr_utf" then
            f1_cat_value = "Yazılımlar"
            f2_cat_value = "Yazılım - ETS"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Software"
            f2_cat_value = "Software - ETS"
        end
    elseif content_source == "BSL_DOC" and doc_type == "PC Drivers" then
        if language == "de_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "no_utf" then
            f1_cat_value = "Programvare"
            f2_cat_value = doc_type_translation
        elseif language == "fi_utf" then
            f1_cat_value = "Ohjelmistot"
            f2_cat_value = doc_type_translation
        elseif language == "ru_utf" then
            f1_cat_value = "Программное обеспечение"
            f2_cat_value = doc_type_translation
        elseif language == "bg_utf" then
            f1_cat_value = "Софтуер"
            f2_cat_value = doc_type_translation
        elseif language == "lt_utf" then
            f1_cat_value = "Programinė įranga"
            f2_cat_value = doc_type_translation
        elseif language == "hr_utf" then
            f1_cat_value = "Softver"
            f2_cat_value = doc_type_translation
        elseif language == "lv_utf" then
            f1_cat_value = "Programmatūra"
            f2_cat_value = doc_type_translation
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "fr_utf" then
            f1_cat_value = "Logiciels"
            f2_cat_value = doc_type_translation
        elseif language == "hu_utf" then
            f1_cat_value = "Szoftver"
            f2_cat_value = doc_type_translation
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "軟體"
            f2_cat_value = doc_type_translation
        elseif language == "uk_utf" then
            f1_cat_value = "Програмне забезпечення"
            f2_cat_value = doc_type_translation
        elseif language == "sk_utf" then
            f1_cat_value = "Softvér"
            f2_cat_value = doc_type_translation
        elseif language == "sl_utf" then
            f1_cat_value = "Programska oprema"
            f2_cat_value = doc_type_translation
        elseif language == "id_utf" then
            f1_cat_value = "Perangkat Lunak"
            f2_cat_value = doc_type_translation
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "sr_utf" then
            f1_cat_value = "Softver"
            f2_cat_value = doc_type_translation
        elseif language == "sv_utf" then
            f1_cat_value = "Programvara"
            f2_cat_value = doc_type_translation
        elseif language == "ko_utf" then
            f1_cat_value = "소프트웨어"
            f2_cat_value = doc_type_translation
        elseif language == "el_utf" then
            f1_cat_value = "Λογισμικό"
            f2_cat_value = doc_type_translation
        elseif language == "en_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "it_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "et_utf" then
            f1_cat_value = "Tarkvara"
            f2_cat_value = doc_type_translation
        elseif language == "cs_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "ar_utf" then
            f1_cat_value = "البرامج"
            f2_cat_value = doc_type_translation
        elseif language == "vi_utf" then
            f1_cat_value = "Phần mềm"
            f2_cat_value = doc_type_translation
        elseif language == "th_utf" then
            f1_cat_value = "ซอฟต์แวร์"
            f2_cat_value = doc_type_translation
        elseif language == "ja_utf" then
            f1_cat_value = "ソフトウェア"
            f2_cat_value = doc_type_translation
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "软件"
            f2_cat_value = doc_type_translation
        elseif language == "pl_utf" then
            f1_cat_value = "Oprogramowanie"
            f2_cat_value = doc_type_translation
        elseif language == "da_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "he_utf" then
            f1_cat_value = "תוכנה"
            f2_cat_value = doc_type_translation
        elseif language == "ro_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "nl_utf" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        elseif language == "tr_utf" then
            f1_cat_value = "Yazılımlar"
            f2_cat_value = doc_type_translation
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Software"
            f2_cat_value = doc_type_translation
        end
    elseif content_source == "PARTNER_PROMOTED" then
        if language == "de_utf" then
            f1_cat_value = "Lösungen"
            f2_cat_value = "Für Ihr Business"
        elseif language == "no_utf" then
            f1_cat_value = "Løsninger"
            f2_cat_value = "For bedrift"
        elseif language == "fi_utf" then
            f1_cat_value = "Ratkaisut"
            f2_cat_value = "Yritykselle"
        elseif language == "ru_utf" then
            f1_cat_value = "Решения"
            f2_cat_value = "Для бизнеса"
        elseif language == "bg_utf" then
            f1_cat_value = "Решения"
            f2_cat_value = "За бизнеса"
        elseif language == "lt_utf" then
            f1_cat_value = "Sprendimai"
            f2_cat_value = "Verslui"
        elseif language == "hr_utf" then
            f1_cat_value = "Rješenja"
            f2_cat_value = "Rješenja za poslovanje"
        elseif language == "lv_utf" then
            f1_cat_value = "Risinājumi"
            f2_cat_value = "Biznesam"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Soluções"
            f2_cat_value = "Para o negócio"
        elseif language == "fr_utf" then
            f1_cat_value = "Solutions"
            f2_cat_value = "Pour les professionnels"
        elseif language == "hu_utf" then
            f1_cat_value = "Megoldások"
            f2_cat_value = "Üzleti megoldások"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "解决方案"
            f2_cat_value = "營業用"
        elseif language == "uk_utf" then
            f1_cat_value = "Рішення"
            f2_cat_value = "Для бізнесу"
        elseif language == "sk_utf" then
            f1_cat_value = "Riešenia"
            f2_cat_value = "Pre Podnikateľov"
        elseif language == "sl_utf" then
            f1_cat_value = "Rešitve"
            f2_cat_value = "Rešitve za podjetja"
        elseif language == "id_utf" then
            f1_cat_value = "Solusi"
            f2_cat_value = "Untuk bisnis"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Soluciones"
            f2_cat_value = "Para empresas"
        elseif language == "sr_utf" then
            f1_cat_value = "Rešenja"
            f2_cat_value = "Za Vaš biznis"
        elseif language == "sv_utf" then
            f1_cat_value = "Lösningar"
            f2_cat_value = "För företag"
        elseif language == "ko_utf" then
            f1_cat_value = "솔루션"
            f2_cat_value = "파트너 및 기업용솔루션"
        elseif language == "el_utf" then
            f1_cat_value = "Λύσεις"
            f2_cat_value = "Για την επιχείρηση"
        elseif language == "en_utf" then
            f1_cat_value = "Solutions"
            f2_cat_value = "For Business"
        elseif language == "it_utf" then
            f1_cat_value = "Soluzioni"
            f2_cat_value = "Soluzioni per il business"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Soluciones"
            f2_cat_value = "Para empresas"
        elseif language == "et_utf" then
            f1_cat_value = "Lahendused"
            f2_cat_value = "Ettevõtetele"
        elseif language == "cs_utf" then
            f1_cat_value = "Řešení"
            f2_cat_value = "Pro vaše podnikání"
        elseif language == "ar_utf" then
            f1_cat_value = "الحلول"
            f2_cat_value = "للعمل"
        elseif language == "vi_utf" then
            f1_cat_value = "Giải pháp"
            f2_cat_value = "Dành cho Doanh nghiệp"
        elseif language == "th_utf" then
            f1_cat_value = "โซลูชั่น"
            f2_cat_value = "สำหรับธุรกิจ"
        elseif language == "ja_utf" then
            f1_cat_value = "ソリューション"
            f2_cat_value = "事業用"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "解决方案"
            f2_cat_value = "商业用途"
        elseif language == "pl_utf" then
            f1_cat_value = "Rozwiązania"
            f2_cat_value = "Dla biznesu"
        elseif language == "da_utf" then
            f1_cat_value = "Løsninger"
            f2_cat_value = "Til brancher"
        elseif language == "he_utf" then
            f1_cat_value = "פתרונות"
            f2_cat_value = "לעסקים"
        elseif language == "ro_utf" then
            f1_cat_value = "Solutii"
            f2_cat_value = "Pentru afaceri"
        elseif language == "nl_utf" then
            f1_cat_value = "Oplossingen"
            f2_cat_value = "Voor bedrijven"
        elseif language == "tr_utf" then
            f1_cat_value = "Çözümler"
            f2_cat_value = "İş için"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Soluções"
            f2_cat_value = "Para o negócio"
        end
    elseif content_source == "VIDEO_SDL" then
        if language == "de_utf" then
            f1_cat_value = "Über uns"
            f2_cat_value = "Erfolgsgeschichten von Kunden"
        elseif language == "no_utf" then
            f1_cat_value = "Selskap"
            f2_cat_value = "Våre kunders suksesshistorier"
        elseif language == "fi_utf" then
            f1_cat_value = "Yritys"
            f2_cat_value = "Asiakkaiden menestystarinat"
        elseif language == "ru_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "Истории успеха наших клиентов"
        elseif language == "bg_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "Истории за успех на клиенти"
        elseif language == "lt_utf" then
            f1_cat_value = "Mūsų Įmonė"
            f2_cat_value = "Užsakovų sėkmės istorijos"
        elseif language == "hr_utf" then
            f1_cat_value = "Poduzeće"
            f2_cat_value = "Svjedočanstva klijenata"
        elseif language == "lv_utf" then
            f1_cat_value = "Uzņēmums"
            f2_cat_value = "Klientu veiksmes stāsti"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Cases de sucesso dos clientes"
        elseif language == "fr_utf" then
            f1_cat_value = "Société"
            f2_cat_value = "Témoignages clients"
        elseif language == "hu_utf" then
            f1_cat_value = "Céges információk"
            f2_cat_value = "Ügyfeleink sikertörténetei"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "公司"
            f2_cat_value = "客戶成功案例"
        elseif language == "uk_utf" then
            f1_cat_value = "Компанія"
            f2_cat_value = "Історії успіху наших користувачів"
        elseif language == "sk_utf" then
            f1_cat_value = "Spoločnosť"
            f2_cat_value = "Príbehy o úspechoch našich zákazníkov"
        elseif language == "sl_utf" then
            f1_cat_value = "Podjetje"
            f2_cat_value = "Zgodbe o uspehu strank"
        elseif language == "id_utf" then
            f1_cat_value = "Perusahaan"
            f2_cat_value = "Kisah Sukses Pelanggan"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Casos de éxito de clientes"
        elseif language == "sr_utf" then
            f1_cat_value = "Kompanija"
            f2_cat_value = "Priče o uspehu klijenata"
        elseif language == "sv_utf" then
            f1_cat_value = "Företaget"
            f2_cat_value = "Kundreferenser"
        elseif language == "ko_utf" then
            f1_cat_value = "회사"
            f2_cat_value = "고객 성공 사례"
        elseif language == "el_utf" then
            f1_cat_value = "Εταιρεία"
            f2_cat_value = "Ιστορίες επιτυχίας πελατών"
        elseif language == "en_utf" then
            f1_cat_value = "Company"
            f2_cat_value = "Customer Success Stories"
        elseif language == "it_utf" then
            f1_cat_value = "Società"
            f2_cat_value = "Storie di successo dei clienti"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Casos de éxito de clientes"
        elseif language == "et_utf" then
            f1_cat_value = "Ettevõte"
            f2_cat_value = "Klientide edulood"
        elseif language == "cs_utf" then
            f1_cat_value = "Společnost"
            f2_cat_value = "Úspěšné příběhy zákazníků"
        elseif language == "ar_utf" then
            f1_cat_value = "الشركة"
            f2_cat_value = "قصص نجاح العملاء"
        elseif language == "vi_utf" then
            f1_cat_value = "Công ty"
            f2_cat_value = "Câu chuyện thành công của khách hàng"
        elseif language == "th_utf" then
            f1_cat_value = "บริษัท"
            f2_cat_value = "เรื่องราวความสำเร็จของลูกค้า"
        elseif language == "ja_utf" then
            f1_cat_value = "会社名"
            f2_cat_value = "お客様の導入事例"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "公司"
            f2_cat_value = "客户成功案例"
        elseif language == "pl_utf" then
            f1_cat_value = "Firma"
            f2_cat_value = "Historia sukcesu naszych Klientów"
        elseif language == "da_utf" then
            f1_cat_value = "Virksomhed"
            f2_cat_value = "Kundehistorier"
        elseif language == "he_utf" then
            f1_cat_value = "חברה"
            f2_cat_value = "סיפורי הצלחה של לקוחות"
        elseif language == "ro_utf" then
            f1_cat_value = "Companie"
            f2_cat_value = "Povesti de succes ale clientilor"
        elseif language == "nl_utf" then
            f1_cat_value = "Bedrijf"
            f2_cat_value = "Succesverhalen van klanten"
        elseif language == "tr_utf" then
            f1_cat_value = "Şirket"
            f2_cat_value = "Müşteri Başarı Hikayeleri"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Casos de sucesso"
        end
    elseif content_source == "VIDEO_FAQ" then
        if language == "de_utf" then
            f1_cat_value = "FAQ"
            f2_cat_value = "Video"
        elseif language == "no_utf" then
            f1_cat_value = "Vanlige spørsmål"
            f2_cat_value = "Video"
        elseif language == "fi_utf" then
            f1_cat_value = "Usein kysyttyjä kysymyksiä"
            f2_cat_value = " Video"
        elseif language == "ru_utf" then
            f1_cat_value = "Часто задаваемые вопросы"
            f2_cat_value = "Видео"
        elseif language == "bg_utf" then
            f1_cat_value = "Често задавани въпроси"
            f2_cat_value = "Видео"
        elseif language == "lt_utf" then
            f1_cat_value = "DUK"
            f2_cat_value = "Video"
        elseif language == "hr_utf" then
            f1_cat_value = "Česta pitanja"
            f2_cat_value = "Video"
        elseif language == "lv_utf" then
            f1_cat_value = "BUJ"
            f2_cat_value = "Video"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Perguntas frequentes"
            f2_cat_value = "Vídeo"
        elseif language == "fr_utf" then
            f1_cat_value = "Questions fréquentes"
            f2_cat_value = "Vidéo"
        elseif language == "hu_utf" then
            f1_cat_value = "GYIK"
            f2_cat_value = "Videó"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "常見問題"
            f2_cat_value = "影片"
        elseif language == "uk_utf" then
            f1_cat_value = "Відповіді на типові запитання"
            f2_cat_value = "Відео"
        elseif language == "sk_utf" then
            f1_cat_value = "Často kladené otázky"
            f2_cat_value = "Video"
        elseif language == "sl_utf" then
            f1_cat_value = "Pogosta vprašanja"
            f2_cat_value = "Video"
        elseif language == "id_utf" then
            f1_cat_value = "Pertanyaan Umum"
            f2_cat_value = "Video"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Preguntas frecuentes"
            f2_cat_value = "Vídeo"
        elseif language == "sr_utf" then
            f1_cat_value = "Najčešća pitanja"
            f2_cat_value = "Video"
        elseif language == "sv_utf" then
            f1_cat_value = "Vanliga frågor och svar (FAQ)"
            f2_cat_value = "Video"
        elseif language == "ko_utf" then
            f1_cat_value = "FAQ"
            f2_cat_value = "동영상"
        elseif language == "el_utf" then
            f1_cat_value = "Συχνές ερωτήσεις"
            f2_cat_value = "βίντεο"
        elseif language == "en_utf" then
            f1_cat_value = "FAQ"
            f2_cat_value = "Video"
        elseif language == "it_utf" then
            f1_cat_value = "Domande frequenti"
            f2_cat_value = "Video"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Preguntas frecuentes"
            f2_cat_value = "Vídeo"
        elseif language == "et_utf" then
            f1_cat_value = "KKK"
            f2_cat_value = "Video"
        elseif language == "cs_utf" then
            f1_cat_value = "Časté dotazy"
            f2_cat_value = "Video"
        elseif language == "ar_utf" then
            f1_cat_value = "الأسئلة الشائعة"
            f2_cat_value = "فيديو"
        elseif language == "vi_utf" then
            f1_cat_value = "Câu hỏi thường gặp"
            f2_cat_value = "Video"
        elseif language == "th_utf" then
            f1_cat_value = "คำถามที่พบบ่อย"
            f2_cat_value = "วีดีโอ"
        elseif language == "ja_utf" then
            f1_cat_value = "よくある質問"
            f2_cat_value = "ビデオ"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "常见问题"
            f2_cat_value = "视频"
        elseif language == "pl_utf" then
            f1_cat_value = "Często zadawane pytania"
            f2_cat_value = "Wideo"
        elseif language == "da_utf" then
            f1_cat_value = "Ofte stillede spørgsmål"
            f2_cat_value = "Video"
        elseif language == "he_utf" then
            f1_cat_value = "שאלות נפוצות"
            f2_cat_value = "סרטון"
        elseif language == "ro_utf" then
            f1_cat_value = "Intrebari frecvente"
            f2_cat_value = "Video"
        elseif language == "nl_utf" then
            f1_cat_value = "Veelgestelde vragen"
            f2_cat_value = "Video's"
        elseif language == "tr_utf" then
            f1_cat_value = "SSS"
            f2_cat_value = "Video"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Perguntas frequentes"
            f2_cat_value = "Vídeo"
        end
    elseif content_source == "PTN_SDLPAGE" then
        if language == "de_utf" then
            f1_cat_value = "Lösungen"
            f2_cat_value = "Für Ihr Business"
        elseif language == "no_utf" then
            f1_cat_value = "Løsninger"
            f2_cat_value = "For bedrift"
        elseif language == "fi_utf" then
            f1_cat_value = "Ratkaisut"
            f2_cat_value = "Yritykselle"
        elseif language == "ru_utf" then
            f1_cat_value = "Решения"
            f2_cat_value = "Для бизнеса"
        elseif language == "bg_utf" then
            f1_cat_value = "Решения"
            f2_cat_value = "За бизнеса"
        elseif language == "lt_utf" then
            f1_cat_value = "Sprendimai"
            f2_cat_value = "Verslui"
        elseif language == "hr_utf" then
            f1_cat_value = "Rješenja"
            f2_cat_value = "Rješenja za poslovanje"
        elseif language == "lv_utf" then
            f1_cat_value = "Risinājumi"
            f2_cat_value = "Biznesam"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Soluções"
            f2_cat_value = "Para o negócio"
        elseif language == "fr_utf" then
            f1_cat_value = "Solutions"
            f2_cat_value = "Pour les professionnels"
        elseif language == "hu_utf" then
            f1_cat_value = "Megoldások"
            f2_cat_value = "Üzleti megoldások"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "解决方案"
            f2_cat_value = "營業用"
        elseif language == "uk_utf" then
            f1_cat_value = "Рішення"
            f2_cat_value = "Для бізнесу"
        elseif language == "sk_utf" then
            f1_cat_value = "Riešenia"
            f2_cat_value = "Pre Podnikateľov"
        elseif language == "sl_utf" then
            f1_cat_value = "Rešitve"
            f2_cat_value = "Rešitve za podjetja"
        elseif language == "id_utf" then
            f1_cat_value = "Solusi"
            f2_cat_value = "Untuk bisnis"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Soluciones"
            f2_cat_value = "Para empresas"
        elseif language == "sr_utf" then
            f1_cat_value = "Rešenja"
            f2_cat_value = "Za Vaš biznis"
        elseif language == "sv_utf" then
            f1_cat_value = "Lösningar"
            f2_cat_value = "För företag"
        elseif language == "ko_utf" then
            f1_cat_value = "솔루션"
            f2_cat_value = "파트너 및 기업용솔루션"
        elseif language == "el_utf" then
            f1_cat_value = "Λύσεις"
            f2_cat_value = "Για την επιχείρηση"
        elseif language == "en_utf" then
            f1_cat_value = "Solutions"
            f2_cat_value = "For Business"
        elseif language == "it_utf" then
            f1_cat_value = "Soluzioni"
            f2_cat_value = "Soluzioni per il business"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Soluciones"
            f2_cat_value = "Para empresas"
        elseif language == "et_utf" then
            f1_cat_value = "Lahendused"
            f2_cat_value = "Ettevõtetele"
        elseif language == "cs_utf" then
            f1_cat_value = "Řešení"
            f2_cat_value = "Pro vaše podnikání"
        elseif language == "ar_utf" then
            f1_cat_value = "الحلول"
            f2_cat_value = "للعمل"
        elseif language == "vi_utf" then
            f1_cat_value = "Giải pháp"
            f2_cat_value = "Dành cho Doanh nghiệp"
        elseif language == "th_utf" then
            f1_cat_value = "โซลูชั่น"
            f2_cat_value = "สำหรับธุรกิจ"
        elseif language == "ja_utf" then
            f1_cat_value = "ソリューション"
            f2_cat_value = "事業用"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "解决方案"
            f2_cat_value = "商业用途"
        elseif language == "pl_utf" then
            f1_cat_value = "Rozwiązania"
            f2_cat_value = "Dla biznesu"
        elseif language == "da_utf" then
            f1_cat_value = "Løsninger"
            f2_cat_value = "Til brancher"
        elseif language == "he_utf" then
            f1_cat_value = "פתרונות"
            f2_cat_value = "לעסקים"
        elseif language == "ro_utf" then
            f1_cat_value = "Solutii"
            f2_cat_value = "Pentru afaceri"
        elseif language == "nl_utf" then
            f1_cat_value = "Oplossingen"
            f2_cat_value = "Voor bedrijven"
        elseif language == "tr_utf" then
            f1_cat_value = "Çözümler"
            f2_cat_value = "İş için"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Soluções"
            f2_cat_value = "Para o negócio"
        end
    elseif content_source == "FAMILY_PES" then
        if language == "de_utf" then
            f1_cat_value = "Produkte"
            f2_cat_value = "Produktunterkategorie"
        elseif language == "no_utf" then
            f1_cat_value = "Produkter"
            f2_cat_value = "Underkategori"
        elseif language == "fi_utf" then
            f1_cat_value = "Tuotteet"
            f2_cat_value = "Tuotteen ala-luokka"
        elseif language == "ru_utf" then
            f1_cat_value = "Продукты"
            f2_cat_value = "Линейка продуктов"
        elseif language == "bg_utf" then
            f1_cat_value = "Продукти"
            f2_cat_value = "Продуктова подкатегория"
        elseif language == "lt_utf" then
            f1_cat_value = "Gaminiai"
            f2_cat_value = "Produktų pogrupis"
        elseif language == "hr_utf" then
            f1_cat_value = "Proizvodi"
            f2_cat_value = "Podkategorija proizvoda"
        elseif language == "lv_utf" then
            f1_cat_value = "Produkti"
            f2_cat_value = "Produkta apakškategorija"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Produtos"
            f2_cat_value = "Subcategoria de produto"
        elseif language == "fr_utf" then
            f1_cat_value = "Produits"
            f2_cat_value = "Sous-catégorie de produits"
        elseif language == "hu_utf" then
            f1_cat_value = "Termékek"
            f2_cat_value = "Termékcsalád"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "產品"
            f2_cat_value = "產品次類別"
        elseif language == "uk_utf" then
            f1_cat_value = "Продукти"
            f2_cat_value = "Підкатегорія товарів"
        elseif language == "sk_utf" then
            f1_cat_value = "Produkty"
            f2_cat_value = "Produktová Podkategória"
        elseif language == "sl_utf" then
            f1_cat_value = "izdelkov"
            f2_cat_value = "Podkategorija izdelka"
        elseif language == "id_utf" then
            f1_cat_value = "Produk"
            f2_cat_value = "Subkategori produk"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Productos"
            f2_cat_value = "Subcategoría de producto"
        elseif language == "sr_utf" then
            f1_cat_value = "Proizvodi"
            f2_cat_value = "Podkategorija proizvoda"
        elseif language == "sv_utf" then
            f1_cat_value = "Produkter"
            f2_cat_value = "produktunderkategori"
        elseif language == "ko_utf" then
            f1_cat_value = "제품"
            f2_cat_value = "하위 제품카테고리"
        elseif language == "el_utf" then
            f1_cat_value = "Προϊόντα"
            f2_cat_value = "Υποκατηγορία προϊόντων"
        elseif language == "en_utf" then
            f1_cat_value = "Products"
            f2_cat_value = "Product Sub-category"
        elseif language == "it_utf" then
            f1_cat_value = "Prodotti"
            f2_cat_value = "Sottocategoria di prodotti"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Productos"
            f2_cat_value = "Subcategoría de producto"
        elseif language == "et_utf" then
            f1_cat_value = "Tooted"
            f2_cat_value = "Tootepere"
        elseif language == "cs_utf" then
            f1_cat_value = "Produkty"
            f2_cat_value = "Podkategorie produktů"
        elseif language == "ar_utf" then
            f1_cat_value = "المنتجات"
            f2_cat_value = "عائلة المنتجات"
        elseif language == "vi_utf" then
            f1_cat_value = "Sản phẩm"
            f2_cat_value = "Danh mục phụ sản phẩm"
        elseif language == "th_utf" then
            f1_cat_value = "ผลิตภัณฑ์"
            f2_cat_value = "หมวดหมู่ย่อยสินค้า"
        elseif language == "ja_utf" then
            f1_cat_value = "製品"
            f2_cat_value = "製品ファミリー"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "产品"
            f2_cat_value = "产品分类目"
        elseif language == "pl_utf" then
            f1_cat_value = "Produkty"
            f2_cat_value = "Rodzina produktów"
        elseif language == "da_utf" then
            f1_cat_value = "Produkter"
            f2_cat_value = "produktunderkategori"
        elseif language == "he_utf" then
            f1_cat_value = "מוצרים"
            f2_cat_value = "תת-קטגוריית מוצר"
        elseif language == "ro_utf" then
            f1_cat_value = "Produse"
            f2_cat_value = "Subcategoria produselor"
        elseif language == "nl_utf" then
            f1_cat_value = "Producten"
            f2_cat_value = "Productfamilie"
        elseif language == "tr_utf" then
            f1_cat_value = "Ürünler"
            f2_cat_value = "Ürün alt kategorisi"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Produtos"
            f2_cat_value = "Subcategoria de produto"
        end
    elseif content_source == "ABOUTUS_SDLPAGE_EVENTS" then
        if language == "de_utf" then
            f1_cat_value = "Über uns"
            f2_cat_value = "Unternehmens-Übersicht"
        elseif language == "no_utf" then
            f1_cat_value = "Selskap"
            f2_cat_value = "Om Schneider Electric"
        elseif language == "fi_utf" then
            f1_cat_value = "Yritys"
            f2_cat_value = "Yrityskuvaus"
        elseif language == "ru_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "Сведения о компании"
        elseif language == "bg_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "За компанията"
        elseif language == "lt_utf" then
            f1_cat_value = "Mūsų Įmonė"
            f2_cat_value = " Įmonės apžvalga"
        elseif language == "hr_utf" then
            f1_cat_value = "Poduzeće"
            f2_cat_value = "O Schneider Electric"
        elseif language == "lv_utf" then
            f1_cat_value = "Uzņēmums"
            f2_cat_value = "Kompānijas pārskats"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Resenha da Companhia"
        elseif language == "fr_utf" then
            f1_cat_value = "Société"
            f2_cat_value = "Présentation de la société"
        elseif language == "hu_utf" then
            f1_cat_value = "Céges információk"
            f2_cat_value = "Cégünkről"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "公司"
            f2_cat_value = "公司概況"
        elseif language == "uk_utf" then
            f1_cat_value = "Компанія"
            f2_cat_value = "Загальна інформація про компанію"
        elseif language == "sk_utf" then
            f1_cat_value = "Spoločnosť"
            f2_cat_value = "O spoločnosti"
        elseif language == "sl_utf" then
            f1_cat_value = "Podjetje"
            f2_cat_value = "O Schneider Electric"
        elseif language == "id_utf" then
            f1_cat_value = "Perusahaan"
            f2_cat_value = "Ikhtisar Perusahaan"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Visión general de la empresa"
        elseif language == "sr_utf" then
            f1_cat_value = "Kompanija"
            f2_cat_value = "O kompaniji"
        elseif language == "sv_utf" then
            f1_cat_value = "Företaget"
            f2_cat_value = "Företagsöversikt"
        elseif language == "ko_utf" then
            f1_cat_value = "회사"
            f2_cat_value = "슈나이더일렉트릭 관련"
        elseif language == "el_utf" then
            f1_cat_value = "Εταιρεία"
            f2_cat_value = "Επισκόπηση της εταιρείας"
        elseif language == "en_utf" then
            f1_cat_value = "Company"
            f2_cat_value = "Company Overview"
        elseif language == "it_utf" then
            f1_cat_value = "Società"
            f2_cat_value = "Informazioni aziendali"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Visión general de la empresa"
        elseif language == "et_utf" then
            f1_cat_value = "Ettevõte"
            f2_cat_value = "Ettevõtte ülevaade"
        elseif language == "cs_utf" then
            f1_cat_value = "Společnost"
            f2_cat_value = "O společnosti"
        elseif language == "ar_utf" then
            f1_cat_value = "الشركة"
            f2_cat_value = "نبذة عن الشركة"
        elseif language == "vi_utf" then
            f1_cat_value = "Công ty"
            f2_cat_value = "Tổng quan công ty"
        elseif language == "th_utf" then
            f1_cat_value = "บริษัท"
            f2_cat_value = "ภาพรวมบริษัท"
        elseif language == "ja_utf" then
            f1_cat_value = "会社名"
            f2_cat_value = "会社概要"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "公司"
            f2_cat_value = "集团概览"
        elseif language == "pl_utf" then
            f1_cat_value = "Firma"
            f2_cat_value = "O firmie"
        elseif language == "da_utf" then
            f1_cat_value = "Virksomhed"
            f2_cat_value = "Virksomhedsprofil "
        elseif language == "he_utf" then
            f1_cat_value = "חברה"
            f2_cat_value = "סקירת חברה"
        elseif language == "ro_utf" then
            f1_cat_value = "Companie"
            f2_cat_value = "Prezentarea generală a companiei"
        elseif language == "nl_utf" then
            f1_cat_value = "Bedrijf"
            f2_cat_value = "Over ons"
        elseif language == "tr_utf" then
            f1_cat_value = "Şirket"
            f2_cat_value = "Şirket Genel Bakışı"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Visão Geral da Empresa"
        end
    elseif content_source == "CLIPSAL" then
        if language == "de_utf" then
            f1_cat_value = "Produkte"
            f2_cat_value = "Produkt"
        elseif language == "no_utf" then
            f1_cat_value = "Produkter"
            f2_cat_value = "Produkt"
        elseif language == "fi_utf" then
            f1_cat_value = "Tuotteet"
            f2_cat_value = "Tuote"
        elseif language == "ru_utf" then
            f1_cat_value = "Продукты"
            f2_cat_value = "Продукт"
        elseif language == "bg_utf" then
            f1_cat_value = "Продукти"
            f2_cat_value = "Продукт"
        elseif language == "lt_utf" then
            f1_cat_value = "Gaminiai"
            f2_cat_value = "Produktas"
        elseif language == "hr_utf" then
            f1_cat_value = "Proizvodi"
            f2_cat_value = "Proizvod"
        elseif language == "lv_utf" then
            f1_cat_value = "Produkti"
            f2_cat_value = "Produkts"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Produtos"
            f2_cat_value = "Produto"
        elseif language == "fr_utf" then
            f1_cat_value = "Produits"
            f2_cat_value = "Produit"
        elseif language == "hu_utf" then
            f1_cat_value = "Termékek"
            f2_cat_value = "Termék"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "產品"
            f2_cat_value = "產品"
        elseif language == "uk_utf" then
            f1_cat_value = "Продукти"
            f2_cat_value = "Товар"
        elseif language == "sk_utf" then
            f1_cat_value = "Produkty"
            f2_cat_value = "Produkt"
        elseif language == "sl_utf" then
            f1_cat_value = "izdelkov"
            f2_cat_value = "Izdelka"
        elseif language == "id_utf" then
            f1_cat_value = "Produk"
            f2_cat_value = "Produk"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Productos"
            f2_cat_value = "Producto"
        elseif language == "sr_utf" then
            f1_cat_value = "Proizvodi"
            f2_cat_value = "Proizvod"
        elseif language == "sv_utf" then
            f1_cat_value = "Produkter"
            f2_cat_value = "Produkt"
        elseif language == "ko_utf" then
            f1_cat_value = "제품"
            f2_cat_value = "제품"
        elseif language == "el_utf" then
            f1_cat_value = "Προϊόντα"
            f2_cat_value = "Προϊόν"
        elseif language == "en_utf" then
            f1_cat_value = "Products"
            f2_cat_value = "Product"
        elseif language == "it_utf" then
            f1_cat_value = "Prodotti"
            f2_cat_value = "Prodotto"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Productos"
            f2_cat_value = "Producto"
        elseif language == "et_utf" then
            f1_cat_value = "Tooted"
            f2_cat_value = "Toode"
        elseif language == "cs_utf" then
            f1_cat_value = "Produkty"
            f2_cat_value = "Produkt"
        elseif language == "ar_utf" then
            f1_cat_value = "المنتجات"
            f2_cat_value = "المنتج"
        elseif language == "vi_utf" then
            f1_cat_value = "Sản phẩm"
            f2_cat_value = "Sản phẩm"
        elseif language == "th_utf" then
            f1_cat_value = "ผลิตภัณฑ์"
            f2_cat_value = "ผลิตภัณฑ์"
        elseif language == "ja_utf" then
            f1_cat_value = "製品"
            f2_cat_value = "製品"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "产品"
            f2_cat_value = "产品"
        elseif language == "pl_utf" then
            f1_cat_value = "Produkty"
            f2_cat_value = "Produkt"
        elseif language == "da_utf" then
            f1_cat_value = "Produkter"
            f2_cat_value = "Produkt"
        elseif language == "he_utf" then
            f1_cat_value = "מוצרים"
            f2_cat_value = "מוצר"
        elseif language == "ro_utf" then
            f1_cat_value = "Produse"
            f2_cat_value = "Produs"
        elseif language == "nl_utf" then
            f1_cat_value = "Producten"
            f2_cat_value = "Product"
        elseif language == "tr_utf" then
            f1_cat_value = "Ürünler"
            f2_cat_value = "Ürün"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Produtos"
            f2_cat_value = "Produto"
        end
    elseif content_source == "OFFER_PES" then
        if language == "de_utf" then
            f1_cat_value = "Produkte"
            f2_cat_value = "Produktreihe"
        elseif language == "no_utf" then
            f1_cat_value = "Produkter"
            f2_cat_value = "Produktspekter"
        elseif language == "fi_utf" then
            f1_cat_value = "Tuotteet"
            f2_cat_value = "Tuoteperhe"
        elseif language == "ru_utf" then
            f1_cat_value = "Продукты"
            f2_cat_value = "Серия продукта"
        elseif language == "bg_utf" then
            f1_cat_value = "Продукти"
            f2_cat_value = "Продуктова гама"
        elseif language == "lt_utf" then
            f1_cat_value = "Gaminiai"
            f2_cat_value = "Produktų serija"
        elseif language == "hr_utf" then
            f1_cat_value = "Proizvodi"
            f2_cat_value = "Paleta proizvoda"
        elseif language == "lv_utf" then
            f1_cat_value = "Produkti"
            f2_cat_value = "Produkta klāsts"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Produtos"
            f2_cat_value = "Família de Produto"
        elseif language == "fr_utf" then
            f1_cat_value = "Produits"
            f2_cat_value = "Gamme de produits"
        elseif language == "hu_utf" then
            f1_cat_value = "Termékek"
            f2_cat_value = "Termékválaszték"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "產品"
            f2_cat_value = "產品系列"
        elseif language == "uk_utf" then
            f1_cat_value = "Продукти"
            f2_cat_value = "Асортимент товарів"
        elseif language == "sk_utf" then
            f1_cat_value = "Produkty"
            f2_cat_value = "Produktový Rad"
        elseif language == "sl_utf" then
            f1_cat_value = "izdelkov"
            f2_cat_value = "Paleta izdelkov"
        elseif language == "id_utf" then
            f1_cat_value = "Produk"
            f2_cat_value = "Daftar Produk"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Productos"
            f2_cat_value = "Rango de producto"
        elseif language == "sr_utf" then
            f1_cat_value = "Proizvodi"
            f2_cat_value = "Paleta proizvoda"
        elseif language == "sv_utf" then
            f1_cat_value = "Produkter"
            f2_cat_value = "Produktfamilj"
        elseif language == "ko_utf" then
            f1_cat_value = "제품"
            f2_cat_value = "제품군"
        elseif language == "el_utf" then
            f1_cat_value = "Προϊόντα"
            f2_cat_value = "Σειρά προϊόντων"
        elseif language == "en_utf" then
            f1_cat_value = "Products"
            f2_cat_value = "Product Range"
        elseif language == "it_utf" then
            f1_cat_value = "Prodotti"
            f2_cat_value = "Range prodotto"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Productos"
            f2_cat_value = "Rango de producto"
        elseif language == "et_utf" then
            f1_cat_value = "Tooted"
            f2_cat_value = "Tooteseeria"
        elseif language == "cs_utf" then
            f1_cat_value = "Produkty"
            f2_cat_value = "Produktová řada"
        elseif language == "ar_utf" then
            f1_cat_value = "المنتجات"
            f2_cat_value = "مجموعة المنتجات"
        elseif language == "vi_utf" then
            f1_cat_value = "Sản phẩm"
            f2_cat_value = "Dòng Sản phẩm"
        elseif language == "th_utf" then
            f1_cat_value = "ผลิตภัณฑ์"
            f2_cat_value = "กลุ่มผลิตภัณฑ์"
        elseif language == "ja_utf" then
            f1_cat_value = "製品"
            f2_cat_value = "製品ライン"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "产品"
            f2_cat_value = "产品系列"
        elseif language == "pl_utf" then
            f1_cat_value = "Produkty"
            f2_cat_value = "Gama produktu"
        elseif language == "da_utf" then
            f1_cat_value = "Produkter"
            f2_cat_value = "Produktsortiment"
        elseif language == "he_utf" then
            f1_cat_value = "מוצרים"
            f2_cat_value = "טווח מוצר"
        elseif language == "ro_utf" then
            f1_cat_value = "Produse"
            f2_cat_value = "Gama de produse"
        elseif language == "nl_utf" then
            f1_cat_value = "Producten"
            f2_cat_value = "Productserie"
        elseif language == "tr_utf" then
            f1_cat_value = "Ürünler"
            f2_cat_value = "Ürün Gurubu"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Produtos"
            f2_cat_value = "Gama de Produtos"
        end
    elseif content_source == "HOME_SDLPAGE" then
        if language == "de_utf" then
            f1_cat_value = "Über uns"
            f2_cat_value = "Unternehmens-Übersicht"
        elseif language == "no_utf" then
            f1_cat_value = "Selskap"
            f2_cat_value = "Om Schneider Electric"
        elseif language == "fi_utf" then
            f1_cat_value = "Yritys"
            f2_cat_value = "Yrityskuvaus"
        elseif language == "ru_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "Сведения о компании"
        elseif language == "bg_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "За компанията"
        elseif language == "lt_utf" then
            f1_cat_value = "Mūsų Įmonė"
            f2_cat_value = " Įmonės apžvalga"
        elseif language == "hr_utf" then
            f1_cat_value = "Poduzeće"
            f2_cat_value = "O Schneider Electric"
        elseif language == "lv_utf" then
            f1_cat_value = "Uzņēmums"
            f2_cat_value = "Kompānijas pārskats"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Resenha da Companhia"
        elseif language == "fr_utf" then
            f1_cat_value = "Société"
            f2_cat_value = "Présentation de la société"
        elseif language == "hu_utf" then
            f1_cat_value = "Céges információk"
            f2_cat_value = "Cégünkről"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "公司"
            f2_cat_value = "公司概況"
        elseif language == "uk_utf" then
            f1_cat_value = "Компанія"
            f2_cat_value = "Загальна інформація про компанію"
        elseif language == "sk_utf" then
            f1_cat_value = "Spoločnosť"
            f2_cat_value = "O spoločnosti"
        elseif language == "sl_utf" then
            f1_cat_value = "Podjetje"
            f2_cat_value = "O Schneider Electric"
        elseif language == "id_utf" then
            f1_cat_value = "Perusahaan"
            f2_cat_value = "Ikhtisar Perusahaan"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Visión general de la empresa"
        elseif language == "sr_utf" then
            f1_cat_value = "Kompanija"
            f2_cat_value = "O kompaniji"
        elseif language == "sv_utf" then
            f1_cat_value = "Företaget"
            f2_cat_value = "Företagsöversikt"
        elseif language == "ko_utf" then
            f1_cat_value = "회사"
            f2_cat_value = "슈나이더일렉트릭 관련"
        elseif language == "el_utf" then
            f1_cat_value = "Εταιρεία"
            f2_cat_value = "Επισκόπηση της εταιρείας"
        elseif language == "en_utf" then
            f1_cat_value = "Company"
            f2_cat_value = "Company Overview"
        elseif language == "it_utf" then
            f1_cat_value = "Società"
            f2_cat_value = "Informazioni aziendali"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Visión general de la empresa"
        elseif language == "et_utf" then
            f1_cat_value = "Ettevõte"
            f2_cat_value = "Ettevõtte ülevaade"
        elseif language == "cs_utf" then
            f1_cat_value = "Společnost"
            f2_cat_value = "O společnosti"
        elseif language == "ar_utf" then
            f1_cat_value = "الشركة"
            f2_cat_value = "نبذة عن الشركة"
        elseif language == "vi_utf" then
            f1_cat_value = "Công ty"
            f2_cat_value = "Tổng quan công ty"
        elseif language == "th_utf" then
            f1_cat_value = "บริษัท"
            f2_cat_value = "ภาพรวมบริษัท"
        elseif language == "ja_utf" then
            f1_cat_value = "会社名"
            f2_cat_value = "会社概要"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "公司"
            f2_cat_value = "集团概览"
        elseif language == "pl_utf" then
            f1_cat_value = "Firma"
            f2_cat_value = "O firmie"
        elseif language == "da_utf" then
            f1_cat_value = "Virksomhed"
            f2_cat_value = "Virksomhedsprofil "
        elseif language == "he_utf" then
            f1_cat_value = "חברה"
            f2_cat_value = "סקירת חברה"
        elseif language == "ro_utf" then
            f1_cat_value = "Companie"
            f2_cat_value = "Prezentarea generală a companiei"
        elseif language == "nl_utf" then
            f1_cat_value = "Bedrijf"
            f2_cat_value = "Over ons"
        elseif language == "tr_utf" then
            f1_cat_value = "Şirket"
            f2_cat_value = "Şirket Genel Bakışı"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Visão Geral da Empresa"
        end
    elseif content_source == "S3_DOC" then
        if language == "de_utf" then
            f1_cat_value = "Über uns"
            f2_cat_value = "Pressemitteilungen"
        elseif language == "no_utf" then
            f1_cat_value = "Selskap"
            f2_cat_value = "Trykk"
        elseif language == "fi_utf" then
            f1_cat_value = "Yritys"
            f2_cat_value = "Lehdistötiedote"
        elseif language == "ru_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "Пресса"
        elseif language == "bg_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "Преса"
        elseif language == "lt_utf" then
            f1_cat_value = "Mūsų Įmonė"
            f2_cat_value = "Spauda"
        elseif language == "hr_utf" then
            f1_cat_value = "Poduzeće"
            f2_cat_value = "Press"
        elseif language == "lv_utf" then
            f1_cat_value = "Uzņēmums"
            f2_cat_value = "Prese"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Comunicado de imprensa"
        elseif language == "fr_utf" then
            f1_cat_value = "Société"
            f2_cat_value = "Presse"
        elseif language == "hu_utf" then
            f1_cat_value = "Céges információk"
            f2_cat_value = "Présgép"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "公司"
            f2_cat_value = "媒體"
        elseif language == "uk_utf" then
            f1_cat_value = "Компанія"
            f2_cat_value = "ЗМІ"
        elseif language == "sk_utf" then
            f1_cat_value = "Spoločnosť"
            f2_cat_value = "Lis"
        elseif language == "sl_utf" then
            f1_cat_value = "Podjetje"
            f2_cat_value = "Medijski kotiček"
        elseif language == "id_utf" then
            f1_cat_value = "Perusahaan"
            f2_cat_value = "Tekan"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Prensa"
        elseif language == "sr_utf" then
            f1_cat_value = "Kompanija"
            f2_cat_value = "Novosti"
        elseif language == "sv_utf" then
            f1_cat_value = "Företaget"
            f2_cat_value = "Press"
        elseif language == "ko_utf" then
            f1_cat_value = "회사"
            f2_cat_value = "보도 자료"
        elseif language == "el_utf" then
            f1_cat_value = "Εταιρεία"
            f2_cat_value = "Νέα"
        elseif language == "en_utf" then
            f1_cat_value = "Company"
            f2_cat_value = "Press"
        elseif language == "it_utf" then
            f1_cat_value = "Società"
            f2_cat_value = "Stampa"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Prensa"
        elseif language == "et_utf" then
            f1_cat_value = "Ettevõte"
            f2_cat_value = "Vajutage"
        elseif language == "cs_utf" then
            f1_cat_value = "Společnost"
            f2_cat_value = "Lis"
        elseif language == "ar_utf" then
            f1_cat_value = "الشركة"
            f2_cat_value = "الصحافة"
        elseif language == "vi_utf" then
            f1_cat_value = "Công ty"
            f2_cat_value = "Báo chí"
        elseif language == "th_utf" then
            f1_cat_value = "บริษัท"
            f2_cat_value = "ข่าวประชาสัมพันธ์"
        elseif language == "ja_utf" then
            f1_cat_value = "会社名"
            f2_cat_value = "プレスリリース"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "公司"
            f2_cat_value = "新闻"
        elseif language == "pl_utf" then
            f1_cat_value = "Firma"
            f2_cat_value = "Prasa"
        elseif language == "da_utf" then
            f1_cat_value = "Virksomhed"
            f2_cat_value = "Presse"
        elseif language == "he_utf" then
            f1_cat_value = "חברה"
            f2_cat_value = "עיתונות"
        elseif language == "ro_utf" then
            f1_cat_value = "Companie"
            f2_cat_value = "Presa"
        elseif language == "nl_utf" then
            f1_cat_value = "Bedrijf"
            f2_cat_value = "Pers"
        elseif language == "tr_utf" then
            f1_cat_value = "Şirket"
            f2_cat_value = "Basın"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Comunicado de imprensa"
        end
    elseif content_source == "CAMPGN_SDLPAGE" then
        if language == "de_utf" then
            f1_cat_value = "Über uns"
            f2_cat_value = "Verkaufsförderung"
        elseif language == "no_utf" then
            f1_cat_value = "Selskap"
            f2_cat_value = "temaer"
        elseif language == "fi_utf" then
            f1_cat_value = "Yritys"
            f2_cat_value = "Campaign"
        elseif language == "ru_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "Маркетинговые кампании"
        elseif language == "bg_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "Кампания"
        elseif language == "lt_utf" then
            f1_cat_value = "Mūsų Įmonė"
            f2_cat_value = "EcoStruxure sprendimai"
        elseif language == "hr_utf" then
            f1_cat_value = "Poduzeće"
            f2_cat_value = "Kampanja"
        elseif language == "lv_utf" then
            f1_cat_value = "Uzņēmums"
            f2_cat_value = "Piedāvājumi"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Materiais informativos"
        elseif language == "fr_utf" then
            f1_cat_value = "Société"
            f2_cat_value = "Campagne"
        elseif language == "hu_utf" then
            f1_cat_value = "Céges információk"
            f2_cat_value = "Kampányok"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "公司"
            f2_cat_value = "Campaign"
        elseif language == "uk_utf" then
            f1_cat_value = "Компанія"
            f2_cat_value = "Кампанія"
        elseif language == "sk_utf" then
            f1_cat_value = "Spoločnosť"
            f2_cat_value = "Campaign"
        elseif language == "sl_utf" then
            f1_cat_value = "Podjetje"
            f2_cat_value = "Kampanja"
        elseif language == "id_utf" then
            f1_cat_value = "Perusahaan"
            f2_cat_value = "Campaign"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Campaña"
        elseif language == "sr_utf" then
            f1_cat_value = "Kompanija"
            f2_cat_value = "Kampanja"
        elseif language == "sv_utf" then
            f1_cat_value = "Företaget"
            f2_cat_value = "Campaign"
        elseif language == "ko_utf" then
            f1_cat_value = "회사"
            f2_cat_value = "캠페인"
        elseif language == "el_utf" then
            f1_cat_value = "Εταιρεία"
            f2_cat_value = "Εκστρατείες"
        elseif language == "en_utf" then
            f1_cat_value = "Company"
            f2_cat_value = "Promotions and events"
        elseif language == "it_utf" then
            f1_cat_value = "Società"
            f2_cat_value = "Campagne"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Campaign"
        elseif language == "et_utf" then
            f1_cat_value = "Ettevõte"
            f2_cat_value = "Campaign"
        elseif language == "cs_utf" then
            f1_cat_value = "Společnost"
            f2_cat_value = "Campaign"
        elseif language == "ar_utf" then
            f1_cat_value = "الشركة"
            f2_cat_value = "Campaign"
        elseif language == "vi_utf" then
            f1_cat_value = "Công ty"
            f2_cat_value = "Campaign"
        elseif language == "th_utf" then
            f1_cat_value = "บริษัท"
            f2_cat_value = "แคมเปญ"
        elseif language == "ja_utf" then
            f1_cat_value = "会社名"
            f2_cat_value = "限定コンテンツ"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "公司"
            f2_cat_value = "推广活动"
        elseif language == "pl_utf" then
            f1_cat_value = "Firma"
            f2_cat_value = "Campaign"
        elseif language == "da_utf" then
            f1_cat_value = "Virksomhed"
            f2_cat_value = "Nyhede"
        elseif language == "he_utf" then
            f1_cat_value = "חברה"
            f2_cat_value = "קמפיין"
        elseif language == "ro_utf" then
            f1_cat_value = "Companie"
            f2_cat_value = "campanii"
        elseif language == "nl_utf" then
            f1_cat_value = "Bedrijf"
            f2_cat_value = "Interessant"
        elseif language == "tr_utf" then
            f1_cat_value = "Şirket"
            f2_cat_value = "Campaign"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Campaign"
        end
    elseif content_source == "OFFER_PROMOTED" then
        if language == "de_utf" then
            f1_cat_value = "Produkte"
            f2_cat_value = "Produktreihe"
        elseif language == "no_utf" then
            f1_cat_value = "Produkter"
            f2_cat_value = "Produktspekter"
        elseif language == "fi_utf" then
            f1_cat_value = "Tuotteet"
            f2_cat_value = "Tuoteperhe"
        elseif language == "ru_utf" then
            f1_cat_value = "Продукты"
            f2_cat_value = "Серия продукта"
        elseif language == "bg_utf" then
            f1_cat_value = "Продукти"
            f2_cat_value = "Продуктова гама"
        elseif language == "lt_utf" then
            f1_cat_value = "Gaminiai"
            f2_cat_value = "Produktų serija"
        elseif language == "hr_utf" then
            f1_cat_value = "Proizvodi"
            f2_cat_value = "Paleta proizvoda"
        elseif language == "lv_utf" then
            f1_cat_value = "Produkti"
            f2_cat_value = "Produkta klāsts"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Produtos"
            f2_cat_value = "Família de Produto"
        elseif language == "fr_utf" then
            f1_cat_value = "Produits"
            f2_cat_value = "Gamme de produits"
        elseif language == "hu_utf" then
            f1_cat_value = "Termékek"
            f2_cat_value = "Termékválaszték"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "產品"
            f2_cat_value = "產品系列"
        elseif language == "uk_utf" then
            f1_cat_value = "Продукти"
            f2_cat_value = "Асортимент товарів"
        elseif language == "sk_utf" then
            f1_cat_value = "Produkty"
            f2_cat_value = "Produktový Rad"
        elseif language == "sl_utf" then
            f1_cat_value = "izdelkov"
            f2_cat_value = "Paleta izdelkov"
        elseif language == "id_utf" then
            f1_cat_value = "Produk"
            f2_cat_value = "Daftar Produk"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Productos"
            f2_cat_value = "Rango de producto"
        elseif language == "sr_utf" then
            f1_cat_value = "Proizvodi"
            f2_cat_value = "Paleta proizvoda"
        elseif language == "sv_utf" then
            f1_cat_value = "Produkter"
            f2_cat_value = "Produktfamilj"
        elseif language == "ko_utf" then
            f1_cat_value = "제품"
            f2_cat_value = "제품군"
        elseif language == "el_utf" then
            f1_cat_value = "Προϊόντα"
            f2_cat_value = "Σειρά προϊόντων"
        elseif language == "en_utf" then
            f1_cat_value = "Products"
            f2_cat_value = "Product Range"
        elseif language == "it_utf" then
            f1_cat_value = "Prodotti"
            f2_cat_value = "Range prodotto"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Productos"
            f2_cat_value = "Rango de producto"
        elseif language == "et_utf" then
            f1_cat_value = "Tooted"
            f2_cat_value = "Tooteseeria"
        elseif language == "cs_utf" then
            f1_cat_value = "Produkty"
            f2_cat_value = "Produktová řada"
        elseif language == "ar_utf" then
            f1_cat_value = "المنتجات"
            f2_cat_value = "مجموعة المنتجات"
        elseif language == "vi_utf" then
            f1_cat_value = "Sản phẩm"
            f2_cat_value = "Dòng Sản phẩm"
        elseif language == "th_utf" then
            f1_cat_value = "ผลิตภัณฑ์"
            f2_cat_value = "กลุ่มผลิตภัณฑ์"
        elseif language == "ja_utf" then
            f1_cat_value = "製品"
            f2_cat_value = "製品ライン"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "产品"
            f2_cat_value = "产品系列"
        elseif language == "pl_utf" then
            f1_cat_value = "Produkty"
            f2_cat_value = "Gama produktu"
        elseif language == "da_utf" then
            f1_cat_value = "Produkter"
            f2_cat_value = "Produktsortiment"
        elseif language == "he_utf" then
            f1_cat_value = "מוצרים"
            f2_cat_value = "טווח מוצר"
        elseif language == "ro_utf" then
            f1_cat_value = "Produse"
            f2_cat_value = "Gama de produse"
        elseif language == "nl_utf" then
            f1_cat_value = "Producten"
            f2_cat_value = "Productserie"
        elseif language == "tr_utf" then
            f1_cat_value = "Ürünler"
            f2_cat_value = "Ürün Gurubu"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Produtos"
            f2_cat_value = "Gama de Produtos"
        end
    elseif content_source == "CAT_COMP_VIDEO" then
        if language == "de_utf" then
            f1_cat_value = "Über uns"
            f2_cat_value = "Video"
        elseif language == "no_utf" then
            f1_cat_value = "Selskap"
            f2_cat_value = "Video"
        elseif language == "fi_utf" then
            f1_cat_value = "Yritys"
            f2_cat_value = " Video"
        elseif language == "ru_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "Видео"
        elseif language == "bg_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "Видео"
        elseif language == "lt_utf" then
            f1_cat_value = "Mūsų Įmonė"
            f2_cat_value = "Video"
        elseif language == "hr_utf" then
            f1_cat_value = "Poduzeće"
            f2_cat_value = "Video"
        elseif language == "lv_utf" then
            f1_cat_value = "Uzņēmums"
            f2_cat_value = "Video"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Vídeo"
        elseif language == "fr_utf" then
            f1_cat_value = "Société"
            f2_cat_value = "Vidéo"
        elseif language == "hu_utf" then
            f1_cat_value = "Céges információk"
            f2_cat_value = "Videó"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "公司"
            f2_cat_value = "影片"
        elseif language == "uk_utf" then
            f1_cat_value = "Компанія"
            f2_cat_value = "Відео"
        elseif language == "sk_utf" then
            f1_cat_value = "Spoločnosť"
            f2_cat_value = "Video"
        elseif language == "sl_utf" then
            f1_cat_value = "Podjetje"
            f2_cat_value = "Video"
        elseif language == "id_utf" then
            f1_cat_value = "Perusahaan"
            f2_cat_value = "Video"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Vídeo"
        elseif language == "sr_utf" then
            f1_cat_value = "Kompanija"
            f2_cat_value = "Video"
        elseif language == "sv_utf" then
            f1_cat_value = "Företaget"
            f2_cat_value = "Video"
        elseif language == "ko_utf" then
            f1_cat_value = "회사"
            f2_cat_value = "동영상"
        elseif language == "el_utf" then
            f1_cat_value = "Εταιρεία"
            f2_cat_value = "βίντεο"
        elseif language == "en_utf" then
            f1_cat_value = "Company"
            f2_cat_value = "Video"
        elseif language == "it_utf" then
            f1_cat_value = "Società"
            f2_cat_value = "Video"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Vídeo"
        elseif language == "et_utf" then
            f1_cat_value = "Ettevõte"
            f2_cat_value = "Video"
        elseif language == "cs_utf" then
            f1_cat_value = "Společnost"
            f2_cat_value = "Video"
        elseif language == "ar_utf" then
            f1_cat_value = "الشركة"
            f2_cat_value = "فيديو"
        elseif language == "vi_utf" then
            f1_cat_value = "Công ty"
            f2_cat_value = "Video"
        elseif language == "th_utf" then
            f1_cat_value = "บริษัท"
            f2_cat_value = "วีดีโอ"
        elseif language == "ja_utf" then
            f1_cat_value = "会社名"
            f2_cat_value = "ビデオ"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "公司"
            f2_cat_value = "视频"
        elseif language == "pl_utf" then
            f1_cat_value = "Firma"
            f2_cat_value = "Wideo"
        elseif language == "da_utf" then
            f1_cat_value = "Virksomhed"
            f2_cat_value = "Video"
        elseif language == "he_utf" then
            f1_cat_value = "חברה"
            f2_cat_value = "סרטון"
        elseif language == "ro_utf" then
            f1_cat_value = "Companie"
            f2_cat_value = "Video"
        elseif language == "nl_utf" then
            f1_cat_value = "Bedrijf"
            f2_cat_value = "Video's"
        elseif language == "tr_utf" then
            f1_cat_value = "Şirket"
            f2_cat_value = "Video"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Vídeo"
        end
    elseif content_source == "BSL_DOC" then
        if language == "de_utf" then
            f1_cat_value = "Dokumente"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "no_utf" then
            f1_cat_value = "Dokumenter"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "fi_utf" then
            f1_cat_value = "Asiakirjat"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "ru_utf" then
            f1_cat_value = "Документация"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "bg_utf" then
            f1_cat_value = "Документи"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "lt_utf" then
            f1_cat_value = "Dokumentai"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "hr_utf" then
            f1_cat_value = "Dokumenti"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "lv_utf" then
            f1_cat_value = "Dokumenti"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Documentos"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "fr_utf" then
            f1_cat_value = "Documents"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "hu_utf" then
            f1_cat_value = "Dokumentumok"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "文件"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "uk_utf" then
            f1_cat_value = "Документи"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "sk_utf" then
            f1_cat_value = "Dokumenty"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "sl_utf" then
            f1_cat_value = "Dokumenti"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "id_utf" then
            f1_cat_value = "Dokumen"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Documentos"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "sr_utf" then
            f1_cat_value = "Dokumenti"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "sv_utf" then
            f1_cat_value = "Dokument"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "ko_utf" then
            f1_cat_value = "문서"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "el_utf" then
            f1_cat_value = "Έγγραφα"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "en_utf" then
            f1_cat_value = "Documents"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "it_utf" then
            f1_cat_value = "Documenti"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Documentos"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "et_utf" then
            f1_cat_value = "Dokumendid"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "cs_utf" then
            f1_cat_value = "Dokumenty"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "ar_utf" then
            f1_cat_value = "المستندات"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "vi_utf" then
            f1_cat_value = "Tài liệu"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "th_utf" then
            f1_cat_value = "เอกสาร"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "ja_utf" then
            f1_cat_value = "ドキュメント"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "文档"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "pl_utf" then
            f1_cat_value = "Dokumenty"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "da_utf" then
            f1_cat_value = "Dokumenter"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "he_utf" then
            f1_cat_value = "מסמכים"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "ro_utf" then
            f1_cat_value = "Documente"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "nl_utf" then
            f1_cat_value = "Documenten"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "tr_utf" then
            f1_cat_value = "Belgeler"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Documentos"
            f2_cat_value = doc_type_group_translation
            f3_cat_value = doc_type_translation
        end
    elseif content_source == "BRANDS_SDLPAGE" then
        if language == "de_utf" then
            f1_cat_value = "Lösungen"
            f2_cat_value = "Für Ihr Zuhause"
        elseif language == "no_utf" then
            f1_cat_value = "Løsninger"
            f2_cat_value = "For hjem"
        elseif language == "fi_utf" then
            f1_cat_value = "Ratkaisut"
            f2_cat_value = "Kotiin"
        elseif language == "ru_utf" then
            f1_cat_value = "Решения"
            f2_cat_value = "Для дома"
        elseif language == "bg_utf" then
            f1_cat_value = "Решения"
            f2_cat_value = "За дома"
        elseif language == "lt_utf" then
            f1_cat_value = "Sprendimai"
            f2_cat_value = " Namams"
        elseif language == "hr_utf" then
            f1_cat_value = "Rješenja"
            f2_cat_value = "Rješenja za dom"
        elseif language == "lv_utf" then
            f1_cat_value = "Risinājumi"
            f2_cat_value = "Mājai"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Soluções"
            f2_cat_value = "Para casa"
        elseif language == "fr_utf" then
            f1_cat_value = "Solutions"
            f2_cat_value = "Pour les particuliers"
        elseif language == "hu_utf" then
            f1_cat_value = "Megoldások"
            f2_cat_value = "Lakossági megoldások"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "解决方案"
            f2_cat_value = "住宅用"
        elseif language == "uk_utf" then
            f1_cat_value = "Рішення"
            f2_cat_value = "Для дому"
        elseif language == "sk_utf" then
            f1_cat_value = "Riešenia"
            f2_cat_value = "Pre váš Domov"
        elseif language == "sl_utf" then
            f1_cat_value = "Rešitve"
            f2_cat_value = "Rešitve za dom"
        elseif language == "id_utf" then
            f1_cat_value = "Solusi"
            f2_cat_value = "Untuk Rumah"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Soluciones"
            f2_cat_value = "Para el hogar"
        elseif language == "sr_utf" then
            f1_cat_value = "Rešenja"
            f2_cat_value = "Za Vaš dom"
        elseif language == "sv_utf" then
            f1_cat_value = "Lösningar"
            f2_cat_value = "För privatpersoner"
        elseif language == "ko_utf" then
            f1_cat_value = "솔루션"
            f2_cat_value = "가정용솔루션"
        elseif language == "el_utf" then
            f1_cat_value = "Λύσεις"
            f2_cat_value = "Για το σπίτι"
        elseif language == "en_utf" then
            f1_cat_value = "Solutions"
            f2_cat_value = "For Home"
        elseif language == "it_utf" then
            f1_cat_value = "Soluzioni"
            f2_cat_value = "Soluzioni per la casa"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Soluciones"
            f2_cat_value = "Para el hogar"
        elseif language == "et_utf" then
            f1_cat_value = "Lahendused"
            f2_cat_value = "Kodu jaoks"
        elseif language == "cs_utf" then
            f1_cat_value = "Řešení"
            f2_cat_value = "Pro váš domov"
        elseif language == "ar_utf" then
            f1_cat_value = "الحلول"
            f2_cat_value = "من أجل الوطن"
        elseif language == "vi_utf" then
            f1_cat_value = "Giải pháp"
            f2_cat_value = "Cho gia đình"
        elseif language == "th_utf" then
            f1_cat_value = "โซลูชั่น"
            f2_cat_value = "สำหรับบ้าน"
        elseif language == "ja_utf" then
            f1_cat_value = "ソリューション"
            f2_cat_value = "家庭用"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "解决方案"
            f2_cat_value = "家居用途"
        elseif language == "pl_utf" then
            f1_cat_value = "Rozwiązania"
            f2_cat_value = "Rozwiązania dla domu"
        elseif language == "da_utf" then
            f1_cat_value = "Løsninger"
            f2_cat_value = "til hjemmet"
        elseif language == "he_utf" then
            f1_cat_value = "פתרונות"
            f2_cat_value = "לבית"
        elseif language == "ro_utf" then
            f1_cat_value = "Solutii"
            f2_cat_value = "Pentru acasa"
        elseif language == "nl_utf" then
            f1_cat_value = "Oplossingen"
            f2_cat_value = "Voor woningen"
        elseif language == "tr_utf" then
            f1_cat_value = "Çözümler"
            f2_cat_value = "Ev için"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Soluções"
            f2_cat_value = "Para casa"
        end
    elseif content_source == "MARKETO" then
        if language == "de_utf" then
            f1_cat_value = "Lösungen"
            f2_cat_value = "Für Ihr Business"
        elseif language == "no_utf" then
            f1_cat_value = "Løsninger"
            f2_cat_value = "For bedrift"
        elseif language == "fi_utf" then
            f1_cat_value = "Ratkaisut"
            f2_cat_value = "Yritykselle"
        elseif language == "ru_utf" then
            f1_cat_value = "Решения"
            f2_cat_value = "Для бизнеса"
        elseif language == "bg_utf" then
            f1_cat_value = "Решения"
            f2_cat_value = "За бизнеса"
        elseif language == "lt_utf" then
            f1_cat_value = "Sprendimai"
            f2_cat_value = "Verslui"
        elseif language == "hr_utf" then
            f1_cat_value = "Rješenja"
            f2_cat_value = "Rješenja za poslovanje"
        elseif language == "lv_utf" then
            f1_cat_value = "Risinājumi"
            f2_cat_value = "Biznesam"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Soluções"
            f2_cat_value = "Para o negócio"
        elseif language == "fr_utf" then
            f1_cat_value = "Solutions"
            f2_cat_value = "Pour les professionnels"
        elseif language == "hu_utf" then
            f1_cat_value = "Megoldások"
            f2_cat_value = "Üzleti megoldások"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "解决方案"
            f2_cat_value = "營業用"
        elseif language == "uk_utf" then
            f1_cat_value = "Рішення"
            f2_cat_value = "Для бізнесу"
        elseif language == "sk_utf" then
            f1_cat_value = "Riešenia"
            f2_cat_value = "Pre Podnikateľov"
        elseif language == "sl_utf" then
            f1_cat_value = "Rešitve"
            f2_cat_value = "Rešitve za podjetja"
        elseif language == "id_utf" then
            f1_cat_value = "Solusi"
            f2_cat_value = "Untuk bisnis"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Soluciones"
            f2_cat_value = "Para empresas"
        elseif language == "sr_utf" then
            f1_cat_value = "Rešenja"
            f2_cat_value = "Za Vaš biznis"
        elseif language == "sv_utf" then
            f1_cat_value = "Lösningar"
            f2_cat_value = "För företag"
        elseif language == "ko_utf" then
            f1_cat_value = "솔루션"
            f2_cat_value = "파트너 및 기업용솔루션"
        elseif language == "el_utf" then
            f1_cat_value = "Λύσεις"
            f2_cat_value = "Για την επιχείρηση"
        elseif language == "en_utf" then
            f1_cat_value = "Solutions"
            f2_cat_value = "For Business"
        elseif language == "it_utf" then
            f1_cat_value = "Soluzioni"
            f2_cat_value = "Soluzioni per il business"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Soluciones"
            f2_cat_value = "Para empresas"
        elseif language == "et_utf" then
            f1_cat_value = "Lahendused"
            f2_cat_value = "Ettevõtetele"
        elseif language == "cs_utf" then
            f1_cat_value = "Řešení"
            f2_cat_value = "Pro vaše podnikání"
        elseif language == "ar_utf" then
            f1_cat_value = "الحلول"
            f2_cat_value = "للعمل"
        elseif language == "vi_utf" then
            f1_cat_value = "Giải pháp"
            f2_cat_value = "Dành cho Doanh nghiệp"
        elseif language == "th_utf" then
            f1_cat_value = "โซลูชั่น"
            f2_cat_value = "สำหรับธุรกิจ"
        elseif language == "ja_utf" then
            f1_cat_value = "ソリューション"
            f2_cat_value = "事業用"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "解决方案"
            f2_cat_value = "商业用途"
        elseif language == "pl_utf" then
            f1_cat_value = "Rozwiązania"
            f2_cat_value = "Dla biznesu"
        elseif language == "da_utf" then
            f1_cat_value = "Løsninger"
            f2_cat_value = "Til brancher"
        elseif language == "he_utf" then
            f1_cat_value = "פתרונות"
            f2_cat_value = "לעסקים"
        elseif language == "ro_utf" then
            f1_cat_value = "Solutii"
            f2_cat_value = "Pentru afaceri"
        elseif language == "nl_utf" then
            f1_cat_value = "Oplossingen"
            f2_cat_value = "Voor bedrijven"
        elseif language == "tr_utf" then
            f1_cat_value = "Çözümler"
            f2_cat_value = "İş için"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Soluções"
            f2_cat_value = "Para o negócio"
        end
    elseif content_source == "ABOUTUS_SDLPAGE_LEGAL" then
        if language == "de_utf" then
            f1_cat_value = "Über uns"
            f2_cat_value = "Unternehmens-Übersicht"
        elseif language == "no_utf" then
            f1_cat_value = "Selskap"
            f2_cat_value = "Om Schneider Electric"
        elseif language == "fi_utf" then
            f1_cat_value = "Yritys"
            f2_cat_value = "Yrityskuvaus"
        elseif language == "ru_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "Сведения о компании"
        elseif language == "bg_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "За компанията"
        elseif language == "lt_utf" then
            f1_cat_value = "Mūsų Įmonė"
            f2_cat_value = " Įmonės apžvalga"
        elseif language == "hr_utf" then
            f1_cat_value = "Poduzeće"
            f2_cat_value = "O Schneider Electric"
        elseif language == "lv_utf" then
            f1_cat_value = "Uzņēmums"
            f2_cat_value = "Kompānijas pārskats"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Resenha da Companhia"
        elseif language == "fr_utf" then
            f1_cat_value = "Société"
            f2_cat_value = "Présentation de la société"
        elseif language == "hu_utf" then
            f1_cat_value = "Céges információk"
            f2_cat_value = "Cégünkről"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "公司"
            f2_cat_value = "公司概況"
        elseif language == "uk_utf" then
            f1_cat_value = "Компанія"
            f2_cat_value = "Загальна інформація про компанію"
        elseif language == "sk_utf" then
            f1_cat_value = "Spoločnosť"
            f2_cat_value = "O spoločnosti"
        elseif language == "sl_utf" then
            f1_cat_value = "Podjetje"
            f2_cat_value = "O Schneider Electric"
        elseif language == "id_utf" then
            f1_cat_value = "Perusahaan"
            f2_cat_value = "Ikhtisar Perusahaan"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Visión general de la empresa"
        elseif language == "sr_utf" then
            f1_cat_value = "Kompanija"
            f2_cat_value = "O kompaniji"
        elseif language == "sv_utf" then
            f1_cat_value = "Företaget"
            f2_cat_value = "Företagsöversikt"
        elseif language == "ko_utf" then
            f1_cat_value = "회사"
            f2_cat_value = "슈나이더일렉트릭 관련"
        elseif language == "el_utf" then
            f1_cat_value = "Εταιρεία"
            f2_cat_value = "Επισκόπηση της εταιρείας"
        elseif language == "en_utf" then
            f1_cat_value = "Company"
            f2_cat_value = "Company Overview"
        elseif language == "it_utf" then
            f1_cat_value = "Società"
            f2_cat_value = "Informazioni aziendali"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Visión general de la empresa"
        elseif language == "et_utf" then
            f1_cat_value = "Ettevõte"
            f2_cat_value = "Ettevõtte ülevaade"
        elseif language == "cs_utf" then
            f1_cat_value = "Společnost"
            f2_cat_value = "O společnosti"
        elseif language == "ar_utf" then
            f1_cat_value = "الشركة"
            f2_cat_value = "نبذة عن الشركة"
        elseif language == "vi_utf" then
            f1_cat_value = "Công ty"
            f2_cat_value = "Tổng quan công ty"
        elseif language == "th_utf" then
            f1_cat_value = "บริษัท"
            f2_cat_value = "ภาพรวมบริษัท"
        elseif language == "ja_utf" then
            f1_cat_value = "会社名"
            f2_cat_value = "会社概要"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "公司"
            f2_cat_value = "集团概览"
        elseif language == "pl_utf" then
            f1_cat_value = "Firma"
            f2_cat_value = "O firmie"
        elseif language == "da_utf" then
            f1_cat_value = "Virksomhed"
            f2_cat_value = "Virksomhedsprofil "
        elseif language == "he_utf" then
            f1_cat_value = "חברה"
            f2_cat_value = "סקירת חברה"
        elseif language == "ro_utf" then
            f1_cat_value = "Companie"
            f2_cat_value = "Prezentarea generală a companiei"
        elseif language == "nl_utf" then
            f1_cat_value = "Bedrijf"
            f2_cat_value = "Over ons"
        elseif language == "tr_utf" then
            f1_cat_value = "Şirket"
            f2_cat_value = "Şirket Genel Bakışı"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Visão Geral da Empresa"
        end
    elseif content_source == "BLOG" then
        if language == "de_utf" then
            f1_cat_value = "Über uns"
            f2_cat_value = "Blog"
        elseif language == "no_utf" then
            f1_cat_value = "Selskap"
            f2_cat_value = "Blogg"
        elseif language == "fi_utf" then
            f1_cat_value = "Yritys"
            f2_cat_value = "Blogi"
        elseif language == "ru_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "Блог"
        elseif language == "bg_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "Блог"
        elseif language == "lt_utf" then
            f1_cat_value = "Mūsų Įmonė"
            f2_cat_value = "Tinklaraštis"
        elseif language == "hr_utf" then
            f1_cat_value = "Poduzeće"
            f2_cat_value = "Blog"
        elseif language == "lv_utf" then
            f1_cat_value = "Uzņēmums"
            f2_cat_value = "Emuārs"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Blog"
        elseif language == "fr_utf" then
            f1_cat_value = "Société"
            f2_cat_value = "Blog"
        elseif language == "hu_utf" then
            f1_cat_value = "Céges információk"
            f2_cat_value = "Blog"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "公司"
            f2_cat_value = "部落格"
        elseif language == "uk_utf" then
            f1_cat_value = "Компанія"
            f2_cat_value = "Блог"
        elseif language == "sk_utf" then
            f1_cat_value = "Spoločnosť"
            f2_cat_value = "Blog"
        elseif language == "sl_utf" then
            f1_cat_value = "Podjetje"
            f2_cat_value = "Blog"
        elseif language == "id_utf" then
            f1_cat_value = "Perusahaan"
            f2_cat_value = "Blog"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Blog"
        elseif language == "sr_utf" then
            f1_cat_value = "Kompanija"
            f2_cat_value = "Blog"
        elseif language == "sv_utf" then
            f1_cat_value = "Företaget"
            f2_cat_value = "Blogg"
        elseif language == "ko_utf" then
            f1_cat_value = "회사"
            f2_cat_value = "블로그"
        elseif language == "el_utf" then
            f1_cat_value = "Εταιρεία"
            f2_cat_value = "Blog"
        elseif language == "en_utf" then
            f1_cat_value = "Company"
            f2_cat_value = "Blog"
        elseif language == "it_utf" then
            f1_cat_value = "Società"
            f2_cat_value = "Blog"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Blog"
        elseif language == "et_utf" then
            f1_cat_value = "Ettevõte"
            f2_cat_value = "Blogi"
        elseif language == "cs_utf" then
            f1_cat_value = "Společnost"
            f2_cat_value = "Blog"
        elseif language == "ar_utf" then
            f1_cat_value = "الشركة"
            f2_cat_value = "مدونة"
        elseif language == "vi_utf" then
            f1_cat_value = "Công ty"
            f2_cat_value = "Blog"
        elseif language == "th_utf" then
            f1_cat_value = "บริษัท"
            f2_cat_value = "บล็อก"
        elseif language == "ja_utf" then
            f1_cat_value = "会社名"
            f2_cat_value = "ブログ"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "公司"
            f2_cat_value = "博客"
        elseif language == "pl_utf" then
            f1_cat_value = "Firma"
            f2_cat_value = "Blog"
        elseif language == "da_utf" then
            f1_cat_value = "Virksomhed"
            f2_cat_value = "Blog"
        elseif language == "he_utf" then
            f1_cat_value = "חברה"
            f2_cat_value = "בלוג"
        elseif language == "ro_utf" then
            f1_cat_value = "Companie"
            f2_cat_value = "Blog"
        elseif language == "nl_utf" then
            f1_cat_value = "Bedrijf"
            f2_cat_value = "Blog"
        elseif language == "tr_utf" then
            f1_cat_value = "Şirket"
            f2_cat_value = "Blog"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Blog"
        end
    elseif content_source == "TRACEPARTS" then
        if language == "de_utf" then
            f1_cat_value = "Dokumente"
            f2_cat_value = "Technische Informationen"
            f3_cat_value = "CAD"
        elseif language == "no_utf" then
            f1_cat_value = "Dokumenter"
            f2_cat_value = "Teknisk informasjon"
            f3_cat_value = "CAD"
        elseif language == "fi_utf" then
            f1_cat_value = "Asiakirjat"
            f2_cat_value = "Tekniset tiedot"
            f3_cat_value = "CAD"
        elseif language == "ru_utf" then
            f1_cat_value = "Документация"
            f2_cat_value = "Техническая информация"
            f3_cat_value = "CAD"
        elseif language == "bg_utf" then
            f1_cat_value = "Документи"
            f2_cat_value = "Техническа информация"
            f3_cat_value = "CAD"
        elseif language == "lt_utf" then
            f1_cat_value = "Dokumentai"
            f2_cat_value = "Techninė informacija "
            f3_cat_value = "CAD"
        elseif language == "hr_utf" then
            f1_cat_value = "Dokumenti"
            f2_cat_value = "Tehničke informacije"
            f3_cat_value = "CAD"
        elseif language == "lv_utf" then
            f1_cat_value = "Dokumenti"
            f2_cat_value = "Tehniskā informācija"
            f3_cat_value = "CAD"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Documentos"
            f2_cat_value = "Informações técnicas"
            f3_cat_value = "CAD"
        elseif language == "fr_utf" then
            f1_cat_value = "Documents"
            f2_cat_value = "Informations techniques"
            f3_cat_value = "CAD"
        elseif language == "hu_utf" then
            f1_cat_value = "Dokumentumok"
            f2_cat_value = "Műszaki információk"
            f3_cat_value = "CAD"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "文件"
            f2_cat_value = "技術資訊 "
            f3_cat_value = "CAD"
        elseif language == "uk_utf" then
            f1_cat_value = "Документи"
            f2_cat_value = "Технічна інформація"
            f3_cat_value = "CAD"
        elseif language == "sk_utf" then
            f1_cat_value = "Dokumenty"
            f2_cat_value = "Technické informácie "
            f3_cat_value = "CAD"
        elseif language == "sl_utf" then
            f1_cat_value = "Dokumenti"
            f2_cat_value = "Tehnične informacije"
            f3_cat_value = "CAD"
        elseif language == "id_utf" then
            f1_cat_value = "Dokumen"
            f2_cat_value = "Informasi Teknis"
            f3_cat_value = "CAD"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Documentos"
            f2_cat_value = "Información técnica"
            f3_cat_value = "CAD"
        elseif language == "sr_utf" then
            f1_cat_value = "Dokumenti"
            f2_cat_value = "Tehničke informacije"
            f3_cat_value = "CAD"
        elseif language == "sv_utf" then
            f1_cat_value = "Dokument"
            f2_cat_value = "Teknisk information "
            f3_cat_value = "CAD"
        elseif language == "ko_utf" then
            f1_cat_value = "문서"
            f2_cat_value = "기술 정보"
            f3_cat_value = "CAD"
        elseif language == "el_utf" then
            f1_cat_value = "Έγγραφα"
            f2_cat_value = "Τεχνικές πληροφορίες"
            f3_cat_value = "CAD"
        elseif language == "en_utf" then
            f1_cat_value = "Documents"
            f2_cat_value = "Technical Information"
            f3_cat_value = "CAD"
        elseif language == "it_utf" then
            f1_cat_value = "Documenti"
            f2_cat_value = "Informazioni tecniche"
            f3_cat_value = "CAD"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Documentos"
            f2_cat_value = "Información técnica"
            f3_cat_value = "CAD"
        elseif language == "et_utf" then
            f1_cat_value = "Dokumendid"
            f2_cat_value = "Tehniline teave"
            f3_cat_value = "CAD"
        elseif language == "cs_utf" then
            f1_cat_value = "Dokumenty"
            f2_cat_value = "Technické informace"
            f3_cat_value = "CAD"
        elseif language == "ar_utf" then
            f1_cat_value = "المستندات"
            f2_cat_value = "معلومات تقنية"
            f3_cat_value = "CAD"
        elseif language == "vi_utf" then
            f1_cat_value = "Tài liệu"
            f2_cat_value = "Hỗ trợ & Câu hỏi thường gặp"
            f3_cat_value = "CAD"
        elseif language == "th_utf" then
            f1_cat_value = "เอกสาร"
            f2_cat_value = "ข้อมูลทางเทคนิค"
            f3_cat_value = "CAD"
        elseif language == "ja_utf" then
            f1_cat_value = "ドキュメント"
            f2_cat_value = "技術情報"
            f3_cat_value = "CAD"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "文档"
            f2_cat_value = "技术信息"
            f3_cat_value = "CAD"
        elseif language == "pl_utf" then
            f1_cat_value = "Dokumenty"
            f2_cat_value = "Informacje techniczne"
            f3_cat_value = "CAD"
        elseif language == "da_utf" then
            f1_cat_value = "Dokumenter"
            f2_cat_value = "Tekniske oplysninger"
            f3_cat_value = "CAD"
        elseif language == "he_utf" then
            f1_cat_value = "מסמכים"
            f2_cat_value = "מידע טכני"
            f3_cat_value = "CAD"
        elseif language == "ro_utf" then
            f1_cat_value = "Documente"
            f2_cat_value = "Informatii tehnice"
            f3_cat_value = "CAD"
        elseif language == "nl_utf" then
            f1_cat_value = "Documenten"
            f2_cat_value = "Technische informatie"
            f3_cat_value = "CAD"
        elseif language == "tr_utf" then
            f1_cat_value = "Belgeler"
            f2_cat_value = "Teknik Bilgiler"
            f3_cat_value = "CAD"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Documentos"
            f2_cat_value = "Informações técnicas"
            f3_cat_value = "CAD"
        end
    elseif content_source == "ABOUTUS_SDLPAGE_COMPANY_PROFILE" then
        if language == "de_utf" then
            f1_cat_value = "Über uns"
            f2_cat_value = "Unternehmens-Übersicht"
        elseif language == "no_utf" then
            f1_cat_value = "Selskap"
            f2_cat_value = "Om Schneider Electric"
        elseif language == "fi_utf" then
            f1_cat_value = "Yritys"
            f2_cat_value = "Yrityskuvaus"
        elseif language == "ru_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "Сведения о компании"
        elseif language == "bg_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "За компанията"
        elseif language == "lt_utf" then
            f1_cat_value = "Mūsų Įmonė"
            f2_cat_value = " Įmonės apžvalga"
        elseif language == "hr_utf" then
            f1_cat_value = "Poduzeće"
            f2_cat_value = "O Schneider Electric"
        elseif language == "lv_utf" then
            f1_cat_value = "Uzņēmums"
            f2_cat_value = "Kompānijas pārskats"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Resenha da Companhia"
        elseif language == "fr_utf" then
            f1_cat_value = "Société"
            f2_cat_value = "Présentation de la société"
        elseif language == "hu_utf" then
            f1_cat_value = "Céges információk"
            f2_cat_value = "Cégünkről"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "公司"
            f2_cat_value = "公司概況"
        elseif language == "uk_utf" then
            f1_cat_value = "Компанія"
            f2_cat_value = "Загальна інформація про компанію"
        elseif language == "sk_utf" then
            f1_cat_value = "Spoločnosť"
            f2_cat_value = "O spoločnosti"
        elseif language == "sl_utf" then
            f1_cat_value = "Podjetje"
            f2_cat_value = "O Schneider Electric"
        elseif language == "id_utf" then
            f1_cat_value = "Perusahaan"
            f2_cat_value = "Ikhtisar Perusahaan"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Visión general de la empresa"
        elseif language == "sr_utf" then
            f1_cat_value = "Kompanija"
            f2_cat_value = "O kompaniji"
        elseif language == "sv_utf" then
            f1_cat_value = "Företaget"
            f2_cat_value = "Företagsöversikt"
        elseif language == "ko_utf" then
            f1_cat_value = "회사"
            f2_cat_value = "슈나이더일렉트릭 관련"
        elseif language == "el_utf" then
            f1_cat_value = "Εταιρεία"
            f2_cat_value = "Επισκόπηση της εταιρείας"
        elseif language == "en_utf" then
            f1_cat_value = "Company"
            f2_cat_value = "Company Overview"
        elseif language == "it_utf" then
            f1_cat_value = "Società"
            f2_cat_value = "Informazioni aziendali"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Visión general de la empresa"
        elseif language == "et_utf" then
            f1_cat_value = "Ettevõte"
            f2_cat_value = "Ettevõtte ülevaade"
        elseif language == "cs_utf" then
            f1_cat_value = "Společnost"
            f2_cat_value = "O společnosti"
        elseif language == "ar_utf" then
            f1_cat_value = "الشركة"
            f2_cat_value = "نبذة عن الشركة"
        elseif language == "vi_utf" then
            f1_cat_value = "Công ty"
            f2_cat_value = "Tổng quan công ty"
        elseif language == "th_utf" then
            f1_cat_value = "บริษัท"
            f2_cat_value = "ภาพรวมบริษัท"
        elseif language == "ja_utf" then
            f1_cat_value = "会社名"
            f2_cat_value = "会社概要"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "公司"
            f2_cat_value = "集团概览"
        elseif language == "pl_utf" then
            f1_cat_value = "Firma"
            f2_cat_value = "O firmie"
        elseif language == "da_utf" then
            f1_cat_value = "Virksomhed"
            f2_cat_value = "Virksomhedsprofil "
        elseif language == "he_utf" then
            f1_cat_value = "חברה"
            f2_cat_value = "סקירת חברה"
        elseif language == "ro_utf" then
            f1_cat_value = "Companie"
            f2_cat_value = "Prezentarea generală a companiei"
        elseif language == "nl_utf" then
            f1_cat_value = "Bedrijf"
            f2_cat_value = "Over ons"
        elseif language == "tr_utf" then
            f1_cat_value = "Şirket"
            f2_cat_value = "Şirket Genel Bakışı"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Visão Geral da Empresa"
        end
    elseif content_source == "SOLN_SDLPAGE" then
        if language == "de_utf" then
            f1_cat_value = "Lösungen"
            f2_cat_value = "Für Ihr Business"
        elseif language == "no_utf" then
            f1_cat_value = "Løsninger"
            f2_cat_value = "For bedrift"
        elseif language == "fi_utf" then
            f1_cat_value = "Ratkaisut"
            f2_cat_value = "Yritykselle"
        elseif language == "ru_utf" then
            f1_cat_value = "Решения"
            f2_cat_value = "Для бизнеса"
        elseif language == "bg_utf" then
            f1_cat_value = "Решения"
            f2_cat_value = "За бизнеса"
        elseif language == "lt_utf" then
            f1_cat_value = "Sprendimai"
            f2_cat_value = "Verslui"
        elseif language == "hr_utf" then
            f1_cat_value = "Rješenja"
            f2_cat_value = "Rješenja za poslovanje"
        elseif language == "lv_utf" then
            f1_cat_value = "Risinājumi"
            f2_cat_value = "Biznesam"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Soluções"
            f2_cat_value = "Para o negócio"
        elseif language == "fr_utf" then
            f1_cat_value = "Solutions"
            f2_cat_value = "Pour les professionnels"
        elseif language == "hu_utf" then
            f1_cat_value = "Megoldások"
            f2_cat_value = "Üzleti megoldások"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "解决方案"
            f2_cat_value = "營業用"
        elseif language == "uk_utf" then
            f1_cat_value = "Рішення"
            f2_cat_value = "Для бізнесу"
        elseif language == "sk_utf" then
            f1_cat_value = "Riešenia"
            f2_cat_value = "Pre Podnikateľov"
        elseif language == "sl_utf" then
            f1_cat_value = "Rešitve"
            f2_cat_value = "Rešitve za podjetja"
        elseif language == "id_utf" then
            f1_cat_value = "Solusi"
            f2_cat_value = "Untuk bisnis"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Soluciones"
            f2_cat_value = "Para empresas"
        elseif language == "sr_utf" then
            f1_cat_value = "Rešenja"
            f2_cat_value = "Za Vaš biznis"
        elseif language == "sv_utf" then
            f1_cat_value = "Lösningar"
            f2_cat_value = "För företag"
        elseif language == "ko_utf" then
            f1_cat_value = "솔루션"
            f2_cat_value = "파트너 및 기업용솔루션"
        elseif language == "el_utf" then
            f1_cat_value = "Λύσεις"
            f2_cat_value = "Για την επιχείρηση"
        elseif language == "en_utf" then
            f1_cat_value = "Solutions"
            f2_cat_value = "For Business"
        elseif language == "it_utf" then
            f1_cat_value = "Soluzioni"
            f2_cat_value = "Soluzioni per il business"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Soluciones"
            f2_cat_value = "Para empresas"
        elseif language == "et_utf" then
            f1_cat_value = "Lahendused"
            f2_cat_value = "Ettevõtetele"
        elseif language == "cs_utf" then
            f1_cat_value = "Řešení"
            f2_cat_value = "Pro vaše podnikání"
        elseif language == "ar_utf" then
            f1_cat_value = "الحلول"
            f2_cat_value = "للعمل"
        elseif language == "vi_utf" then
            f1_cat_value = "Giải pháp"
            f2_cat_value = "Dành cho Doanh nghiệp"
        elseif language == "th_utf" then
            f1_cat_value = "โซลูชั่น"
            f2_cat_value = "สำหรับธุรกิจ"
        elseif language == "ja_utf" then
            f1_cat_value = "ソリューション"
            f2_cat_value = "事業用"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "解决方案"
            f2_cat_value = "商业用途"
        elseif language == "pl_utf" then
            f1_cat_value = "Rozwiązania"
            f2_cat_value = "Dla biznesu"
        elseif language == "da_utf" then
            f1_cat_value = "Løsninger"
            f2_cat_value = "Til brancher"
        elseif language == "he_utf" then
            f1_cat_value = "פתרונות"
            f2_cat_value = "לעסקים"
        elseif language == "ro_utf" then
            f1_cat_value = "Solutii"
            f2_cat_value = "Pentru afaceri"
        elseif language == "nl_utf" then
            f1_cat_value = "Oplossingen"
            f2_cat_value = "Voor bedrijven"
        elseif language == "tr_utf" then
            f1_cat_value = "Çözümler"
            f2_cat_value = "İş için"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Soluções"
            f2_cat_value = "Para o negócio"
        end
    elseif content_source == "PRD_SDLPAGE" then
        if language == "de_utf" then
            f1_cat_value = "Produkte"
            f2_cat_value = "Produktreihe"
        elseif language == "no_utf" then
            f1_cat_value = "Produkter"
            f2_cat_value = "Produktspekter"
        elseif language == "fi_utf" then
            f1_cat_value = "Tuotteet"
            f2_cat_value = "Tuoteperhe"
        elseif language == "ru_utf" then
            f1_cat_value = "Продукты"
            f2_cat_value = "Серия продукта"
        elseif language == "bg_utf" then
            f1_cat_value = "Продукти"
            f2_cat_value = "Продуктова гама"
        elseif language == "lt_utf" then
            f1_cat_value = "Gaminiai"
            f2_cat_value = "Produktų serija"
        elseif language == "hr_utf" then
            f1_cat_value = "Proizvodi"
            f2_cat_value = "Paleta proizvoda"
        elseif language == "lv_utf" then
            f1_cat_value = "Produkti"
            f2_cat_value = "Produkta klāsts"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Produtos"
            f2_cat_value = "Família de Produto"
        elseif language == "fr_utf" then
            f1_cat_value = "Produits"
            f2_cat_value = "Gamme de produits"
        elseif language == "hu_utf" then
            f1_cat_value = "Termékek"
            f2_cat_value = "Termékválaszték"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "產品"
            f2_cat_value = "產品系列"
        elseif language == "uk_utf" then
            f1_cat_value = "Продукти"
            f2_cat_value = "Асортимент товарів"
        elseif language == "sk_utf" then
            f1_cat_value = "Produkty"
            f2_cat_value = "Produktový Rad"
        elseif language == "sl_utf" then
            f1_cat_value = "izdelkov"
            f2_cat_value = "Paleta izdelkov"
        elseif language == "id_utf" then
            f1_cat_value = "Produk"
            f2_cat_value = "Daftar Produk"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Productos"
            f2_cat_value = "Rango de producto"
        elseif language == "sr_utf" then
            f1_cat_value = "Proizvodi"
            f2_cat_value = "Paleta proizvoda"
        elseif language == "sv_utf" then
            f1_cat_value = "Produkter"
            f2_cat_value = "Produktfamilj"
        elseif language == "ko_utf" then
            f1_cat_value = "제품"
            f2_cat_value = "제품군"
        elseif language == "el_utf" then
            f1_cat_value = "Προϊόντα"
            f2_cat_value = "Σειρά προϊόντων"
        elseif language == "en_utf" then
            f1_cat_value = "Products"
            f2_cat_value = "Product Range"
        elseif language == "it_utf" then
            f1_cat_value = "Prodotti"
            f2_cat_value = "Range prodotto"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Productos"
            f2_cat_value = "Rango de producto"
        elseif language == "et_utf" then
            f1_cat_value = "Tooted"
            f2_cat_value = "Tooteseeria"
        elseif language == "cs_utf" then
            f1_cat_value = "Produkty"
            f2_cat_value = "Produktová řada"
        elseif language == "ar_utf" then
            f1_cat_value = "المنتجات"
            f2_cat_value = "مجموعة المنتجات"
        elseif language == "vi_utf" then
            f1_cat_value = "Sản phẩm"
            f2_cat_value = "Dòng Sản phẩm"
        elseif language == "th_utf" then
            f1_cat_value = "ผลิตภัณฑ์"
            f2_cat_value = "กลุ่มผลิตภัณฑ์"
        elseif language == "ja_utf" then
            f1_cat_value = "製品"
            f2_cat_value = "製品ライン"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "产品"
            f2_cat_value = "产品系列"
        elseif language == "pl_utf" then
            f1_cat_value = "Produkty"
            f2_cat_value = "Gama produktu"
        elseif language == "da_utf" then
            f1_cat_value = "Produkter"
            f2_cat_value = "Produktsortiment"
        elseif language == "he_utf" then
            f1_cat_value = "מוצרים"
            f2_cat_value = "טווח מוצר"
        elseif language == "ro_utf" then
            f1_cat_value = "Produse"
            f2_cat_value = "Gama de produse"
        elseif language == "nl_utf" then
            f1_cat_value = "Producten"
            f2_cat_value = "Productserie"
        elseif language == "tr_utf" then
            f1_cat_value = "Ürünler"
            f2_cat_value = "Ürün Gurubu"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Produtos"
            f2_cat_value = "Gama de Produtos"
        end
    elseif content_source == "ABOUTUS_SDLPAGE_CAREERS" then
        if language == "de_utf" then
            f1_cat_value = "Über uns"
            f2_cat_value = "Karriere"
        elseif language == "no_utf" then
            f1_cat_value = "Selskap"
            f2_cat_value = "Karrierer"
        elseif language == "fi_utf" then
            f1_cat_value = "Yritys"
            f2_cat_value = "Uramahdollisuudet"
        elseif language == "ru_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "Карьера"
        elseif language == "bg_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "Кариери"
        elseif language == "lt_utf" then
            f1_cat_value = "Mūsų Įmonė"
            f2_cat_value = "Karjera"
        elseif language == "hr_utf" then
            f1_cat_value = "Poduzeće"
            f2_cat_value = "Karijere"
        elseif language == "lv_utf" then
            f1_cat_value = "Uzņēmums"
            f2_cat_value = "Karjera"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Carreiras"
        elseif language == "fr_utf" then
            f1_cat_value = "Société"
            f2_cat_value = "Carrières"
        elseif language == "hu_utf" then
            f1_cat_value = "Céges információk"
            f2_cat_value = "Karrier"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "公司"
            f2_cat_value = "員工招募"
        elseif language == "uk_utf" then
            f1_cat_value = "Компанія"
            f2_cat_value = "Вакансії"
        elseif language == "sk_utf" then
            f1_cat_value = "Spoločnosť"
            f2_cat_value = "Kariéra"
        elseif language == "sl_utf" then
            f1_cat_value = "Podjetje"
            f2_cat_value = "Kariera"
        elseif language == "id_utf" then
            f1_cat_value = "Perusahaan"
            f2_cat_value = "Karier"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Carreras"
        elseif language == "sr_utf" then
            f1_cat_value = "Kompanija"
            f2_cat_value = "Karijera"
        elseif language == "sv_utf" then
            f1_cat_value = "Företaget"
            f2_cat_value = "Karriär"
        elseif language == "ko_utf" then
            f1_cat_value = "회사"
            f2_cat_value = "채용"
        elseif language == "el_utf" then
            f1_cat_value = "Εταιρεία"
            f2_cat_value = "Σταδιοδρομίες"
        elseif language == "en_utf" then
            f1_cat_value = "Company"
            f2_cat_value = "Careers"
        elseif language == "it_utf" then
            f1_cat_value = "Società"
            f2_cat_value = "Carriere"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Carreras"
        elseif language == "et_utf" then
            f1_cat_value = "Ettevõte"
            f2_cat_value = "Karjäärivõimalused"
        elseif language == "cs_utf" then
            f1_cat_value = "Společnost"
            f2_cat_value = "Kariéra"
        elseif language == "ar_utf" then
            f1_cat_value = "الشركة"
            f2_cat_value = "الوظائف"
        elseif language == "vi_utf" then
            f1_cat_value = "Công ty"
            f2_cat_value = "Cơ hội nghề nghiệp"
        elseif language == "th_utf" then
            f1_cat_value = "บริษัท"
            f2_cat_value = "ตำแหน่งงาน"
        elseif language == "ja_utf" then
            f1_cat_value = "会社名"
            f2_cat_value = "採用情報"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "公司"
            f2_cat_value = "人才招聘"
        elseif language == "pl_utf" then
            f1_cat_value = "Firma"
            f2_cat_value = "Kariera"
        elseif language == "da_utf" then
            f1_cat_value = "Virksomhed"
            f2_cat_value = "Karriere"
        elseif language == "he_utf" then
            f1_cat_value = "חברה"
            f2_cat_value = "משרות"
        elseif language == "ro_utf" then
            f1_cat_value = "Companie"
            f2_cat_value = "Cariere"
        elseif language == "nl_utf" then
            f1_cat_value = "Bedrijf"
            f2_cat_value = "Carrières"
        elseif language == "tr_utf" then
            f1_cat_value = "Şirket"
            f2_cat_value = "Kariyer"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Carreiras"
        end
    elseif content_source == "ECAT_BSL" then
        if language == "de_utf" then
            f1_cat_value = "Produkte"
            f2_cat_value = "Produkt"
        elseif language == "no_utf" then
            f1_cat_value = "Produkter"
            f2_cat_value = "Produkt"
        elseif language == "fi_utf" then
            f1_cat_value = "Tuotteet"
            f2_cat_value = "Tuote"
        elseif language == "ru_utf" then
            f1_cat_value = "Продукты"
            f2_cat_value = "Продукт"
        elseif language == "bg_utf" then
            f1_cat_value = "Продукти"
            f2_cat_value = "Продукт"
        elseif language == "lt_utf" then
            f1_cat_value = "Gaminiai"
            f2_cat_value = "Produktas"
        elseif language == "hr_utf" then
            f1_cat_value = "Proizvodi"
            f2_cat_value = "Proizvod"
        elseif language == "lv_utf" then
            f1_cat_value = "Produkti"
            f2_cat_value = "Produkts"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Produtos"
            f2_cat_value = "Produto"
        elseif language == "fr_utf" then
            f1_cat_value = "Produits"
            f2_cat_value = "Produit"
        elseif language == "hu_utf" then
            f1_cat_value = "Termékek"
            f2_cat_value = "Termék"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "產品"
            f2_cat_value = "產品"
        elseif language == "uk_utf" then
            f1_cat_value = "Продукти"
            f2_cat_value = "Товар"
        elseif language == "sk_utf" then
            f1_cat_value = "Produkty"
            f2_cat_value = "Produkt"
        elseif language == "sl_utf" then
            f1_cat_value = "izdelkov"
            f2_cat_value = "Izdelka"
        elseif language == "id_utf" then
            f1_cat_value = "Produk"
            f2_cat_value = "Produk"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Productos"
            f2_cat_value = "Producto"
        elseif language == "sr_utf" then
            f1_cat_value = "Proizvodi"
            f2_cat_value = "Proizvod"
        elseif language == "sv_utf" then
            f1_cat_value = "Produkter"
            f2_cat_value = "Produkt"
        elseif language == "ko_utf" then
            f1_cat_value = "제품"
            f2_cat_value = "제품"
        elseif language == "el_utf" then
            f1_cat_value = "Προϊόντα"
            f2_cat_value = "Προϊόν"
        elseif language == "en_utf" then
            f1_cat_value = "Products"
            f2_cat_value = "Product"
        elseif language == "it_utf" then
            f1_cat_value = "Prodotti"
            f2_cat_value = "Prodotto"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Productos"
            f2_cat_value = "Producto"
        elseif language == "et_utf" then
            f1_cat_value = "Tooted"
            f2_cat_value = "Toode"
        elseif language == "cs_utf" then
            f1_cat_value = "Produkty"
            f2_cat_value = "Produkt"
        elseif language == "ar_utf" then
            f1_cat_value = "المنتجات"
            f2_cat_value = "المنتج"
        elseif language == "vi_utf" then
            f1_cat_value = "Sản phẩm"
            f2_cat_value = "Sản phẩm"
        elseif language == "th_utf" then
            f1_cat_value = "ผลิตภัณฑ์"
            f2_cat_value = "ผลิตภัณฑ์"
        elseif language == "ja_utf" then
            f1_cat_value = "製品"
            f2_cat_value = "製品"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "产品"
            f2_cat_value = "产品"
        elseif language == "pl_utf" then
            f1_cat_value = "Produkty"
            f2_cat_value = "Produkt"
        elseif language == "da_utf" then
            f1_cat_value = "Produkter"
            f2_cat_value = "Produkt"
        elseif language == "he_utf" then
            f1_cat_value = "מוצרים"
            f2_cat_value = "מוצר"
        elseif language == "ro_utf" then
            f1_cat_value = "Produse"
            f2_cat_value = "Produs"
        elseif language == "nl_utf" then
            f1_cat_value = "Producten"
            f2_cat_value = "Product"
        elseif language == "tr_utf" then
            f1_cat_value = "Ürünler"
            f2_cat_value = "Ürün"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Produtos"
            f2_cat_value = "Produto"
        end
    elseif content_source == "NAV_PES" then
        if language == "de_utf" then
            f1_cat_value = "Produkte"
            f2_cat_value = "Produktkategorie"
        elseif language == "no_utf" then
            f1_cat_value = "Produkter"
            f2_cat_value = "Produktkategori"
        elseif language == "fi_utf" then
            f1_cat_value = "Tuotteet"
            f2_cat_value = "Tuoteluokka"
        elseif language == "ru_utf" then
            f1_cat_value = "Продукты"
            f2_cat_value = "Категория продуктов"
        elseif language == "bg_utf" then
            f1_cat_value = "Продукти"
            f2_cat_value = "Продуктова категория"
        elseif language == "lt_utf" then
            f1_cat_value = "Gaminiai"
            f2_cat_value = "Produktų kategorija"
        elseif language == "hr_utf" then
            f1_cat_value = "Proizvodi"
            f2_cat_value = "Kategorija proizvoda"
        elseif language == "lv_utf" then
            f1_cat_value = "Produkti"
            f2_cat_value = "Produkta kategorija"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Produtos"
            f2_cat_value = "Categoria de produtos"
        elseif language == "fr_utf" then
            f1_cat_value = "Produits"
            f2_cat_value = "Catégorie de produit"
        elseif language == "hu_utf" then
            f1_cat_value = "Termékek"
            f2_cat_value = "Termékkategória"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "產品"
            f2_cat_value = "產品主類別"
        elseif language == "uk_utf" then
            f1_cat_value = "Продукти"
            f2_cat_value = "Категорія товарів"
        elseif language == "sk_utf" then
            f1_cat_value = "Produkty"
            f2_cat_value = "Produktová Kategória"
        elseif language == "sl_utf" then
            f1_cat_value = "izdelkov"
            f2_cat_value = "Kategorija izdelka"
        elseif language == "id_utf" then
            f1_cat_value = "Produk"
            f2_cat_value = "Kategori Produk"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Productos"
            f2_cat_value = "Categoría de producto"
        elseif language == "sr_utf" then
            f1_cat_value = "Proizvodi"
            f2_cat_value = "Kategorija proizvoda"
        elseif language == "sv_utf" then
            f1_cat_value = "Produkter"
            f2_cat_value = "Produktkategorier"
        elseif language == "ko_utf" then
            f1_cat_value = "제품"
            f2_cat_value = "제품 카테고리"
        elseif language == "el_utf" then
            f1_cat_value = "Προϊόντα"
            f2_cat_value = "κατηγορία προιόντος"
        elseif language == "en_utf" then
            f1_cat_value = "Products"
            f2_cat_value = "Product Category"
        elseif language == "it_utf" then
            f1_cat_value = "Prodotti"
            f2_cat_value = "Categoria di prodotti"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Productos"
            f2_cat_value = "Categoría de producto"
        elseif language == "et_utf" then
            f1_cat_value = "Tooted"
            f2_cat_value = "Product Category"
        elseif language == "cs_utf" then
            f1_cat_value = "Produkty"
            f2_cat_value = "Kategorie produktů"
        elseif language == "ar_utf" then
            f1_cat_value = "المنتجات"
            f2_cat_value = "فئة المنتجات"
        elseif language == "vi_utf" then
            f1_cat_value = "Sản phẩm"
            f2_cat_value = "danh mục sản phẩm"
        elseif language == "th_utf" then
            f1_cat_value = "ผลิตภัณฑ์"
            f2_cat_value = "ประเภทสินค้า"
        elseif language == "ja_utf" then
            f1_cat_value = "製品"
            f2_cat_value = "製品のカテゴリー"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "产品"
            f2_cat_value = "产品大类目"
        elseif language == "pl_utf" then
            f1_cat_value = "Produkty"
            f2_cat_value = "Kategoria produktów"
        elseif language == "da_utf" then
            f1_cat_value = "Produkter"
            f2_cat_value = "Produktkategori"
        elseif language == "he_utf" then
            f1_cat_value = "מוצרים"
            f2_cat_value = "קטגוריית מוצר"
        elseif language == "ro_utf" then
            f1_cat_value = "Produse"
            f2_cat_value = "Categorie produs"
        elseif language == "nl_utf" then
            f1_cat_value = "Producten"
            f2_cat_value = "Productrubriek"
        elseif language == "tr_utf" then
            f1_cat_value = "Ürünler"
            f2_cat_value = "Ürün Kategorisi"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Produtos"
            f2_cat_value = "Categoria do produto"
        end
    elseif content_source == "ABOUTUS_SDLPAGE_PRESS" then
        if language == "de_utf" then
            f1_cat_value = "Über uns"
            f2_cat_value = "Pressemitteilungen"
        elseif language == "no_utf" then
            f1_cat_value = "Selskap"
            f2_cat_value = "Trykk"
        elseif language == "fi_utf" then
            f1_cat_value = "Yritys"
            f2_cat_value = "Lehdistötiedote"
        elseif language == "ru_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "Пресса"
        elseif language == "bg_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "Преса"
        elseif language == "lt_utf" then
            f1_cat_value = "Mūsų Įmonė"
            f2_cat_value = "Spauda"
        elseif language == "hr_utf" then
            f1_cat_value = "Poduzeće"
            f2_cat_value = "Press"
        elseif language == "lv_utf" then
            f1_cat_value = "Uzņēmums"
            f2_cat_value = "Prese"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Comunicado de imprensa"
        elseif language == "fr_utf" then
            f1_cat_value = "Société"
            f2_cat_value = "Presse"
        elseif language == "hu_utf" then
            f1_cat_value = "Céges információk"
            f2_cat_value = "Présgép"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "公司"
            f2_cat_value = "媒體"
        elseif language == "uk_utf" then
            f1_cat_value = "Компанія"
            f2_cat_value = "ЗМІ"
        elseif language == "sk_utf" then
            f1_cat_value = "Spoločnosť"
            f2_cat_value = "Lis"
        elseif language == "sl_utf" then
            f1_cat_value = "Podjetje"
            f2_cat_value = "Medijski kotiček"
        elseif language == "id_utf" then
            f1_cat_value = "Perusahaan"
            f2_cat_value = "Tekan"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Prensa"
        elseif language == "sr_utf" then
            f1_cat_value = "Kompanija"
            f2_cat_value = "Novosti"
        elseif language == "sv_utf" then
            f1_cat_value = "Företaget"
            f2_cat_value = "Press"
        elseif language == "ko_utf" then
            f1_cat_value = "회사"
            f2_cat_value = "보도 자료"
        elseif language == "el_utf" then
            f1_cat_value = "Εταιρεία"
            f2_cat_value = "Νέα"
        elseif language == "en_utf" then
            f1_cat_value = "Company"
            f2_cat_value = "Press"
        elseif language == "it_utf" then
            f1_cat_value = "Società"
            f2_cat_value = "Stampa"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Prensa"
        elseif language == "et_utf" then
            f1_cat_value = "Ettevõte"
            f2_cat_value = "Vajutage"
        elseif language == "cs_utf" then
            f1_cat_value = "Společnost"
            f2_cat_value = "Lis"
        elseif language == "ar_utf" then
            f1_cat_value = "الشركة"
            f2_cat_value = "الصحافة"
        elseif language == "vi_utf" then
            f1_cat_value = "Công ty"
            f2_cat_value = "Báo chí"
        elseif language == "th_utf" then
            f1_cat_value = "บริษัท"
            f2_cat_value = "ข่าวประชาสัมพันธ์"
        elseif language == "ja_utf" then
            f1_cat_value = "会社名"
            f2_cat_value = "プレスリリース"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "公司"
            f2_cat_value = "新闻"
        elseif language == "pl_utf" then
            f1_cat_value = "Firma"
            f2_cat_value = "Prasa"
        elseif language == "da_utf" then
            f1_cat_value = "Virksomhed"
            f2_cat_value = "Presse"
        elseif language == "he_utf" then
            f1_cat_value = "חברה"
            f2_cat_value = "עיתונות"
        elseif language == "ro_utf" then
            f1_cat_value = "Companie"
            f2_cat_value = "Presa"
        elseif language == "nl_utf" then
            f1_cat_value = "Bedrijf"
            f2_cat_value = "Pers"
        elseif language == "tr_utf" then
            f1_cat_value = "Şirket"
            f2_cat_value = "Basın"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Comunicado de imprensa"
        end
    elseif content_source == "SUPP_SDLPAGE" then
        if language == "de_utf" then
            f1_cat_value = "Support"
        elseif language == "no_utf" then
            f1_cat_value = "Support"
        elseif language == "fi_utf" then
            f1_cat_value = "Tuki"
        elseif language == "ru_utf" then
            f1_cat_value = "Поддержка"
        elseif language == "bg_utf" then
            f1_cat_value = "Поддръжка"
        elseif language == "lt_utf" then
            f1_cat_value = "Pagalba"
        elseif language == "hr_utf" then
            f1_cat_value = "Podrška"
        elseif language == "lv_utf" then
            f1_cat_value = "Atbalsts"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Suporte"
        elseif language == "fr_utf" then
            f1_cat_value = "Support"
        elseif language == "hu_utf" then
            f1_cat_value = "Támogatás"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "支援"
        elseif language == "uk_utf" then
            f1_cat_value = "Підтримка"
        elseif language == "sk_utf" then
            f1_cat_value = "Podpora"
        elseif language == "sl_utf" then
            f1_cat_value = "Podpora"
        elseif language == "id_utf" then
            f1_cat_value = "Dukungan"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Soporte"
        elseif language == "sr_utf" then
            f1_cat_value = "Podrška"
        elseif language == "sv_utf" then
            f1_cat_value = "Support"
        elseif language == "ko_utf" then
            f1_cat_value = "지원"
        elseif language == "el_utf" then
            f1_cat_value = "Υποστήριξη"
        elseif language == "en_utf" then
            f1_cat_value = "Support"
        elseif language == "it_utf" then
            f1_cat_value = "Assistenza"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Soporte"
        elseif language == "et_utf" then
            f1_cat_value = "Tugi"
        elseif language == "cs_utf" then
            f1_cat_value = "Podpora"
        elseif language == "ar_utf" then
            f1_cat_value = "الدعم"
        elseif language == "vi_utf" then
            f1_cat_value = "Hỗ trợ"
        elseif language == "th_utf" then
            f1_cat_value = "ความช่วยเหลือ"
        elseif language == "ja_utf" then
            f1_cat_value = "サポート"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "支持"
        elseif language == "pl_utf" then
            f1_cat_value = "Wsparcie"
        elseif language == "da_utf" then
            f1_cat_value = "Support"
        elseif language == "he_utf" then
            f1_cat_value = "תמיכה"
        elseif language == "ro_utf" then
            f1_cat_value = "Asistenta"
        elseif language == "nl_utf" then
            f1_cat_value = "Ondersteuning"
        elseif language == "tr_utf" then
            f1_cat_value = "Destek"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Assistência"
        end
    elseif content_source == "FAQ_INQUIRA" then
        if language == "de_utf" then
            f1_cat_value = "FAQ"
            f2_cat_value = "FAQ"
        elseif language == "no_utf" then
            f1_cat_value = "Vanlige spørsmål"
            f2_cat_value = "Vanlige spørsmål"
        elseif language == "fi_utf" then
            f1_cat_value = "Usein kysyttyjä kysymyksiä"
            f2_cat_value = "Usein kysyttyjä kysymyksiä"
        elseif language == "ru_utf" then
            f1_cat_value = "Часто задаваемые вопросы"
            f2_cat_value = "Часто задаваемые вопросы"
        elseif language == "bg_utf" then
            f1_cat_value = "Често задавани въпроси"
            f2_cat_value = "Често задавани въпроси"
        elseif language == "lt_utf" then
            f1_cat_value = "DUK"
            f2_cat_value = "DUK"
        elseif language == "hr_utf" then
            f1_cat_value = "Česta pitanja"
            f2_cat_value = "Česta pitanja"
        elseif language == "lv_utf" then
            f1_cat_value = "BUJ"
            f2_cat_value = "BUJ"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Perguntas frequentes"
            f2_cat_value = "Perguntas frequentes"
        elseif language == "fr_utf" then
            f1_cat_value = "Questions fréquentes"
            f2_cat_value = "Questions fréquentes"
        elseif language == "hu_utf" then
            f1_cat_value = "GYIK"
            f2_cat_value = "GYIK"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "常見問題"
            f2_cat_value = "常見問題"
        elseif language == "uk_utf" then
            f1_cat_value = "Відповіді на типові запитання"
            f2_cat_value = "Відповіді на типові запитання"
        elseif language == "sk_utf" then
            f1_cat_value = "Často kladené otázky"
            f2_cat_value = "Často kladené otázky"
        elseif language == "sl_utf" then
            f1_cat_value = "Pogosta vprašanja"
            f2_cat_value = "Pogosta vprašanja"
        elseif language == "id_utf" then
            f1_cat_value = "Pertanyaan Umum"
            f2_cat_value = "Pertanyaan Umum"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Preguntas frecuentes"
            f2_cat_value = "Preguntas frecuentes"
        elseif language == "sr_utf" then
            f1_cat_value = "Najčešća pitanja"
            f2_cat_value = "Najčešća pitanja"
        elseif language == "sv_utf" then
            f1_cat_value = "Vanliga frågor och svar (FAQ)"
            f2_cat_value = "Vanliga frågor och svar (FAQ)"
        elseif language == "ko_utf" then
            f1_cat_value = "FAQ"
            f2_cat_value = "FAQ"
        elseif language == "el_utf" then
            f1_cat_value = "Συχνές ερωτήσεις"
            f2_cat_value = "Συχνές ερωτήσεις"
        elseif language == "en_utf" then
            f1_cat_value = "FAQ"
            f2_cat_value = "FAQ"
        elseif language == "it_utf" then
            f1_cat_value = "Domande frequenti"
            f2_cat_value = "Domande frequenti"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Preguntas frecuentes"
            f2_cat_value = "Preguntas frecuentes"
        elseif language == "et_utf" then
            f1_cat_value = "KKK"
            f2_cat_value = "KKK"
        elseif language == "cs_utf" then
            f1_cat_value = "Časté dotazy"
            f2_cat_value = "Časté dotazy"
        elseif language == "ar_utf" then
            f1_cat_value = "الأسئلة الشائعة"
            f2_cat_value = "الأسئلة الشائعة"
        elseif language == "vi_utf" then
            f1_cat_value = "Câu hỏi thường gặp"
            f2_cat_value = "Câu hỏi thường gặp"
        elseif language == "th_utf" then
            f1_cat_value = "คำถามที่พบบ่อย"
            f2_cat_value = "คำถามที่พบบ่อย"
        elseif language == "ja_utf" then
            f1_cat_value = "よくある質問"
            f2_cat_value = "よくある質問"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "常见问题"
            f2_cat_value = "常见问题"
        elseif language == "pl_utf" then
            f1_cat_value = "Często zadawane pytania"
            f2_cat_value = "Często zadawane pytania"
        elseif language == "da_utf" then
            f1_cat_value = "Ofte stillede spørgsmål"
            f2_cat_value = "Ofte stillede spørgsmål"
        elseif language == "he_utf" then
            f1_cat_value = "שאלות נפוצות"
            f2_cat_value = "שאלות נפוצות"
        elseif language == "ro_utf" then
            f1_cat_value = "Intrebari frecvente"
            f2_cat_value = "Intrebari frecvente"
        elseif language == "nl_utf" then
            f1_cat_value = "Veelgestelde vragen"
            f2_cat_value = "Veelgestelde vragen"
        elseif language == "tr_utf" then
            f1_cat_value = "SSS"
            f2_cat_value = "SSS"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Perguntas frequentes"
            f2_cat_value = "Perguntas frequentes"
        end
    elseif content_source == "PTN_LOC" then
        if language == "de_utf" then
            f1_cat_value = "Lösungen"
            f2_cat_value = "Für Ihr Business"
        elseif language == "no_utf" then
            f1_cat_value = "Løsninger"
            f2_cat_value = "For bedrift"
        elseif language == "fi_utf" then
            f1_cat_value = "Ratkaisut"
            f2_cat_value = "Yritykselle"
        elseif language == "ru_utf" then
            f1_cat_value = "Решения"
            f2_cat_value = "Для бизнеса"
        elseif language == "bg_utf" then
            f1_cat_value = "Решения"
            f2_cat_value = "За бизнеса"
        elseif language == "lt_utf" then
            f1_cat_value = "Sprendimai"
            f2_cat_value = "Verslui"
        elseif language == "hr_utf" then
            f1_cat_value = "Rješenja"
            f2_cat_value = "Rješenja za poslovanje"
        elseif language == "lv_utf" then
            f1_cat_value = "Risinājumi"
            f2_cat_value = "Biznesam"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Soluções"
            f2_cat_value = "Para o negócio"
        elseif language == "fr_utf" then
            f1_cat_value = "Solutions"
            f2_cat_value = "Pour les professionnels"
        elseif language == "hu_utf" then
            f1_cat_value = "Megoldások"
            f2_cat_value = "Üzleti megoldások"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "解决方案"
            f2_cat_value = "營業用"
        elseif language == "uk_utf" then
            f1_cat_value = "Рішення"
            f2_cat_value = "Для бізнесу"
        elseif language == "sk_utf" then
            f1_cat_value = "Riešenia"
            f2_cat_value = "Pre Podnikateľov"
        elseif language == "sl_utf" then
            f1_cat_value = "Rešitve"
            f2_cat_value = "Rešitve za podjetja"
        elseif language == "id_utf" then
            f1_cat_value = "Solusi"
            f2_cat_value = "Untuk bisnis"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Soluciones"
            f2_cat_value = "Para empresas"
        elseif language == "sr_utf" then
            f1_cat_value = "Rešenja"
            f2_cat_value = "Za Vaš biznis"
        elseif language == "sv_utf" then
            f1_cat_value = "Lösningar"
            f2_cat_value = "För företag"
        elseif language == "ko_utf" then
            f1_cat_value = "솔루션"
            f2_cat_value = "파트너 및 기업용솔루션"
        elseif language == "el_utf" then
            f1_cat_value = "Λύσεις"
            f2_cat_value = "Για την επιχείρηση"
        elseif language == "en_utf" then
            f1_cat_value = "Solutions"
            f2_cat_value = "For Business"
        elseif language == "it_utf" then
            f1_cat_value = "Soluzioni"
            f2_cat_value = "Soluzioni per il business"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Soluciones"
            f2_cat_value = "Para empresas"
        elseif language == "et_utf" then
            f1_cat_value = "Lahendused"
            f2_cat_value = "Ettevõtetele"
        elseif language == "cs_utf" then
            f1_cat_value = "Řešení"
            f2_cat_value = "Pro vaše podnikání"
        elseif language == "ar_utf" then
            f1_cat_value = "الحلول"
            f2_cat_value = "للعمل"
        elseif language == "vi_utf" then
            f1_cat_value = "Giải pháp"
            f2_cat_value = "Dành cho Doanh nghiệp"
        elseif language == "th_utf" then
            f1_cat_value = "โซลูชั่น"
            f2_cat_value = "สำหรับธุรกิจ"
        elseif language == "ja_utf" then
            f1_cat_value = "ソリューション"
            f2_cat_value = "事業用"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "解决方案"
            f2_cat_value = "商业用途"
        elseif language == "pl_utf" then
            f1_cat_value = "Rozwiązania"
            f2_cat_value = "Dla biznesu"
        elseif language == "da_utf" then
            f1_cat_value = "Løsninger"
            f2_cat_value = "Til brancher"
        elseif language == "he_utf" then
            f1_cat_value = "פתרונות"
            f2_cat_value = "לעסקים"
        elseif language == "ro_utf" then
            f1_cat_value = "Solutii"
            f2_cat_value = "Pentru afaceri"
        elseif language == "nl_utf" then
            f1_cat_value = "Oplossingen"
            f2_cat_value = "Voor bedrijven"
        elseif language == "tr_utf" then
            f1_cat_value = "Çözümler"
            f2_cat_value = "İş için"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Soluções"
            f2_cat_value = "Para o negócio"
        end
    elseif content_source == "B2C_SDLPAGE" then
        if language == "de_utf" then
            f1_cat_value = "Lösungen"
            f2_cat_value = "Für Ihr Zuhause"
        elseif language == "no_utf" then
            f1_cat_value = "Løsninger"
            f2_cat_value = "For hjem"
        elseif language == "fi_utf" then
            f1_cat_value = "Ratkaisut"
            f2_cat_value = "Kotiin"
        elseif language == "ru_utf" then
            f1_cat_value = "Решения"
            f2_cat_value = "Для дома"
        elseif language == "bg_utf" then
            f1_cat_value = "Решения"
            f2_cat_value = "За дома"
        elseif language == "lt_utf" then
            f1_cat_value = "Sprendimai"
            f2_cat_value = " Namams"
        elseif language == "hr_utf" then
            f1_cat_value = "Rješenja"
            f2_cat_value = "Rješenja za dom"
        elseif language == "lv_utf" then
            f1_cat_value = "Risinājumi"
            f2_cat_value = "Mājai"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Soluções"
            f2_cat_value = "Para casa"
        elseif language == "fr_utf" then
            f1_cat_value = "Solutions"
            f2_cat_value = "Pour les particuliers"
        elseif language == "hu_utf" then
            f1_cat_value = "Megoldások"
            f2_cat_value = "Lakossági megoldások"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "解决方案"
            f2_cat_value = "住宅用"
        elseif language == "uk_utf" then
            f1_cat_value = "Рішення"
            f2_cat_value = "Для дому"
        elseif language == "sk_utf" then
            f1_cat_value = "Riešenia"
            f2_cat_value = "Pre váš Domov"
        elseif language == "sl_utf" then
            f1_cat_value = "Rešitve"
            f2_cat_value = "Rešitve za dom"
        elseif language == "id_utf" then
            f1_cat_value = "Solusi"
            f2_cat_value = "Untuk Rumah"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Soluciones"
            f2_cat_value = "Para el hogar"
        elseif language == "sr_utf" then
            f1_cat_value = "Rešenja"
            f2_cat_value = "Za Vaš dom"
        elseif language == "sv_utf" then
            f1_cat_value = "Lösningar"
            f2_cat_value = "För privatpersoner"
        elseif language == "ko_utf" then
            f1_cat_value = "솔루션"
            f2_cat_value = "가정용솔루션"
        elseif language == "el_utf" then
            f1_cat_value = "Λύσεις"
            f2_cat_value = "Για το σπίτι"
        elseif language == "en_utf" then
            f1_cat_value = "Solutions"
            f2_cat_value = "For Home"
        elseif language == "it_utf" then
            f1_cat_value = "Soluzioni"
            f2_cat_value = "Soluzioni per la casa"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Soluciones"
            f2_cat_value = "Para el hogar"
        elseif language == "et_utf" then
            f1_cat_value = "Lahendused"
            f2_cat_value = "Kodu jaoks"
        elseif language == "cs_utf" then
            f1_cat_value = "Řešení"
            f2_cat_value = "Pro váš domov"
        elseif language == "ar_utf" then
            f1_cat_value = "الحلول"
            f2_cat_value = "من أجل الوطن"
        elseif language == "vi_utf" then
            f1_cat_value = "Giải pháp"
            f2_cat_value = "Cho gia đình"
        elseif language == "th_utf" then
            f1_cat_value = "โซลูชั่น"
            f2_cat_value = "สำหรับบ้าน"
        elseif language == "ja_utf" then
            f1_cat_value = "ソリューション"
            f2_cat_value = "家庭用"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "解决方案"
            f2_cat_value = "家居用途"
        elseif language == "pl_utf" then
            f1_cat_value = "Rozwiązania"
            f2_cat_value = "Rozwiązania dla domu"
        elseif language == "da_utf" then
            f1_cat_value = "Løsninger"
            f2_cat_value = "til hjemmet"
        elseif language == "he_utf" then
            f1_cat_value = "פתרונות"
            f2_cat_value = "לבית"
        elseif language == "ro_utf" then
            f1_cat_value = "Solutii"
            f2_cat_value = "Pentru acasa"
        elseif language == "nl_utf" then
            f1_cat_value = "Oplossingen"
            f2_cat_value = "Voor woningen"
        elseif language == "tr_utf" then
            f1_cat_value = "Çözümler"
            f2_cat_value = "Ev için"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Soluções"
            f2_cat_value = "Para casa"
        end
    elseif content_source == "ABOUTUS_SDLPAGE_SUPPLIERS" then
        if language == "de_utf" then
            f1_cat_value = "Über uns"
            f2_cat_value = "Lieferanten"
        elseif language == "no_utf" then
            f1_cat_value = "Selskap"
            f2_cat_value = "Leverandører"
        elseif language == "fi_utf" then
            f1_cat_value = "Yritys"
            f2_cat_value = " Tavarantoimittajamme"
        elseif language == "ru_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "Наши поставщики"
        elseif language == "bg_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "Нашите доставчици"
        elseif language == "lt_utf" then
            f1_cat_value = "Mūsų Įmonė"
            f2_cat_value = "Mūsų tiekėjai"
        elseif language == "hr_utf" then
            f1_cat_value = "Poduzeće"
            f2_cat_value = "Naši dobavljači"
        elseif language == "lv_utf" then
            f1_cat_value = "Uzņēmums"
            f2_cat_value = "Mūsu piegādātāji"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Nossos Fornecedores"
        elseif language == "fr_utf" then
            f1_cat_value = "Société"
            f2_cat_value = "Nos fournisseurs"
        elseif language == "hu_utf" then
            f1_cat_value = "Céges információk"
            f2_cat_value = "Beszállítóink"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "公司"
            f2_cat_value = "我們的供應商"
        elseif language == "uk_utf" then
            f1_cat_value = "Компанія"
            f2_cat_value = "Наші постачальники"
        elseif language == "sk_utf" then
            f1_cat_value = "Spoločnosť"
            f2_cat_value = "Naši Dodávatelia"
        elseif language == "sl_utf" then
            f1_cat_value = "Podjetje"
            f2_cat_value = "Naši dobavitelji"
        elseif language == "id_utf" then
            f1_cat_value = "Perusahaan"
            f2_cat_value = "Pemasok kami"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Proveedores"
        elseif language == "sr_utf" then
            f1_cat_value = "Kompanija"
            f2_cat_value = "Naši dobavljači"
        elseif language == "sv_utf" then
            f1_cat_value = "Företaget"
            f2_cat_value = "Våra leverantörer"
        elseif language == "ko_utf" then
            f1_cat_value = "회사"
            f2_cat_value = "공급사관련"
        elseif language == "el_utf" then
            f1_cat_value = "Εταιρεία"
            f2_cat_value = "Οι προμηθευτές μας"
        elseif language == "en_utf" then
            f1_cat_value = "Company"
            f2_cat_value = "Our Suppliers"
        elseif language == "it_utf" then
            f1_cat_value = "Società"
            f2_cat_value = "Fornitori"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Proveedores"
        elseif language == "et_utf" then
            f1_cat_value = "Ettevõte"
            f2_cat_value = "Meie tarnijad"
        elseif language == "cs_utf" then
            f1_cat_value = "Společnost"
            f2_cat_value = "Naši dodavatelé"
        elseif language == "ar_utf" then
            f1_cat_value = "الشركة"
            f2_cat_value = "موردينا"
        elseif language == "vi_utf" then
            f1_cat_value = "Công ty"
            f2_cat_value = "Nhà cung cấp của chúng tôi"
        elseif language == "th_utf" then
            f1_cat_value = "บริษัท"
            f2_cat_value = "ซัพพลายเออร์ของเรา"
        elseif language == "ja_utf" then
            f1_cat_value = "会社名"
            f2_cat_value = "私たちのサプライヤー"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "公司"
            f2_cat_value = "我们的供应商"
        elseif language == "pl_utf" then
            f1_cat_value = "Firma"
            f2_cat_value = "Nasi dostawcy"
        elseif language == "da_utf" then
            f1_cat_value = "Virksomhed"
            f2_cat_value = "Leverandører"
        elseif language == "he_utf" then
            f1_cat_value = "חברה"
            f2_cat_value = "הספקים שלנו"
        elseif language == "ro_utf" then
            f1_cat_value = "Companie"
            f2_cat_value = "Furnizorii noștri"
        elseif language == "nl_utf" then
            f1_cat_value = "Bedrijf"
            f2_cat_value = "Onze leveranciers"
        elseif language == "tr_utf" then
            f1_cat_value = "Şirket"
            f2_cat_value = "Tedarikçilerimiz"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Nossos Fornecedores"
        end
    elseif content_source == "INSGT_SDLPAGE" then
        if language == "de_utf" then
            f1_cat_value = "Über uns"
            f2_cat_value = "Unternehmens-Übersicht"
        elseif language == "no_utf" then
            f1_cat_value = "Selskap"
            f2_cat_value = "Om Schneider Electric"
        elseif language == "fi_utf" then
            f1_cat_value = "Yritys"
            f2_cat_value = "Yrityskuvaus"
        elseif language == "ru_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "Сведения о компании"
        elseif language == "bg_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "За компанията"
        elseif language == "lt_utf" then
            f1_cat_value = "Mūsų Įmonė"
            f2_cat_value = " Įmonės apžvalga"
        elseif language == "hr_utf" then
            f1_cat_value = "Poduzeće"
            f2_cat_value = "O Schneider Electric"
        elseif language == "lv_utf" then
            f1_cat_value = "Uzņēmums"
            f2_cat_value = "Kompānijas pārskats"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Resenha da Companhia"
        elseif language == "fr_utf" then
            f1_cat_value = "Société"
            f2_cat_value = "Présentation de la société"
        elseif language == "hu_utf" then
            f1_cat_value = "Céges információk"
            f2_cat_value = "Cégünkről"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "公司"
            f2_cat_value = "公司概況"
        elseif language == "uk_utf" then
            f1_cat_value = "Компанія"
            f2_cat_value = "Загальна інформація про компанію"
        elseif language == "sk_utf" then
            f1_cat_value = "Spoločnosť"
            f2_cat_value = "O spoločnosti"
        elseif language == "sl_utf" then
            f1_cat_value = "Podjetje"
            f2_cat_value = "O Schneider Electric"
        elseif language == "id_utf" then
            f1_cat_value = "Perusahaan"
            f2_cat_value = "Ikhtisar Perusahaan"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Visión general de la empresa"
        elseif language == "sr_utf" then
            f1_cat_value = "Kompanija"
            f2_cat_value = "O kompaniji"
        elseif language == "sv_utf" then
            f1_cat_value = "Företaget"
            f2_cat_value = "Företagsöversikt"
        elseif language == "ko_utf" then
            f1_cat_value = "회사"
            f2_cat_value = "슈나이더일렉트릭 관련"
        elseif language == "el_utf" then
            f1_cat_value = "Εταιρεία"
            f2_cat_value = "Επισκόπηση της εταιρείας"
        elseif language == "en_utf" then
            f1_cat_value = "Company"
            f2_cat_value = "Company Overview"
        elseif language == "it_utf" then
            f1_cat_value = "Società"
            f2_cat_value = "Informazioni aziendali"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Visión general de la empresa"
        elseif language == "et_utf" then
            f1_cat_value = "Ettevõte"
            f2_cat_value = "Ettevõtte ülevaade"
        elseif language == "cs_utf" then
            f1_cat_value = "Společnost"
            f2_cat_value = "O společnosti"
        elseif language == "ar_utf" then
            f1_cat_value = "الشركة"
            f2_cat_value = "نبذة عن الشركة"
        elseif language == "vi_utf" then
            f1_cat_value = "Công ty"
            f2_cat_value = "Tổng quan công ty"
        elseif language == "th_utf" then
            f1_cat_value = "บริษัท"
            f2_cat_value = "ภาพรวมบริษัท"
        elseif language == "ja_utf" then
            f1_cat_value = "会社名"
            f2_cat_value = "会社概要"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "公司"
            f2_cat_value = "集团概览"
        elseif language == "pl_utf" then
            f1_cat_value = "Firma"
            f2_cat_value = "O firmie"
        elseif language == "da_utf" then
            f1_cat_value = "Virksomhed"
            f2_cat_value = "Virksomhedsprofil "
        elseif language == "he_utf" then
            f1_cat_value = "חברה"
            f2_cat_value = "סקירת חברה"
        elseif language == "ro_utf" then
            f1_cat_value = "Companie"
            f2_cat_value = "Prezentarea generală a companiei"
        elseif language == "nl_utf" then
            f1_cat_value = "Bedrijf"
            f2_cat_value = "Over ons"
        elseif language == "tr_utf" then
            f1_cat_value = "Şirket"
            f2_cat_value = "Şirket Genel Bakışı"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Visão Geral da Empresa"
        end
    elseif content_source == "SVC_SDLPAGE" then
        if language == "de_utf" then
            f1_cat_value = "Services"
        elseif language == "no_utf" then
            f1_cat_value = "Tjenester"
        elseif language == "fi_utf" then
            f1_cat_value = "Palvelut"
        elseif language == "ru_utf" then
            f1_cat_value = "Услуги"
        elseif language == "bg_utf" then
            f1_cat_value = "Услуги"
        elseif language == "lt_utf" then
            f1_cat_value = "Aptarnavimas"
        elseif language == "hr_utf" then
            f1_cat_value = "Usluge"
        elseif language == "lv_utf" then
            f1_cat_value = "Pakalpojumi"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Serviços"
        elseif language == "fr_utf" then
            f1_cat_value = "Services"
        elseif language == "hu_utf" then
            f1_cat_value = "Szolgáltatások"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "服務"
        elseif language == "uk_utf" then
            f1_cat_value = "Послуги"
        elseif language == "sk_utf" then
            f1_cat_value = "Služby"
        elseif language == "sl_utf" then
            f1_cat_value = "Storitve"
        elseif language == "id_utf" then
            f1_cat_value = "Layanan"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Servicios"
        elseif language == "sr_utf" then
            f1_cat_value = "Servis"
        elseif language == "sv_utf" then
            f1_cat_value = "Tjänster"
        elseif language == "ko_utf" then
            f1_cat_value = "서비스"
        elseif language == "el_utf" then
            f1_cat_value = "Υπηρεσίες"
        elseif language == "en_utf" then
            f1_cat_value = "Services"
        elseif language == "it_utf" then
            f1_cat_value = "Servizi"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Servicios"
        elseif language == "et_utf" then
            f1_cat_value = "Teenused"
        elseif language == "cs_utf" then
            f1_cat_value = "Služby"
        elseif language == "ar_utf" then
            f1_cat_value = "الخدمات"
        elseif language == "vi_utf" then
            f1_cat_value = "Dịch vụ"
        elseif language == "th_utf" then
            f1_cat_value = "บริการ"
        elseif language == "ja_utf" then
            f1_cat_value = "サービス"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "服务"
        elseif language == "pl_utf" then
            f1_cat_value = "Usługi"
        elseif language == "da_utf" then
            f1_cat_value = "Service"
        elseif language == "he_utf" then
            f1_cat_value = "שירות"
        elseif language == "ro_utf" then
            f1_cat_value = "Servicii"
        elseif language == "nl_utf" then
            f1_cat_value = "Services"
        elseif language == "tr_utf" then
            f1_cat_value = "Hizmetler"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Serviços"
        end
    elseif content_source == "DIGEST_PLUS" then
        if language == "de_utf" then
            f1_cat_value = "Dokumente"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        elseif language == "no_utf" then
            f1_cat_value = "Dokumenter"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        elseif language == "fi_utf" then
            f1_cat_value = "Asiakirjat"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        elseif language == "ru_utf" then
            f1_cat_value = "Документация"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        elseif language == "bg_utf" then
            f1_cat_value = "Документи"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        elseif language == "lt_utf" then
            f1_cat_value = "Dokumentai"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        elseif language == "hr_utf" then
            f1_cat_value = "Dokumenti"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        elseif language == "lv_utf" then
            f1_cat_value = "Dokumenti"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Documentos"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        elseif language == "fr_utf" then
            f1_cat_value = "Documents"
            f2_cat_value = "Catalogues & Brochures"
            f3_cat_value = "Catalogue"
        elseif language == "hu_utf" then
            f1_cat_value = "Dokumentumok"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "文件"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        elseif language == "uk_utf" then
            f1_cat_value = "Документи"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        elseif language == "sk_utf" then
            f1_cat_value = "Dokumenty"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        elseif language == "sl_utf" then
            f1_cat_value = "Dokumenti"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        elseif language == "id_utf" then
            f1_cat_value = "Dokumen"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Documentos"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        elseif language == "sr_utf" then
            f1_cat_value = "Dokumenti"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        elseif language == "sv_utf" then
            f1_cat_value = "Dokument"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        elseif language == "ko_utf" then
            f1_cat_value = "문서"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        elseif language == "el_utf" then
            f1_cat_value = "Έγγραφα"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        elseif language == "en_utf" then
            f1_cat_value = "Documents"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        elseif language == "it_utf" then
            f1_cat_value = "Documenti"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Documentos"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        elseif language == "et_utf" then
            f1_cat_value = "Dokumendid"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        elseif language == "cs_utf" then
            f1_cat_value = "Dokumenty"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        elseif language == "ar_utf" then
            f1_cat_value = "المستندات"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        elseif language == "vi_utf" then
            f1_cat_value = "Tài liệu"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        elseif language == "th_utf" then
            f1_cat_value = "เอกสาร"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        elseif language == "ja_utf" then
            f1_cat_value = "ドキュメント"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "文档"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        elseif language == "pl_utf" then
            f1_cat_value = "Dokumenty"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        elseif language == "da_utf" then
            f1_cat_value = "Dokumenter"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        elseif language == "he_utf" then
            f1_cat_value = "מסמכים"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        elseif language == "ro_utf" then
            f1_cat_value = "Documente"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        elseif language == "nl_utf" then
            f1_cat_value = "Documenten"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        elseif language == "tr_utf" then
            f1_cat_value = "Belgeler"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Documentos"
            f2_cat_value = "Catalogs & Brochures"
            f3_cat_value = "Catalog"
        end
    elseif content_source == "ECAT_BSL_SUB" then
        if language == "de_utf" then
            f1_cat_value = "Produkte"
            f2_cat_value = "Produkte ersetzen"
        elseif language == "no_utf" then
            f1_cat_value = "Produkter"
            f2_cat_value = "Erstatningsprodukt"
        elseif language == "fi_utf" then
            f1_cat_value = "Tuotteet"
            f2_cat_value = "Tuotekorvaavuus"
        elseif language == "ru_utf" then
            f1_cat_value = "Продукты"
            f2_cat_value = "Замена оборудования"
        elseif language == "bg_utf" then
            f1_cat_value = "Продукти"
            f2_cat_value = "Замяна"
        elseif language == "lt_utf" then
            f1_cat_value = "Gaminiai"
            f2_cat_value = "Pakeitimas"
        elseif language == "hr_utf" then
            f1_cat_value = "Proizvodi"
            f2_cat_value = "Zamjena proizvoda"
        elseif language == "lv_utf" then
            f1_cat_value = "Produkti"
            f2_cat_value = "Aizvietošana"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Produtos"
            f2_cat_value = "Substituição"
        elseif language == "fr_utf" then
            f1_cat_value = "Produits"
            f2_cat_value = "Substitution"
        elseif language == "hu_utf" then
            f1_cat_value = "Termékek"
            f2_cat_value = "Termékkiváltások"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "產品"
            f2_cat_value = "替代機種"
        elseif language == "uk_utf" then
            f1_cat_value = "Продукти"
            f2_cat_value = "Заміна"
        elseif language == "sk_utf" then
            f1_cat_value = "Produkty"
            f2_cat_value = "Náhradné Produkty"
        elseif language == "sl_utf" then
            f1_cat_value = "izdelkov"
            f2_cat_value = "Zamenjava"
        elseif language == "id_utf" then
            f1_cat_value = "Produk"
            f2_cat_value = "Pengganti"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Productos"
            f2_cat_value = "Productos de substitución"
        elseif language == "sr_utf" then
            f1_cat_value = "Proizvodi"
            f2_cat_value = "Zamena"
        elseif language == "sv_utf" then
            f1_cat_value = "Produkter"
            f2_cat_value = "Utbyte"
        elseif language == "ko_utf" then
            f1_cat_value = "제품"
            f2_cat_value = "대체품 관련"
        elseif language == "el_utf" then
            f1_cat_value = "Προϊόντα"
            f2_cat_value = "Υποκατάσταση"
        elseif language == "en_utf" then
            f1_cat_value = "Products"
            f2_cat_value = "Substitution"
        elseif language == "it_utf" then
            f1_cat_value = "Prodotti"
            f2_cat_value = "Prodotti sostitutivi"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Productos"
            f2_cat_value = "Productos de substitución"
        elseif language == "et_utf" then
            f1_cat_value = "Tooted"
            f2_cat_value = "Asendamine"
        elseif language == "cs_utf" then
            f1_cat_value = "Produkty"
            f2_cat_value = "Náhrada produktů"
        elseif language == "ar_utf" then
            f1_cat_value = "المنتجات"
            f2_cat_value = "الاستبدال"
        elseif language == "vi_utf" then
            f1_cat_value = "Sản phẩm"
            f2_cat_value = "Thay thế"
        elseif language == "th_utf" then
            f1_cat_value = "ผลิตภัณฑ์"
            f2_cat_value = "การแทน"
        elseif language == "ja_utf" then
            f1_cat_value = "製品"
            f2_cat_value = "置換"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "产品"
            f2_cat_value = "替换查询/工具"
        elseif language == "pl_utf" then
            f1_cat_value = "Produkty"
            f2_cat_value = "Podstawienie"
        elseif language == "da_utf" then
            f1_cat_value = "Produkter"
            f2_cat_value = "udskiftning"
        elseif language == "he_utf" then
            f1_cat_value = "מוצרים"
            f2_cat_value = "החלפה"
        elseif language == "ro_utf" then
            f1_cat_value = "Produse"
            f2_cat_value = "Substituţie"
        elseif language == "nl_utf" then
            f1_cat_value = "Producten"
            f2_cat_value = "Vervangende producten"
        elseif language == "tr_utf" then
            f1_cat_value = "Ürünler"
            f2_cat_value = "ikame"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Produtos"
            f2_cat_value = "Substituição"
        end
    elseif content_source == "ORACLE_CROSSREF" then
        if language == "de_utf" then
            f1_cat_value = "Produkte"
            f2_cat_value = "Produkte ersetzen"
        elseif language == "no_utf" then
            f1_cat_value = "Produkter"
            f2_cat_value = "Erstatningsprodukt"
        elseif language == "fi_utf" then
            f1_cat_value = "Tuotteet"
            f2_cat_value = "Tuotekorvaavuus"
        elseif language == "ru_utf" then
            f1_cat_value = "Продукты"
            f2_cat_value = "Замена оборудования"
        elseif language == "bg_utf" then
            f1_cat_value = "Продукти"
            f2_cat_value = "Замяна"
        elseif language == "lt_utf" then
            f1_cat_value = "Gaminiai"
            f2_cat_value = "Pakeitimas"
        elseif language == "hr_utf" then
            f1_cat_value = "Proizvodi"
            f2_cat_value = "Zamjena proizvoda"
        elseif language == "lv_utf" then
            f1_cat_value = "Produkti"
            f2_cat_value = "Aizvietošana"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Produtos"
            f2_cat_value = "Substituição"
        elseif language == "fr_utf" then
            f1_cat_value = "Produits"
            f2_cat_value = "Substitution"
        elseif language == "hu_utf" then
            f1_cat_value = "Termékek"
            f2_cat_value = "Termékkiváltások"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "產品"
            f2_cat_value = "替代機種"
        elseif language == "uk_utf" then
            f1_cat_value = "Продукти"
            f2_cat_value = "Заміна"
        elseif language == "sk_utf" then
            f1_cat_value = "Produkty"
            f2_cat_value = "Náhradné Produkty"
        elseif language == "sl_utf" then
            f1_cat_value = "izdelkov"
            f2_cat_value = "Zamenjava"
        elseif language == "id_utf" then
            f1_cat_value = "Produk"
            f2_cat_value = "Pengganti"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Productos"
            f2_cat_value = "Productos de substitución"
        elseif language == "sr_utf" then
            f1_cat_value = "Proizvodi"
            f2_cat_value = "Zamena"
        elseif language == "sv_utf" then
            f1_cat_value = "Produkter"
            f2_cat_value = "Utbyte"
        elseif language == "ko_utf" then
            f1_cat_value = "제품"
            f2_cat_value = "대체품 관련"
        elseif language == "el_utf" then
            f1_cat_value = "Προϊόντα"
            f2_cat_value = "Υποκατάσταση"
        elseif language == "en_utf" then
            f1_cat_value = "Products"
            f2_cat_value = "Substitution"
        elseif language == "it_utf" then
            f1_cat_value = "Prodotti"
            f2_cat_value = "Prodotti sostitutivi"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Productos"
            f2_cat_value = "Productos de substitución"
        elseif language == "et_utf" then
            f1_cat_value = "Tooted"
            f2_cat_value = "Asendamine"
        elseif language == "cs_utf" then
            f1_cat_value = "Produkty"
            f2_cat_value = "Náhrada produktů"
        elseif language == "ar_utf" then
            f1_cat_value = "المنتجات"
            f2_cat_value = "الاستبدال"
        elseif language == "vi_utf" then
            f1_cat_value = "Sản phẩm"
            f2_cat_value = "Thay thế"
        elseif language == "th_utf" then
            f1_cat_value = "ผลิตภัณฑ์"
            f2_cat_value = "การแทน"
        elseif language == "ja_utf" then
            f1_cat_value = "製品"
            f2_cat_value = "置換"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "产品"
            f2_cat_value = "替换查询/工具"
        elseif language == "pl_utf" then
            f1_cat_value = "Produkty"
            f2_cat_value = "Podstawienie"
        elseif language == "da_utf" then
            f1_cat_value = "Produkter"
            f2_cat_value = "udskiftning"
        elseif language == "he_utf" then
            f1_cat_value = "מוצרים"
            f2_cat_value = "החלפה"
        elseif language == "ro_utf" then
            f1_cat_value = "Produse"
            f2_cat_value = "Substituţie"
        elseif language == "nl_utf" then
            f1_cat_value = "Producten"
            f2_cat_value = "Vervangende producten"
        elseif language == "tr_utf" then
            f1_cat_value = "Ürünler"
            f2_cat_value = "ikame"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Produtos"
            f2_cat_value = "Substituição"
        end
    elseif content_source == "FAQ_BFO" then
        if language == "de_utf" then
            f1_cat_value = "FAQ"
            f2_cat_value = "FAQ"
        elseif language == "no_utf" then
            f1_cat_value = "Vanlige spørsmål"
            f2_cat_value = "Vanlige spørsmål"
        elseif language == "fi_utf" then
            f1_cat_value = "Usein kysyttyjä kysymyksiä"
            f2_cat_value = "Usein kysyttyjä kysymyksiä"
        elseif language == "ru_utf" then
            f1_cat_value = "Часто задаваемые вопросы"
            f2_cat_value = "Часто задаваемые вопросы"
        elseif language == "bg_utf" then
            f1_cat_value = "Често задавани въпроси"
            f2_cat_value = "Често задавани въпроси"
        elseif language == "lt_utf" then
            f1_cat_value = "DUK"
            f2_cat_value = "DUK"
        elseif language == "hr_utf" then
            f1_cat_value = "Česta pitanja"
            f2_cat_value = "Česta pitanja"
        elseif language == "lv_utf" then
            f1_cat_value = "BUJ"
            f2_cat_value = "BUJ"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Perguntas frequentes"
            f2_cat_value = "Perguntas frequentes"
        elseif language == "fr_utf" then
            f1_cat_value = "Questions fréquentes"
            f2_cat_value = "Questions fréquentes"
        elseif language == "hu_utf" then
            f1_cat_value = "GYIK"
            f2_cat_value = "GYIK"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "常見問題"
            f2_cat_value = "常見問題"
        elseif language == "uk_utf" then
            f1_cat_value = "Відповіді на типові запитання"
            f2_cat_value = "Відповіді на типові запитання"
        elseif language == "sk_utf" then
            f1_cat_value = "Často kladené otázky"
            f2_cat_value = "Často kladené otázky"
        elseif language == "sl_utf" then
            f1_cat_value = "Pogosta vprašanja"
            f2_cat_value = "Pogosta vprašanja"
        elseif language == "id_utf" then
            f1_cat_value = "Pertanyaan Umum"
            f2_cat_value = "Pertanyaan Umum"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Preguntas frecuentes"
            f2_cat_value = "Preguntas frecuentes"
        elseif language == "sr_utf" then
            f1_cat_value = "Najčešća pitanja"
            f2_cat_value = "Najčešća pitanja"
        elseif language == "sv_utf" then
            f1_cat_value = "Vanliga frågor och svar (FAQ)"
            f2_cat_value = "Vanliga frågor och svar (FAQ)"
        elseif language == "ko_utf" then
            f1_cat_value = "FAQ"
            f2_cat_value = "FAQ"
        elseif language == "el_utf" then
            f1_cat_value = "Συχνές ερωτήσεις"
            f2_cat_value = "Συχνές ερωτήσεις"
        elseif language == "en_utf" then
            f1_cat_value = "FAQ"
            f2_cat_value = "FAQ"
        elseif language == "it_utf" then
            f1_cat_value = "Domande frequenti"
            f2_cat_value = "Domande frequenti"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Preguntas frecuentes"
            f2_cat_value = "Preguntas frecuentes"
        elseif language == "et_utf" then
            f1_cat_value = "KKK"
            f2_cat_value = "KKK"
        elseif language == "cs_utf" then
            f1_cat_value = "Časté dotazy"
            f2_cat_value = "Časté dotazy"
        elseif language == "ar_utf" then
            f1_cat_value = "الأسئلة الشائعة"
            f2_cat_value = "الأسئلة الشائعة"
        elseif language == "vi_utf" then
            f1_cat_value = "Câu hỏi thường gặp"
            f2_cat_value = "Câu hỏi thường gặp"
        elseif language == "th_utf" then
            f1_cat_value = "คำถามที่พบบ่อย"
            f2_cat_value = "คำถามที่พบบ่อย"
        elseif language == "ja_utf" then
            f1_cat_value = "よくある質問"
            f2_cat_value = "よくある質問"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "常见问题"
            f2_cat_value = "常见问题"
        elseif language == "pl_utf" then
            f1_cat_value = "Często zadawane pytania"
            f2_cat_value = "Często zadawane pytania"
        elseif language == "da_utf" then
            f1_cat_value = "Ofte stillede spørgsmål"
            f2_cat_value = "Ofte stillede spørgsmål"
        elseif language == "he_utf" then
            f1_cat_value = "שאלות נפוצות"
            f2_cat_value = "שאלות נפוצות"
        elseif language == "ro_utf" then
            f1_cat_value = "Intrebari frecvente"
            f2_cat_value = "Intrebari frecvente"
        elseif language == "nl_utf" then
            f1_cat_value = "Veelgestelde vragen"
            f2_cat_value = "Veelgestelde vragen"
        elseif language == "tr_utf" then
            f1_cat_value = "SSS"
            f2_cat_value = "SSS"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Perguntas frequentes"
            f2_cat_value = "Perguntas frequentes"
        end
    elseif content_source == "ABOUTUS_SDLPAGE_INVESTOR_RELATIONS" then
        if language == "de_utf" then
            f1_cat_value = "Über uns"
            f2_cat_value = "Inverstor relations"
        elseif language == "no_utf" then
            f1_cat_value = "Selskap"
            f2_cat_value = "Inverstor relations"
        elseif language == "fi_utf" then
            f1_cat_value = "Yritys"
            f2_cat_value = "Inverstor relations"
        elseif language == "ru_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "Inverstor relations"
        elseif language == "bg_utf" then
            f1_cat_value = "Компания"
            f2_cat_value = "Inverstor relations"
        elseif language == "lt_utf" then
            f1_cat_value = "Mūsų Įmonė"
            f2_cat_value = "Inverstor relations"
        elseif language == "hr_utf" then
            f1_cat_value = "Poduzeće"
            f2_cat_value = "Inverstor relations"
        elseif language == "lv_utf" then
            f1_cat_value = "Uzņēmums"
            f2_cat_value = "Inverstor relations"
        elseif language == "pt_utf" and country == "BR" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Inverstor relations"
        elseif language == "fr_utf" then
            f1_cat_value = "Société"
            f2_cat_value = "Inverstor relations"
        elseif language == "hu_utf" then
            f1_cat_value = "Céges információk"
            f2_cat_value = "Inverstor relations"
        elseif language == "zh_utf" and country == "TW" then
            f1_cat_value = "公司"
            f2_cat_value = "Inverstor relations"
        elseif language == "uk_utf" then
            f1_cat_value = "Компанія"
            f2_cat_value = "Inverstor relations"
        elseif language == "sk_utf" then
            f1_cat_value = "Spoločnosť"
            f2_cat_value = "Inverstor relations"
        elseif language == "sl_utf" then
            f1_cat_value = "Podjetje"
            f2_cat_value = "Inverstor relations"
        elseif language == "id_utf" then
            f1_cat_value = "Perusahaan"
            f2_cat_value = "Inverstor relations"
        elseif language == "es_utf" and country ~= "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Inverstor relations"
        elseif language == "sr_utf" then
            f1_cat_value = "Kompanija"
            f2_cat_value = "Inverstor relations"
        elseif language == "sv_utf" then
            f1_cat_value = "Företaget"
            f2_cat_value = "Inverstor relations"
        elseif language == "ko_utf" then
            f1_cat_value = "회사"
            f2_cat_value = "Inverstor relations"
        elseif language == "el_utf" then
            f1_cat_value = "Εταιρεία"
            f2_cat_value = "Inverstor relations"
        elseif language == "en_utf" then
            f1_cat_value = "Company"
            f2_cat_value = "Inverstor relations"
        elseif language == "it_utf" then
            f1_cat_value = "Società"
            f2_cat_value = "Inverstor relations"
        elseif language == "es_utf" and country == "ES" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Inverstor relations"
        elseif language == "et_utf" then
            f1_cat_value = "Ettevõte"
            f2_cat_value = "Inverstor relations"
        elseif language == "cs_utf" then
            f1_cat_value = "Společnost"
            f2_cat_value = "Inverstor relations"
        elseif language == "ar_utf" then
            f1_cat_value = "الشركة"
            f2_cat_value = "Inverstor relations"
        elseif language == "vi_utf" then
            f1_cat_value = "Công ty"
            f2_cat_value = "Inverstor relations"
        elseif language == "th_utf" then
            f1_cat_value = "บริษัท"
            f2_cat_value = "Inverstor relations"
        elseif language == "ja_utf" then
            f1_cat_value = "会社名"
            f2_cat_value = "Inverstor relations"
        elseif language == "zh_utf" and country == "CN" then
            f1_cat_value = "公司"
            f2_cat_value = "Inverstor relations"
        elseif language == "pl_utf" then
            f1_cat_value = "Firma"
            f2_cat_value = "Inverstor relations"
        elseif language == "da_utf" then
            f1_cat_value = "Virksomhed"
            f2_cat_value = "Inverstor relations"
        elseif language == "he_utf" then
            f1_cat_value = "חברה"
            f2_cat_value = "Inverstor relations"
        elseif language == "ro_utf" then
            f1_cat_value = "Companie"
            f2_cat_value = "Inverstor relations"
        elseif language == "nl_utf" then
            f1_cat_value = "Bedrijf"
            f2_cat_value = "Inverstor relations"
        elseif language == "tr_utf" then
            f1_cat_value = "Şirket"
            f2_cat_value = "Inverstor relations"
        elseif language == "pt_utf" and country == "PT" then
            f1_cat_value = "Empresa"
            f2_cat_value = "Inverstor relations"
        end
    end


	document:setFieldValue(f1_cat_fieldname, f1_cat_value)
	document:setFieldValue(f2_cat_fieldname, f2_cat_value)
	document:setFieldValue(f3_cat_fieldname, f3_cat_value)

	return true
end


